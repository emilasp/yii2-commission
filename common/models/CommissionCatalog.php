<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_catalog".
 *
 * @property integer $id
 * @property integer $image_id
 * @property integer $im_id
 * @property string  $site
 * @property string  $article
 * @property string  $article_full
 * @property string  $name
 * @property string  $description
 * @property float   $cost
 * @property float   $cost_r
 * @property float   $cost_sol
 * @property string  $nominal
 * @property integer $circulation
 * @property integer $is_modify
 * @property integer $is_modify_count
 * @property string  $url
 * @property string  $data
 * @property string  $data_params
 * @property string  $data_search
 * @property integer $year
 * @property string  $country
 * @property integer $active
 * @property integer $per_list
 * @property integer $type
 * @property integer $status
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User    $createdBy
 * @property User    $updatedBy
 * @property File    $image
 */
class CommissionCatalog extends ActiveRecord
{
    /* public const        TYPE_SINGLE     = 1;
     public const        TYPE_BLOCK_LINE = 2;
     public const        TYPE_BLOCK      = 3;
     public const        TYPE_SET        = 4;

     public static $types = [
         self::TYPE_SINGLE     => 'Single',
         self::TYPE_BLOCK_LINE => 'Block line',
         self::TYPE_BLOCK      => 'Block',
         self::TYPE_SET        => 'Set',
     ];*/

    public const STATUS_NEW   = 1;
    public const STATUS_ISSET = 2;

    public static $statuses = [
        self::STATUS_NEW   => 'New',
        self::STATUS_ISSET => 'Isset',
    ];

    public const TYPE_STAMP          = 1;
    public const TYPE_STAMP_SERIES   = 2;
    public const TYPE_STAMP_BLOCK    = 3;
    public const TYPE_STAMP_ENVELOPE = 4;
    public const TYPE_STAMP_POSTCARD = 5;
    public const TYPE_STAMP_SET      = 6;
    public const TYPE_MONEY_BON      = 7;
    public const TYPE_MONEY_COIN     = 8;
    public const TYPE_MONEY_COIN_SET = 9;
    public const TYPE_MONEY_BON_SET  = 10;
    public const TYPE_MONEYS         = 16;
    public const TYPE_STAMPS         = 17;

    public static $types = [
        self::TYPE_STAMPS => 'Марки(все)',
        self::TYPE_MONEYS => 'Деньги(все)',

        self::TYPE_STAMP          => 'Марка',
        self::TYPE_STAMP_SERIES   => 'Серия',
        self::TYPE_STAMP_BLOCK    => 'Блок',
        self::TYPE_STAMP_ENVELOPE => 'Конверт',
        self::TYPE_STAMP_POSTCARD => 'Открытка',
        self::TYPE_STAMP_SET      => 'Набор',
        self::TYPE_MONEY_BON      => 'Банкнота',
        self::TYPE_MONEY_BON_SET  => 'Банкнота - набор',
        self::TYPE_MONEY_COIN     => 'Монета',
        self::TYPE_MONEY_COIN_SET => 'Монета - набор',
    ];

    public static $typeGroups = [
        self::TYPE_STAMPS => [
            self::TYPE_STAMP,
            self::TYPE_STAMP_SERIES,
            self::TYPE_STAMP_BLOCK,
            self::TYPE_STAMP_ENVELOPE,
            self::TYPE_STAMP_POSTCARD,
            self::TYPE_STAMP_SET,
        ],
        self::TYPE_MONEYS => [
            self::TYPE_MONEY_BON,
            self::TYPE_MONEY_BON_SET,
            self::TYPE_MONEY_COIN,
            self::TYPE_MONEY_COIN_SET,
        ],
    ];

    public static $typeSets = [
        CommissionCatalog::TYPE_MONEY_BON_SET,
        CommissionCatalog::TYPE_MONEY_COIN_SET,
        CommissionCatalog::TYPE_STAMP_SET,
    ];

    public const DATA_ATTRIBUTES = [
        'nominal'           => ['Номинал'],
        'size'              => ['Размер'],
        'description'       => ['Описание'],
        //'image'             => ['Описание'],
        'cost'              => ['Цена чист.'],
        'cost_r'            => ['Цена гаш.'],
        'printing_count'    => ['Тираж', 'Тираж (тыс. шт.)'],
        'printing_method'   => ['Способ печати', 'Печать'],
        'paper'             => ['Бумага'],
        'colors'            => ['Цвета', 'Цвет'],
        'perforation'       => ['Перфорация', 'Перфорация (высечка)'],
        'format'            => ['Формат'],
        'count'             => ['Количество'],
        'format_linkage'    => ['Формат сцепки'],
        'format_block'      => ['Формат блока', 'Формат блоков'],
        'format_stamp'      => ['Формат марки', 'Формат марки в блоке', 'Формат марок', 'Формат марок в блоках'],
        'format_stamp_list' => [
            'Формат марочного листа',
            'Формат малого листа',
            'Формат марочных листов',
            'Формат сувенирного листа'
        ],
        'price_gash'        => ['Цена гашеных'],
        'year'              => ['Год выпуска'],
        'article'           => ['№ по каталогу Соловьёва'],
        'category'          => ['Рубрика'],
        'gear_type'         => ['Тип зубцовки'],
        'gear_size'         => ['Размер зубцовки'],
        'designer'          => ['Художник'],
        'classification'    => ['Классификация'],
        'personalities'     => ['Личности'],
        'condition'         => ['Состояние марки'],
        'in_series'         => ['Марки в серии'],
        'watermark'         => ['Водяной знак'],
    ];

    public $imageUpload;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'image' => [
                'class'         => FileSingleBehavior::class,
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['cost', 'cost_r', 'cost_sol'], 'number'],
            [
                [
                    'year',
                    'created_by',
                    'updated_by',
                    'image_id',
                    'im_id',
                    'circulation',
                    'is_modify',
                    'is_modify_count',
                    'active',
                    'per_list',
                    'type',
                    'status'
                ],
                'integer'
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['data', 'data_params', 'data_search', 'nominal'], 'string'],
            [['site', 'article', 'article_full', 'name', 'url', 'country'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'im_id'           => 'ID monetnik',
            'site'            => 'Сайт',
            'article'         => 'Артикул',
            'article_full'    => 'Артикул(f)',
            'name'            => 'Наименование',
            'description'     => 'Описание',
            'cost'            => 'Стоимость',
            'cost_r'          => 'Стоимость(гаш)',
            'cost_sol'        => 'Стоимость(Соловьев)',
            'nominal'         => 'Номинал',
            'circulation'     => 'Тираж',
            'is_modify'       => 'Работа',
            'is_modify_count' => 'Кол-во',
            'url'             => 'Ссылка',
            'year'            => 'Год',
            'country'         => 'Страна',
            'data'            => 'Data',
            'data_params'     => 'Data Params',
            'data_search'     => 'Data Search',
            'image_id'        => 'Изобр',
            'per_list'        => 'На листе',
            'active'          => 'Активен(ИМ)',
            'type'            => 'Тип',
            'status'          => 'Статус',
            'created_at'      => 'Created At',
            'updated_at'      => 'Updated At',
            'created_by'      => 'Created By',
            'updated_by'      => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }


    /////////////////////////////////////////////////
    ///

    /**
     * Формируем наименование для ИМ
     *
     * @return string
     */
    public function getTitle(): string
    {
       /* switch ($this->type) {
            case self::TYPE_MONEY_BON:
            case self::TYPE_MONEY_COIN:
                $name = $this->name;
                break;
            case self::TYPE_MONEY_BON_SET:
            case self::TYPE_MONEY_COIN_SET:
                $name = $this->country . ' набор из NN банкнот ' . $this->name;
        }*/

        $name = $this->nominal . ' ' . $this->year . ' "' . $this->name . '"';
        return str_replace('""', '"', $name);
    }

    /**
     * Формируем таблицу свойств для ИМ
     *
     * @param int|null $index
     * @param array    $stopWords
     * @return string
     */
    public function getTableParams(int $index = null, array $stopWords = ['№', 'каталог']): string
    {
        Yii::$app->security->generateRandomString();

        $data = json_decode($this->data, true);

        $index = $index !== null ? $index : count($data) - 1;

        $dataItem = $data[$index]['params'];

        $html = '' . PHP_EOL;
        if ($dataItem) {
            foreach ($dataItem as $field => $value) {
                if (!preg_match("/[А-Яа-я]/", $field)) {
                    if (isset(self::DATA_ATTRIBUTES[$field])) {
                        $field = self::DATA_ATTRIBUTES[$field][0];
                    } else {
                        continue;
                    }
                }

                $allow = true;
                foreach ($stopWords as $word) {
                    if (mb_stripos($field, $word)) {
                        $allow = false;
                        break;
                    }
                }

                if ($allow && $value) {
                    $html .= " [b]{$field}:[/b] [i]{$value}[/i]" . PHP_EOL;
                }
            }
        }

        return $html;
    }

    /**
     * Формируем ссылку на монетник
     *
     * @return string
     */
    public function getMonetnikUrl(bool $update = false): string
    {
        if ($update) {
            return "https://www.monetnik.ru/goods/edit/{$this->im_id}/";
        } else {
            return "https://www.monetnik.ru/pochtovye-marki/rossiya/listy/100-rublej-2017-{$this->im_id}/";
        }
    }


    /**
     * Высчитываем стоимость по коэффециенту
     *
     * @param int $koef
     * @return float
     */
    public function getCalcultateSum(float $koef): float
    {
        if (!$this->cost) {
            return 0;
        }
        return $this->cost * $koef;
    }

    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function beforeSave($insert)
    {
        $this->setArticleFull();
        $this->setCirculationAndNominal();

        $this->setDataSearchAttribute();

        $this->setModeItem();

        $this->setStatusByImId();

        return parent::beforeSave($insert);
    }

    /**
     * Set status By Im Id
     */
    private function setStatusByImId(): void
    {
        if (!$this->im_id) {
            $this->status = self::STATUS_NEW;
        }
    }

    /**
     * Обновляем серии
     *
     * @param string $article
     * @return string
     */
    private function setArticleFull(): void
    {
        $this->article_full = $this->article;
        if (mb_stripos($this->article, '-')) {
            $dataArticle = explode('-', $this->article);

            if (count($dataArticle) === 2) {
                $numbers   = range((int)$dataArticle[0], (int)$dataArticle[1]);
                $numbers[] = $this->article;
                if (count($numbers) < 20) {
                    $this->article_full = implode(',', $numbers);
                }
            }
        }
    }

    /**
     * Устанавливаем поле для поиска оп gin индексу
     */
    private function setDataSearchAttribute()
    {
        $data = [
            $this->article_full,
            $this->name,
            $this->description,
            $this->country,
            json_encode(json_decode(($this->data ?: '[]'), true), JSON_UNESCAPED_UNICODE),
            $this->year,
            $this->im_id,
            $this->nominal,
        ];

        $this->data_search = implode(' , ', $data);
    }

    /**
     * парсим номенал
     *
     * @param string $nominalString
     * @return string
     */
    public function parseNominal(string $nominalString = null): ?string
    {
        if ($nominalString) {
            $num = (float)$nominalString;
            if ($num) {
                if (mb_stripos($nominalString, 'к')) {
                    $nominalString = $num . ' ' . $this->num2word($num, ['копейка', 'копейки', 'копеек']);
                } else {
                    $nominalString = $num . ' ' . $this->num2word($num, ['рубль', 'рубля', 'рублей']);
                }
            }
        }

        return $nominalString;
    }

    /**
     * Check is SET
     *
     * @return bool
     */
    public function isSet(): bool
    {
        return in_array($this->type, [
            CommissionCatalog::TYPE_MONEY_COIN_SET,
            CommissionCatalog::TYPE_MONEY_BON_SET,
            CommissionCatalog::TYPE_STAMP_SET
        ]);
    }

    /**
     * Устанавливаем тираж и номинал
     */
    private function setCirculationAndNominal(): void
    {
        if (!$this->im_id) {
            $nominalNames     = ['Nominal', 'nominal', 'Цена чист.', 'Номинал'];
            $circulationNames = ['Printing Count', 'printing_count', 'Тираж'];

            $data = json_decode($this->data, true);

            $dataItem = $data[0]['params'] ?? null;

            if (is_array($dataItem)) {
                foreach ($dataItem as $field => $value) {
                    if (!$this->nominal && in_array($field, $nominalNames)) {
                        $this->nominal = $value;

                        $this->nominal = $this->parseNominal($this->nominal);
                    }
                    if (!$this->circulation && in_array($field, $circulationNames)) {
                        $this->circulation = (int)str_replace(' ', '', $value);
                    }
                }
            }
        }
    }


    /**
     * Обновляем ед=лемент режима
     */
    private function setModeItem(): void
    {
        $oldModify = $this->oldAttributes['is_modify'] ?? 0;
        if ($oldModify !== $this->is_modify) {
            $currentMode = CommissionCatalogMode::getCurrentMode()->id;

            $modeItem = CommissionCatalogModeItem::find()->where([
                'mode_id'    => $currentMode,
                'catalog_id' => $this->id
            ])->one();

            if ($modeItem) {
                $modeItem->delete();
            }

            if ($this->is_modify) {
                $modeItem = new CommissionCatalogModeItem([
                    'mode_id'    => $currentMode,
                    'catalog_id' => $this->id,
                    'count'      => $this->is_modify_count,
                    'status'     => 1,
                ]);
                $modeItem->save();
            }
        }
    }

    /**
     * @param int   $num
     * @param array $words
     * @return string
     */
    private function num2word(float $num, array $words): string
    {
        if ($num < 1) {
            return ($words[1]);
        }

        $num = abs($num) % 100; // для правильного отображения отрицательного значения
        if ($num > 19) {
            $num = $num % 10;
        }
        switch ($num) {
            case 1:
                {
                    return ($words[0]);
                }
            case 2:
            case 3:
            case 4:
                {
                    return ($words[1]);
                }
            default:
                {
                    return ($words[2]);
                }
        }
    }
}
