<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_catalog_parsed".
 *
 * @property integer           $id
 * @property integer           $catalog_id
 * @property string            $site
 * @property string            $url
 * @property string            $identity
 * @property string            $created_at
 *
 * @property CommissionCatalog $catalog
 */
class CommissionCatalogParsed extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']],
                'value'      => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_catalog_parsed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_id'], 'integer'],
            [['created_at'], 'safe'],
            [['site'], 'string', 'max' => 50],
            [['url', 'identity'], 'string', 'max' => 255],
            [
                ['catalog_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CommissionCatalog::className(),
                'targetAttribute' => ['catalog_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'catalog_id' => 'Catalog ID',
            'site'       => 'Site',
            'url'        => 'Url',
            'identity'   => 'Identity',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(CommissionCatalog::className(), ['id' => 'catalog_id']);
    }

    /**
     * Формируем идентификатор
     *
     * @param string $site
     * @param string $country
     * @param int    $type
     * @param int    $year
     * @param string $nominal
     * @return string
     */
    public static function getIdentityFromData(
        string $site,
        string $country,
        int $type,
        int $year,
        string $nominal
    ): string {
        return ($site ?: 'base') . '_' . $type . '_' . $country . '_' . $year . '_' . $nominal;
    }
}
