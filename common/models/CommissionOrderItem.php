<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "commission_order_item".
 *
 * @property integer         $id
 * @property integer         $order_id
 * @property integer         $im_id
 * @property string          $article
 * @property string          $name
 * @property string          $date_at
 * @property integer         $count
 * @property string          $cost
 * @property string          $sum
 * @property string          $commission
 *
 * @property CommissionOrder $order
 */
class CommissionOrderItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'article', 'name', 'count'], 'required'],
            [['order_id', 'im_id', 'count'], 'integer'],
            [['date_at'], 'safe'],
            [['cost', 'sum', 'commission'], 'number'],
            [['article', 'name'], 'string', 'max' => 255],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CommissionOrder::class,
                'targetAttribute' => ['order_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'order_id'   => 'Order ID',
            'im_id'      => 'Im ID',
            'article'    => 'Article',
            'name'       => 'Name',
            'date_at'    => 'Date At',
            'count'      => 'Count',
            'cost'       => 'Cost',
            'sum'        => 'Sum',
            'commission' => 'Commission',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(CommissionOrder::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogItem()
    {
        return $this->hasOne(CommissionCatalog::class, ['im_id' => 'im_id']);
    }

    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function beforeSave($insert)
    {
        $this->sum = $this->cost * $this->count;

        return parent::beforeSave($insert);
    }

}
