<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_catalog_mode_item".
 *
 * @property integer               $id
 * @property integer               $mode_id
 * @property integer               $catalog_id
 * @property integer               $count
 * @property integer               $status
 * @property string                $created_at
 * @property string                $updated_at
 * @property integer               $created_by
 * @property integer               $updated_by
 *
 * @property CommissionCatalog     $catalogItem
 * @property CommissionCatalogMode $mode
 * @property User                  $createdBy
 * @property User                  $updatedBy
 */
class CommissionCatalogModeItem extends ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_catalog_mode_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mode_id', 'catalog_id', 'count'], 'required'],
            [['mode_id', 'catalog_id', 'count', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [
                ['catalog_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CommissionCatalog::class,
                'targetAttribute' => ['catalog_id' => 'id']
            ],
            [
                ['mode_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CommissionCatalogMode::class,
                'targetAttribute' => ['mode_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'mode_id'    => 'Mode ID',
            'catalog_id' => 'Catalog ID',
            'count'      => 'Count',
            'status'     => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogItem()
    {
        return $this->hasOne(CommissionCatalog::className(), ['id' => 'catalog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMode()
    {
        return $this->hasOne(CommissionCatalogMode::className(), ['id' => 'mode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Обновляем элемент каталога
     */
    public function updateCatalogItem()
    {
        $this->catalogItem->is_modify = $this->status;
        $this->catalogItem->is_modify_count = $this->count;
        $this->catalogItem->save(false);
    }


    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function afterSave($insert, $changeAttributes)
    {
        CommissionCatalogMode::setModeCount($this->mode_id);
    }

    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function afterDelete()
    {
        parent::afterDelete();
        CommissionCatalogMode::setModeCount($this->mode_id);
    }
}
