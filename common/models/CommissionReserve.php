<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "commission_reserve".
 *
 * @property integer $id
 * @property integer $im_id
 * @property string  $article
 * @property string  $name
 * @property integer $count
 * @property string  $sell_day
 * @property integer $to_bay
 * @property integer $days_isset
 */
class CommissionReserve extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_reserve';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['im_id', 'count', 'to_bay', 'days_isset'], 'integer'],
            [['article', 'name', 'count', 'to_bay', 'days_isset'], 'required'],
            [['sell_day'], 'number'],
            [['article', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'im_id'      => 'Im ID',
            'article'    => 'Артикул',
            'name'       => 'Наименование',
            'count'      => 'Осталось',
            'sell_day'   => 'Потребление в день',
            'to_bay'     => 'Единиц нужно',
            'days_isset' => 'Запаса хватит, дней',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogItem()
    {
        return $this->hasOne(CommissionCatalog::class, ['im_id' => 'im_id']);
    }


    /**
     * Обновляем резервы
     *
     * @param array $items
     */
    public static function updateReserve(array $item): void
    {
        if (!$model = CommissionReserve::findOne(['im_id' => $item['im_id']])) {
            $model = new CommissionReserve(['im_id' => $item['im_id']]);
        }
        $model->setAttributes($item);
        $model->save();
    }
}
