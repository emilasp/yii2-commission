<?php

namespace emilasp\commission\common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use emilasp\commission\common\models\queries\MarketCategQuery;
use Yii;

/**
 * This is the model class for table "market_categ_new".
 *
 * @property integer $id
 * @property integer $leftId
 * @property integer $level
 * @property integer $rightId
 * @property integer $rootId
 * @property string $name
 * @property string $slug
 * @property string $slugPath
 * @property string $added
 * @property string $lastmod
 * @property integer $parentId
 * @property string $titleShop
 * @property string $headerShop
 * @property string $metaDescrShop
 * @property string $metaKwShop
 * @property string $shortName
 * @property string $marketSearchSynonyms
 * @property string $description
 * @property integer $isFirstLevel
 * @property string $firstLevelName
 * @property string $slugPathPrev
 * @property string $imageName
 *
 * @property MarketCateg $parent
 */
class MarketCateg extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                 'treeAttribute' => 'rootId',
                 'leftAttribute' => 'leftId',
                 'rightAttribute' => 'rightId',
                 'depthAttribute' => 'level',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new MarketCategQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'market_categ_new';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('mysql');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['added', 'lastmod'], 'required'],
            [['leftId', 'level', 'rightId', 'rootId', 'parentId'], 'integer'],
            [['added', 'lastmod'], 'safe'],
            [['metaKwShop', 'marketSearchSynonyms', 'description'], 'string'],
            [['name', 'titleShop', 'headerShop', 'metaDescrShop', 'shortName', 'firstLevelName', 'slugPathPrev'], 'string', 'max' => 255],
            [['slug', 'slugPath'], 'string', 'max' => 191],
            [['isFirstLevel'], 'string', 'max' => 1],
            [['imageName'], 'string', 'max' => 100],
            [['slugPath'], 'unique'],
            [['imageName'], 'unique'],
            [['parentId'], 'exist', 'skipOnError' => true, 'targetClass' => MarketCateg::className(), 'targetAttribute' => ['parentId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leftId' => 'Left ID',
            'level' => 'Level',
            'rightId' => 'Right ID',
            'rootId' => 'Root ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'slugPath' => 'Slug Path',
            'added' => 'Added',
            'lastmod' => 'Lastmod',
            'parentId' => 'Parent ID',
            'titleShop' => 'Title Shop',
            'headerShop' => 'Header Shop',
            'metaDescrShop' => 'Meta Descr Shop',
            'metaKwShop' => 'Meta Kw Shop',
            'shortName' => 'Short Name',
            'marketSearchSynonyms' => 'Market Search Synonyms',
            'description' => 'Description',
            'isFirstLevel' => 'Is First Level',
            'firstLevelName' => 'First Level Name',
            'slugPathPrev' => 'Slug Path Prev',
            'imageName' => 'Image Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(MarketCateg::className(), ['id' => 'parentId']);
    }
}
