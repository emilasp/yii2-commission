<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_check_price".
 *
 * @property integer           $id
 * @property integer           $catalog_id
 * @property string            $price_our
 * @property string            $price_min
 * @property string            $price_max
 * @property string            $im_id
 * @property string            $price_min_im_id
 * @property integer           $year
 * @property string            $category
 * @property string            $nominal
 * @property integer           $status
 * @property string            $created_at
 * @property string            $updated_at
 *
 * @property CommissionCatalog $catalog
 */
class ImCheckPrice extends ActiveRecord
{
    public const STATUS_WAIT  = 5;
    public const STATUS_HIDED = 6;

    public static $statuses = [
        self::STATUS_ENABLED  => 'В работе',
        self::STATUS_DISABLED => 'Отключен',
        self::STATUS_WAIT     => 'Ожидаем отсутствия конкурентов',
        self::STATUS_HIDED    => 'Выбыл',
    ];


    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_check_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_id', 'year', 'category', 'nominal', 'status'], 'required'],
            [['catalog_id', 'year', 'status'], 'integer'],
            [['price_our', 'price_min', 'price_max'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['im_id', 'price_min_im_id', 'category', 'nominal'], 'string', 'max' => 255],
            [
                ['catalog_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CommissionCatalog::class,
                'targetAttribute' => ['catalog_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'catalog_id'      => 'Каталог',
            'price_our'       => 'Наша цена',
            'price_min'       => 'Минимальная цена',
            'price_max'       => 'Price Max',
            'price_min_im_id' => 'Price Min IM ID',
            'im_id'           => 'Our IM ID',
            'year'            => 'Year',
            'category'        => 'Category',
            'nominal'         => 'Nominal',
            'status'          => 'Status',
            'created_at'      => 'Created At',
            'updated_at'      => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(CommissionCatalog::class, ['id' => 'catalog_id']);
    }


    /**
     * Формируем ссылку на товар в монетнике
     *
     * @param string $imId
     * @return string
     */
    public function getMonetnikSearchUrl(): string
    {
        return "https://www.monetnik.ru/{$this->category}/?name={$this->nominal}&yearFrom={$this->year}&yearTo={$this->year}";
    }

    /**
     * Формируем ссылку на товар в монетнике
     *
     * @param string $imId
     * @return string
     */
    public static function getMonetnikUrl(string $imId): string
    {
        return 'https://www.monetnik.ru/banknoty/mira/aziya/kndr/s-' . $imId . '/';
    }
}
