<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_catalog_mode".
 *
 * @property integer                     $id
 * @property string                      $name
 * @property integer                     $type
 * @property integer                     $count
 * @property integer                     $status
 * @property string                      $created_at
 * @property string                      $updated_at
 *
 * @property CommissionCatalogModeItem[] $items
 */
class CommissionCatalogMode extends ActiveRecord
{
    public const TYPE_DELIVERY = 1;
    public const TYPE_MODIFY   = 0;

    /**
     * Получаем текущий id мода
     *
     * @return int|null
     */
    public static function getCurrentMode(): ?CommissionCatalogMode
    {
        return CommissionCatalogMode::findOne(['status' => self::STATUS_ENABLED]) ?? null;
    }

    /**
     * Update mode
     *
     * @param $modeId
     */
    public static function setMode(int $modeId, int $modeReset = null): void
    {
        if ($modeReset) {
            CommissionCatalogModeItem::deleteAll(['mode_id' => $modeId]);
            $mode        = self::findOne($modeId);
            $mode->count = 0;
            $mode->save(false);

            // update all catalog statuses to isset(by im_id)
            CommissionCatalog::updateAll(['status' => 1], ['im_id' => null]);
            CommissionCatalog::updateAll(['status' => 2], ['IS NOT', 'im_id', null]);
        }
        $items = CommissionCatalogModeItem::findAll(['mode_id' => $modeId]);

        CommissionCatalogMode::updateAll(['status' => 0], ['status' => 1]);
        CommissionCatalogMode::updateAll(['status' => 1, 'count' => count($items)], ['id' => $modeId]);


        CommissionCatalog::updateAll(['is_modify' => 0, 'is_modify_count' => 0], ['is_modify' => 1]);

        foreach ($items as $item) {
            $item->updateCatalogItem();
        }
    }

    /**
     * Обновляем количество элементов
     *
     * @param $modeId
     */
    public static function setModeCount($modeId): void
    {
        $count = CommissionCatalogModeItem::find()->where(['mode_id' => $modeId])->count();
        CommissionCatalogMode::updateAll(
            ['count' => $count],
            ['id' => $modeId]
        );
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_catalog_mode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'count', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Name',
            'type'       => 'Type',
            'count'      => 'Count',
            'status'     => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(CommissionCatalogModeItem::class, ['mode_id' => 'id']);
    }
}
