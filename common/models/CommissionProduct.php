<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_product".
 *
 * @property integer $id
 * @property integer $guid
 * @property integer $category_id
 * @property string  $article
 * @property string  $name
 * @property integer $count
 * @property string  $cost
 * @property string  $cost_r
 * @property string  $price
 * @property string  $price_sale
 * @property string  $description
 * @property string  $comment
 * @property string  $keywords
 * @property string  $url
 * @property string  $url_base
 * @property string  $data
 * @property integer $status
 * @property string  $created_at
 * @property string  $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User    $createdBy
 * @property User    $updatedBy
 */
class CommissionProduct extends ActiveRecord
{
    public const        TYPE_SINGLE     = 1;
    public const        TYPE_BLOCK_LINE = 2;
    public const        TYPE_BLOCK      = 3;
    public const        TYPE_SET        = 4;

    public static $types = [
        self::TYPE_SINGLE     => 'Single',
        self::TYPE_BLOCK_LINE => 'Block line',
        self::TYPE_BLOCK      => 'Block',
        self::TYPE_SET        => 'Set',
    ];

    public const MATERIAL_PAPER   = 1;
    public const MATERIAL_PLASTIC = 2;
    public const MATERIAL_METAL   = 3;

    public static $materials = [
        self::MATERIAL_PAPER   => 'Paper',
        self::MATERIAL_PLASTIC => 'Plastic',
        self::MATERIAL_METAL   => 'Metal',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'status'], 'required'],
            [
                [
                    'guid',
                    'category_id',
                    'count',
                    'data_printing_count',
                    'data_year',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['cost', 'cost_r', 'price', 'price_sale'], 'number'],
            [['description', 'comment', 'keywords'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['data'], 'string'],
            [
                [
                    'article',
                ],
                'string',
                'max' => 20
            ],
            [['name', 'url', 'url_base'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::class,
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'guid'        => 'GUID',
            'category_id' => 'Category ID',
            'article'     => 'Артикул',
            'name'        => 'Наименование',
            'count'       => 'Количество',
            'cost'        => 'Стоимость',
            'cost_r'      => 'Стоимость(гаш.)',
            'price'       => 'Цена(ИМ) стра',
            'price_sale'  => 'Цена(ИМ)',
            'description' => 'Описание',
            'comment'     => 'Заметки',
            'keywords'    => 'Ключевики',
            'url'         => 'Url 1',
            'url_base'    => 'Url(марка)',
            'data'        => 'Data',
            'status'      => 'Статус',
            'created_at'  => 'Создан',
            'updated_at'  => 'Изменен',
            'created_by'  => 'Created By',
            'updated_by'  => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }
}
