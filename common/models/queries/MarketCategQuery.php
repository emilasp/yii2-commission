<?php
namespace emilasp\commission\common\models\queries;

use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * Class MarketCateg
 * @package emilasp\commission\common\models\queries
 */
class MarketCategQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}