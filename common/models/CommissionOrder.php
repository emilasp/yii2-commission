<?php

namespace emilasp\commission\common\models;

use DateTime;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\helpers\DateHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "commission_order".
 *
 * @property integer               $id
 * @property string                $number
 * @property string                $date_at
 * @property integer               $count
 * @property string                $sum
 * @property string                $sum_payment
 * @property string                $commission
 * @property integer               $is_payed
 * @property integer               $is_monetized
 * @property string                $status
 *
 * @property CommissionOrderItem[] $commissionOrderItems
 */
class CommissionOrder extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_at'], 'safe'],
            [['count', 'status'], 'required'],
            [['count', 'is_payed', 'is_monetized'], 'integer'],
            [['sum', 'sum_payment', 'commission'], 'number'],
            [['number', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'number'       => 'Номер',
            'date_at'      => 'Дата',
            'count'        => 'Количество',
            'sum'          => 'Сумма',
            'sum_payment'  => 'Сумма(выплата)',
            'commission'   => 'Коммиссия',
            'is_payed'     => 'Оплачен',
            'is_monetized' => 'Выплачен',
            'status'       => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissionOrderItems()
    {
        return $this->hasMany(CommissionOrderItem::class, ['order_id' => 'id']);
    }

    /**
     * Update order by items
     *
     * @param array $items
     * @return CommissionOrder
     */
    public static function updateOrderFromItems(array $items): CommissionOrder
    {
        $firstItem = $items[0];

        if (!$order = CommissionOrder::findOne(['number' => $firstItem['number']])) {
            $order = new CommissionOrder(['number' => $firstItem['number']]);
        }

        $order->setAttributes($firstItem);
        $order->save();

        $count      = 0;
        $sum        = 0;
        $sumPayment = 0;
        $commission = 0;
        foreach ($items as $item) {
            if (!$orderItem = CommissionOrderItem::findOne(['order_id' => $order->id, 'article' => $item['article']])) {
                $orderItem = new CommissionOrderItem(['order_id' => $order->id, 'article' => $item['article']]);
            }
            $orderItem->setAttributes($item);
            $orderItem->save();

            $count      += $orderItem->count;
            $sum        += $orderItem->sum;
            $sumPayment += $orderItem->sum - $orderItem->commission;
            $commission += $orderItem->commission;
        }

        $order->count       = $count;
        $order->sum         = $sum;
        $order->sum_payment = $sumPayment;
        $order->commission  = $commission;
        $order->save();

        return $order;
    }

    /**
     * Получаем данные по месяцам
     *
     * @return array
     */
    public static function getAvgSum(string $chartStart, string $chartEnd, string $formatDate = 'W'): array // MM
    {
        $chartStart = date('Y-m-d', strtotime($chartStart));
        $chartEnd   = date('Y-m-d', strtotime($chartEnd));

        $sql  = <<<SQL
            SELECT
              date_at::DATE as date,
              AVG(AvgValue) as sum
            FROM (
                   SELECT
                     date_at,
                     SUM(sum_payment) as AvgValue
                   FROM commission_order
                   WHERE date_at::DATE >= '{$chartStart}'::DATE AND date_at::DATE <= '{$chartEnd}'::DATE
                   GROUP BY date_at
                 ) as tmp
            GROUP BY date_at::DATE
            ORDER BY date_at::DATE DESC
SQL;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $maxDate = $data[0]['date'] ?? null;
        $minDate = $data[count($data) - 1]['date'] ?? null;

        $data = ArrayHelper::map($data, 'date', 'sum');

        $datesFull = DateHelper::getDatesByRange($minDate, $maxDate, true);

        $newData = [];
        foreach ($datesFull as $date => $tmp) {
            $newData[date($formatDate, strtotime($date))] = $data[$date] ?? 0;
        }

        array_walk($newData, function (&$item1) {
            $item1 = (int)$item1;
        });

        return array_reverse($newData);
    }

    /**
     * Получаем суммы по дням
     *
     * @return array
     */
    public static function getSumByPeriod(
        string $chartStart,
        string $chartEnd,
        string $formatDate = 'd.m',
        array $types = []
    ): array {
        $chartStart = date('Y-m-d', strtotime($chartStart));
        $chartEnd   = date('Y-m-d', strtotime($chartEnd));

        if ($types) {
            $types = implode(',', $types);
            $sql   = <<<SQL
               SELECT
                  orders.date_at::DATE as date,
                  round(SUM(item.sum) - SUM(item.sum)/100*28.85, 0) as sum
                FROM commission_order_item item
                INNER JOIN commission_order orders ON orders.id=item.order_id
                INNER JOIN commission_catalog catalog ON catalog.im_id=item.im_id
                WHERE orders.date_at::DATE >= '{$chartStart}'::DATE AND orders.date_at::DATE <= '{$chartEnd}'::DATE
                AND catalog.type IN ({$types})
                GROUP BY orders.date_at::DATE
                ORDER BY orders.date_at::DATE DESC
SQL;
        } else {
            $sql = <<<SQL
               SELECT
                 date_at::DATE as date,
                 SUM(sum_payment) as sum
               FROM commission_order orders
               WHERE date_at::DATE >= '{$chartStart}'::DATE AND date_at::DATE <= '{$chartEnd}'::DATE
               GROUP BY date_at::DATE
               ORDER BY date_at::DATE DESC
SQL;
        }

        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $maxDate = $data[0]['date'] ?? null;
        $minDate = $data[count($data) - 1]['date'] ?? null;

        $data = ArrayHelper::map($data, 'date', 'sum');

        $datesFull = DateHelper::getDatesByRange($minDate, $maxDate, true);

        $newData = [];
        foreach ($datesFull as $date => $tmp) {
            $newData[date($formatDate, strtotime($date))] = $data[$date] ?? 0;
        }

        array_walk($newData, function (&$item1) {
            $item1 = (int)$item1;
        });

        return array_reverse($newData);
    }

    /**
     * Получаем данные по месяцам
     *
     * @return array
     */
    public static function getAvgSumWithGroup(string $chartStart, string $chartEnd, string $by = 'WW'): array // MM
    {
        $chartStart = date('Y-m-d', strtotime($chartStart));
        $chartEnd   = date('Y-m-d', strtotime($chartEnd));

        $sql = <<<SQL
            SELECT
              to_char(date_at, '{$by}') as date,
              AVG(AvgValue) as sum
            FROM (
                   SELECT
                     date_at,
                     SUM(sum_payment) as AvgValue
                   FROM commission_order
                   WHERE date_at::DATE >= '{$chartStart}'::DATE AND date_at::DATE <= '{$chartEnd}'::DATE
                   GROUP BY date_at
                 ) as tmp
            GROUP BY to_char(date_at, '{$by}')
            ORDER BY to_char(date_at, '{$by}') DESC
SQL;
        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $data = ArrayHelper::map($data, 'date', 'sum');

        array_walk($data, function (&$item1) {
            $item1 = (int)$item1;
        });

        return $data;
    }

    /**
     * Получаем суммы по дням
     *
     * @return array
     */
    public static function getSumByPeriodWithGroup(string $chartStart, string $chartEnd, string $by = 'yy.mm.dd', array $types = []): array
    {
        $chartStart = date('Y-m-d', strtotime($chartStart));
        $chartEnd   = date('Y-m-d', strtotime($chartEnd));

        if ($types) {
            $types = implode(',', $types);
            $sql = <<<SQL
           SELECT
              to_char(orders.date_at, '{$by}') as date,
              round(SUM(item.sum) - SUM(item.sum)/100*28.85, 0) as sum
            FROM commission_order_item item
            INNER JOIN commission_order orders ON orders.id=item.order_id
            INNER JOIN commission_catalog catalog ON catalog.im_id=item.im_id
            WHERE orders.date_at::DATE >= '{$chartStart}'::DATE AND orders.date_at::DATE <= '{$chartEnd}'::DATE
            AND catalog.type IN ({$types})
            GROUP BY to_char(orders.date_at, '{$by}')
            ORDER BY to_char(orders.date_at, '{$by}') DESC
SQL;
        } else {
            $sql = <<<SQL
           SELECT
             to_char(date_at, '{$by}') as date,
             SUM(sum_payment) as sum
           FROM commission_order orders
           WHERE date_at::DATE >= '{$chartStart}'::DATE AND date_at::DATE <= '{$chartEnd}'::DATE
           GROUP BY to_char(date_at, '{$by}')
           ORDER BY to_char(date_at, '{$by}') DESC
SQL;
        }

        $data = Yii::$app->db->createCommand($sql)->queryAll();

        $data = ArrayHelper::map($data, 'date', 'sum');

        array_walk($data, function (&$item1) {
            $item1 = (int)$item1;
        });

        return $data;
    }


    /**
     * Получаем дату по номеру недели
     *
     * @param $week
     * @param $year
     * @return mixed
     */
    public static function getDateByMonthNumber($week)
    {
        $year     = date('Y');
        $dateTime = new DateTime();
        $dateTime->setISODate($year, $week);
        $dateTime->modify('+6 days');
        return $dateTime->format('m.d');
    }
}
