<?php

namespace emilasp\commission\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\commission\common\models\ImCheckPrice;

/**
 * ImCheckPriceSearch represents the model behind the search form of `emilasp\commission\common\models\ImCheckPrice`.
 */
class ImCheckPriceSearch extends ImCheckPrice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'catalog_id', 'year', 'status'], 'integer'],
            [['price_our', 'price_min', 'price_max'], 'number'],
            [['im_id', 'price_min_im_id', 'category', 'nominal', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImCheckPrice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'catalog_id' => $this->catalog_id,
            'price_our' => $this->price_our,
            'price_min' => $this->price_min,
            'price_max' => $this->price_max,
            'year' => $this->year,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'im_id', $this->im_id])
            ->andFilterWhere(['like', 'price_min_im_id', $this->price_min_im_id])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'nominal', $this->nominal]);

        return $dataProvider;
    }
}
