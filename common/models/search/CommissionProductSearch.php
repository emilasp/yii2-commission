<?php

namespace emilasp\commission\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\commission\common\models\CommissionProduct;

/**
 * CommissionProductSearch represents the model behind the search form of
 * `emilasp\commission\common\models\CommissionProduct`.
 */
class CommissionProductSearch extends CommissionProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'guid',
                    'category_id',
                    'count',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [
                [
                    'article',
                    'name',
                    'data',
                    'description',
                    'comment',
                    'keywords',
                    'url',
                    'url_base',
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ],
            [['cost', 'cost_r', 'price', 'price_sale'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommissionProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                  => $this->id,
            'guid'                => $this->guid,
            'category_id'         => $this->category_id,
            'count'               => $this->count,
            'cost'                => $this->cost,
            'cost_r'              => $this->cost_r,
            'price'               => $this->price,
            'price_sale'          => $this->price_sale,
            'status'              => $this->status,
            'created_at'          => $this->created_at,
            'updated_at'          => $this->updated_at,
            'created_by'          => $this->created_by,
            'updated_by'          => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'url_base', $this->url_base])
            ->andFilterWhere(['like', 'data', $this->data]);

        return $dataProvider;
    }
}
