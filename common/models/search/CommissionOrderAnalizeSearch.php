<?php

namespace emilasp\commission\common\models\search;

use emilasp\commission\common\models\CommissionCatalog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\commission\common\models\CommissionOrderAnalize;

/**
 * CommissionOrderAnalizeSearch represents the model behind the search form of
 * `emilasp\commission\common\models\CommissionOrderAnalize`.
 */
class CommissionOrderAnalizeSearch extends CommissionOrderAnalize
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['im_id', 'count', 'type'], 'integer'],
            [['cost', 'sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommissionOrderAnalize::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'im_id' => $this->im_id,
            'count' => $this->count,
            'cost'  => $this->cost,
            'sum'   => $this->sum,
        ]);

        $type = CommissionCatalog::$typeGroups[$this->type] ?? $this->type;
        $query->andFilterWhere(['type' => $type]);

        return $dataProvider;
    }
}
