<?php

namespace emilasp\commission\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\commission\common\models\CommissionCatalog;

/**
 * CommissionCatalogSearch represents the model behind the search form of
 * `emilasp\commission\common\models\CommissionCatalog`.
 */
class CommissionCatalogSearch extends CommissionCatalog
{
    public $search;
    public $years = [];
    public $is_live;
    public $is_series;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'active',
                    'status',
                    'type',
                    'year',
                    'created_by',
                    'updated_by',
                    'is_modify',
                    'is_live',
                    'is_series'
                ],
                'integer'
            ],
            [
                [
                    'site',
                    'article',
                    'name',
                    'description',
                    'url',
                    'nominal',
                    'circulation',
                    'data',
                    'data_params',
                    'data_search',
                    'created_at',
                    'updated_at',
                    'search',
                    'years'
                ],
                'safe'
            ],
            [['cost', 'cost_r', 'cost_sol', 'im_id'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommissionCatalog::find();
        //$query->orderBy('year DESC, updated_at DESC')
        // add conditions that should always apply here

        $defaultOrder = !$params ? ['created_at' => SORT_DESC] : ['year' => SORT_DESC, 'updated_at' => SORT_DESC] ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => $defaultOrder]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'im_id'      => $this->im_id,
            'cost'       => $this->cost,
            'cost_r'     => $this->cost_r,
            'year'       => $this->year,
            'status'     => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'is_modify'  => $this->is_modify,
        ]);

        $query->andFilterWhere(['ilike', 'site', $this->site])
            ->andFilterWhere(['ilike', 'nominal', $this->nominal])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'url', $this->url])
            ->andFilterWhere(['ilike', 'data', $this->data])
            ->andFilterWhere(['ilike', 'data_search', $this->data_search]);

        $type = self::$typeGroups[$this->type] ?? $this->type;
        $query->andFilterWhere(['type' => $type]);


        if ($this->is_modify) {
            $dataProvider->sort->defaultOrder = ['updated_at' => SORT_DESC];
        }

        if ($this->article) {
            $dataProvider->sort->defaultOrder = ['year' => SORT_DESC, 'id' => SORT_DESC];
            $dataArticle                      = explode(',', $this->article);

            $condition = ['or'];
            foreach ($dataArticle as $word) {
                $word        = trim($word, " \t\n\r\0\x0B ");
                $condition[] = ['like', 'article_full', $word];
            }
            $query->andFilterWhere($condition);
        }

        if ($this->article || $this->im_id) {
            $this->search = null;
        }

        if ($this->search) {
            if (strpos($this->search, '"') === false) {
                $dataSearch = explode(' ', $this->search);
            } else {
                $dataSearch = [str_replace('"', '', $this->search)];
            }
            foreach ($dataSearch as $search) {
                $query->andWhere("data_search ilike '%{$search}%'");
            }
        }
        if ($this->is_live) {
            $query->andWhere(['IS NOT', 'im_id', null]);
        }

        if ($this->is_series) {
            $query->andWhere("(article ilike '%,%' OR article ilike '%-%')");
        }

        if ($this->years && count($this->years) === 2 && ($this->years[0] || $this->years[1])) {
            $this->years[0] = $this->years[0] ?: 1900;
            $this->years[1] = $this->years[1] ?: date('Y');

            $years = implode(',', range($this->years[0], $this->years[1]));
            $query->andWhere("year IN ({$years})");
        }

        return $dataProvider;
    }

    /**
     * Get array years
     *
     * @return array
     */
    public static function getYearsArray(): array
    {
        $years = [];
        foreach (range(date('Y'), 1900) as $year) {
            $years[$year] = $year;
        }

        return $years;
    }
}
