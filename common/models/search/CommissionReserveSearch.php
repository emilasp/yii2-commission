<?php

namespace emilasp\commission\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\commission\common\models\CommissionReserve;

/**
 * CommissionReserveSearch represents the model behind the search form of
 * `emilasp\commission\common\models\CommissionReserve`.
 */
class CommissionReserveSearch extends CommissionReserve
{
    public $site;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'im_id', 'count', 'to_bay', 'days_isset'], 'integer'],
            [['article', 'name', 'site'], 'safe'],
            [['sell_day'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommissionReserve::find();
        $query->joinWith('catalogItem');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['to_bay' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['site'] = [
            'asc' => ['commission_catalog.site' => SORT_ASC],
            'desc' => ['commission_catalog.site' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'im_id'      => $this->im_id,
            'count'      => $this->count,
            'sell_day'   => $this->sell_day,
            'to_bay'     => $this->to_bay,
            'days_isset' => $this->days_isset,
        ]);

        $query->andFilterWhere(['ilike', 'commission_catalog.site', $this->site]);
        $query->andFilterWhere(['ilike', 'article', $this->article])
            ->andFilterWhere(['ilike', 'name', $this->name]);




        return $dataProvider;
    }
}
