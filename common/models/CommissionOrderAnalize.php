<?php

namespace emilasp\commission\common\models;

use emilasp\core\components\base\ActiveRecord;
use Yii;

/**
 * This is the model class for table "commission_order_analize_view".
 *
 * @property integer $im_id
 * @property integer $type
 * @property integer $count
 * @property string  $cost
 * @property string  $sum
 */
class CommissionOrderAnalize extends ActiveRecord
{
    public static function primaryKey()
    {
        return ['im_id'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commission_order_analize_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['im_id', 'count', 'type'], 'integer'],
            [['cost', 'sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'im_id' => 'Im ID',
            'type'  => 'type',
            'count' => 'Count',
            'cost'  => 'Cost',
            'sum'   => 'Sum',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(CommissionCatalog::class, ['im_id' => 'im_id']);
    }
}
