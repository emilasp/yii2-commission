<?php

namespace emilasp\commission\common\components;

use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class MeshokComponent
 * @package emilasp\commission\common\components
 */
class MeshokComponent extends Component
{
    public const DATA_FIELDS = [];

    private const CACHE_KEY_CSRF_TOKEN    = 'key:csrf';
    private const CACHE_KEY_CSRF_COOKIES  = 'key:csrfCookies';
    private const CACHE_KEY_LOGIN_COOKIES = 'key:loginCookies';

    private const ACTION_LOGIN_CSRF = '/login';
    private const ACTION_LOGIN      = '/login_check';


    public $baseUrl = 'https://meshok.net/';
    public $login;
    public $password;


    /**
     * Auth in IM
     *
     * @return bool
     */
    public function login(): bool
    {
        //$pageCsrf = $this->getCsrfToken(self::ACTION_LOGIN_CSRF);

        $response = $this->sendRequest(
            self::ACTION_LOGIN,
            [
                'ajaxCounter' => 1,
                'ajax'        => 1,
                'action'      => 'login',
                'selector'    => 'a[rel=login]',
                'LOGIN'       => 'emilasp@mail.ru',
                'password'    => 'fgd54dDFg',
            ], 'post', self::CACHE_KEY_CSRF_COOKIES . self::ACTION_LOGIN_CSRF);


        //if ($response->isOk) {
        $cookies = ArrayHelper::toArray($response->getCookies());

        //$cookies = $response->headers->get('set-cookie');


        $this->setInCache(self::CACHE_KEY_LOGIN_COOKIES, $cookies);
        //}

        return true;
    }


    /**
     * @return string
     */
    private function getCsrfToken(string $action): string
    {
        $csrfToken = $this->getFromCache(self::CACHE_KEY_CSRF_TOKEN . $action);

        if (!$csrfToken) {
            $response = $this->sendRequest($action);

            if ($response->isOk) {
                $cookies = ArrayHelper::toArray($response->getCookies());

                $crawler   = new Crawler($response->content);
                $inputCsrf = $crawler->filter('form [name="_csrf_token"]');

                $csrfToken = $inputCsrf->attr('value');

                $this->setInCache(self::CACHE_KEY_CSRF_TOKEN . $action, $csrfToken);
                $this->setInCache(self::CACHE_KEY_CSRF_COOKIES . $action, $cookies);
            }
        }
        return $csrfToken;
    }


    /**
     * @param string $action
     * @param array  $data
     * @param string $method
     * @return array
     */
    private function sendRequest(
        string $action,
        array $data = [],
        string $method = 'get',
        string $cookieName = null,
        array $headers = [
            'Origin'                                          => 'https://www.monetnik.ru',
            'Upgrade-Insecure-Requests'                       => 1,
            //'Content-Type'                                    => 'application/x-www-form-urlencoded',
            'User-Agent'                                      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
            'X-DevTools-Emulate-Network-Conditions-Client-Id' => 'B72A04C0215FD004F5A13DE1FE7A5979',
            'Accept'                                          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            //'Referer'                                         => 'https://www.monetnik.ru/login',
            //'Accept-Encoding'                                 => 'gzip, deflate, br',
            'Accept-Language'                                 => 'ru,en;q=0.9,en-US;q=0.8',
        ]
    ): Response {
        sleep(0.1);

        $client  = new Client([
            'baseUrl'   => $this->baseUrl,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($action)
            ->setData($data)
            ->setHeaders($headers);

        $request->setOptions([
            'encoding' => ''
        ]);

        if ($cookieName) {
            $cookies = $this->getFromCache($cookieName);
            $request->setCookies($cookies);
        }

        $response = $request->send();
        return $response;
    }


    /**
     * Save cookies
     *
     * @param string       $key
     * @param array|string $cookies
     */
    private function setInCache(string $key, $value): void
    {
        Yii::$app->cache->set($key, $value, 3600);
    }

    /**
     * @param string $key
     * @return mixed
     */
    private function getFromCache(string $key)
    {
        return Yii::$app->cache->get($key);
    }
}
