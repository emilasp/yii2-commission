<?php

namespace emilasp\commission\common\components\export;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionCatalogMode;
use emilasp\core\helpers\StringHelper;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\Component;
use yii\web\View;

/**
 * Class DeliveryExportPdf
 * @package emilasp\commission\common\components\export
 */
class DeliveryExportPdf extends Component
{
    private const TEMPLATES_PATH = '@vendor/emilasp/yii2-commission/common/components/export/templates';

    /**
     * Генерируем документ
     *
     * @param CommissionCatalogMode $mode
     * @param int                   $status
     * @return null|string
     */
    public function generate(CommissionCatalogMode $mode, int $status): ?string
    {
        $data = [];
        foreach ($mode->items as $item) {
            if ($item->catalogItem->status === $status) {
                $data[] = [
                    'name'    => StringHelper::truncateString($item->catalogItem->title, 120),
                    'count'   => $item->catalogItem->is_modify_count,
                    'article' => $item->catalogItem->im_id,
                    'status'  => $status === CommissionCatalog::STATUS_NEW ? 'новый' : 'в продаже',
                ];
            }
        }

        $parts = array_chunk($data, 24);

        $pages = [];
        foreach ($parts as $data) {
            $path = Yii::getAlias(self::TEMPLATES_PATH) . DIRECTORY_SEPARATOR . 'delivery.php';

            $pages[] = (new View())->renderFile($path, ['items' => $data]);

        }

        $content = implode('', $pages);


        $content = '<!DOCTYPE html><html dir="ltr" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>'
            . $content . '</body></html>';

        $fileName = Yii::getAlias('@backend/runtime/deliveryExport_' . date('d-m-Y_h-i') . '.pdf');

        ini_set("pcre.backtrack_limit", "100000000");

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode'        => Pdf::MODE_UTF8,
            'format'      => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_FILE,
            'filename'    => $fileName,
            'content'     => $content,
            'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline'   => '
             html,
        body {
            height: 100%;
            border: 1px solid #ccc;
        }
      
        table {
            border-collapse: collapse;
            table-layout: fixed;
        }
        th { 
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid #ccc; /* Граница вокруг ячеек */
        }
        td { 
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid #ccc; /* Граница вокруг ячеек */
            white-space: nowrap; /* Отменяем перенос текста */
            overflow: hidden; 
        }
        .small {
            font-size:6px;
        }
        .item-name {
            font-size:8px;
        }
        
        .border-black {
            border-bottom: 1px solid black;
        }
        .border-black-top {
            border-top: 1px solid black;
        }
   
        #container {
            min-height: 100%;
            position: relative;
        }

        /*Стили для блока с шапкой*/
        #header {
            background: #222;
            padding: 10px;
        }

        /*Стили для центральной части*/
        #body {
            padding-bottom: 70px; /* Высота блока "footer" */
        }

        /*Стили для нижней части*/
        #footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 70px; /* Высота блока "footer" */
            background: #66ccff;
        }
            
            
            
         body {
            width: 210mm;
            margin-left: auto;
            margin-right: auto;
            border: 1px #efefef solid;
            font-size: 10px;
        }

        table.invoice_bank_rekv {
            border-collapse: collapse;
            border: 1px solid;
        }

        table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td {
            border: 1px solid;
        }

        table.invoice_items {
            border: 1px solid;
            border-collapse: collapse;
        }

        table.invoice_items td, table.invoice_items th {
            border: 1px solid;
        }
            ',
            'options'     => ['title' => 'Krajee Report Title'],
            'methods'     => [
                'SetHeader' => ['Расписка о получении товара'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        // return the pdf output as per the destination setting
        $pdf->render();

        return $fileName;
    }

}
