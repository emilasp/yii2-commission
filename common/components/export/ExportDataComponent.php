<?php

namespace emilasp\commission\common\components\export;

use backend\modules\companies\helpers\CompanyHelper;
use backend\modules\companies\models\Company;
use backend\modules\counteragents\models\Counteragent;
use backend\modules\counteragents\models\CounteragentDocumentGenerated;
use backend\modules\counteragents\models\CounteragentDocumentGeneratedType;
use backend\modules\counteragents\models\CounteragentLevyPaid;
use emilasp\core\helpers\DateHelper;
use emilasp\core\helpers\StringHelper;
use IntlDateFormatter;
use kartik\mpdf\Pdf;
use Mpdf\Mpdf;
use Yii;
use yii\base\Component;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 *:q Export as file
 *
 * Class ExportDataComponent
 * @package backend\modules\counteragents\components\documentGenerator
 */
class ExportDataComponent extends Component
{
    /**
     * Формируем и отправляем файл как CSV таблицу
     *
     * @param Query    $query
     * @param array    $fields
     * @param string   $fileName
     * @param int|null $orderColumn
     */
    public function sendAsCsv(Query $query, array $fields, string $fileName = 'export_', int $orderColumn = null): void
    {
        $data = [];

        /** Собираем данные */
        $batch = 500;
        foreach ($query->batch($batch) as $indexBatch => $models) {
            foreach ($models as $model) {
                $itemData = [];
                foreach ($fields as $fieldTitle => $field) {
                    if (is_callable($field)) {
                        $value = $field($model);
                    } else {
                        $value = ArrayHelper::getValue($model, $field);
                    }
                    $itemData[] = str_replace(';', '|', $value);
                }

                $data[] = $itemData;
            }
        }

        /** Сортируем */
        if ($orderColumn) {
            array_multisort(ArrayHelper::getColumn($data, $orderColumn), SORT_DESC, $data);
        }

        /** Добавляем заголовки */
        $keys = array_keys($fields);
        if (is_string($keys[0])) {
            array_unshift($data, $keys);
        }

        /** Формируем CSV */
        foreach ($data as $index => $item) {
            $data[$index] = implode(';', $item);
        }

        Yii::$app->response->sendContentAsFile(
            implode("\n", $data), $fileName . date('d-m-Y_h-i') . '.csv'
        );
    }
}
