<?php

namespace backend\modules\counteragents\components\documentGenerator;

use backend\modules\companies\helpers\CompanyHelper;
use backend\modules\companies\models\Company;
use backend\modules\counteragents\models\Counteragent;
use backend\modules\counteragents\models\CounteragentDocumentGenerated;
use backend\modules\counteragents\models\CounteragentDocumentGeneratedType;
use backend\modules\counteragents\models\CounteragentLevyPaid;
use emilasp\core\helpers\DateHelper;
use emilasp\core\helpers\StringHelper;
use IntlDateFormatter;
use kartik\mpdf\Pdf;
use Mpdf\Mpdf;
use Yii;
use yii\base\Component;
use yii\db\ActiveRecord;
use yii\web\View;

/**
 * Компонент предоставляет функционал для генерации документов
 *
 * Class DocAccountGenerator
 * @package backend\modules\counteragents\components\documentGenerator
 */
class DocAccountGenerator extends ADocumentGenerator
{
    /**
     * Генерируем Счёт
     *
     * @param Company      $company
     * @param Counteragent $counteragent
     * @return string
     */
    public function generate(
        Company $company,
        Counteragent $counteragent,
        int $id = null,
        bool $fullAccountMonths = false,
        array $customPeriod = null,
        int $levyType = CounteragentLevyPaid::TYPE_MEMBER
    ): ?string {
        $levies = $counteragent->getLevies($levyType)->all();

        $startBalanceBase = $levies ? (float)$levies[0]->balance : 0;

        /** Отсавляем только начисления */
        foreach ($levies as $indexLevy => $levy) {
            if ($levy->status === CounteragentLevyPaid::STATUS_PAYED) {
                unset($levies[$indexLevy]);
            }
        }
        $levies = array_values($levies);

        $levyTypeTitleCode = $levyType === CounteragentLevyPaid::TYPE_MEMBER_ODO ? 'ОДО' : 'ЧВ';

        if ($levies) {
            $dateNow       = date('Y-m-01 00:00:00');
            $currentLevyAt = date('Y-m-01 00:00:00', strtotime($levies[0]->levy_at));
            if ($currentLevyAt === $dateNow) {
                if (!$fullAccountMonths) {
                    array_splice($levies, 0, 1);
                }
            }
        }


        $data = [];
        if ($id && $document = CounteragentDocumentGenerated::findOne($id)) {
            $data = json_decode($document->data, true);
        } else {
            $lastLevyAt = $levies[0]->levy_at ?? null;
            $document   = CounteragentDocumentGenerated::find()
                ->where([
                    'counteragent_id' => $counteragent->id,
                    'type_id'         => CounteragentDocumentGenerated::TYPE_ACCOUNT,
                    'status'          => CounteragentDocumentGenerated::STATUS_ENABLED,
                    'date_at'         => date('Y-m-01 00:00:00', strtotime($lastLevyAt)),
                ])->one();

            if (!$document) {
                $document = new CounteragentDocumentGenerated([
                    'counteragent_id' => $counteragent->id,
                    'type_id'         => CounteragentDocumentGenerated::TYPE_ACCOUNT,
                    'status'          => CounteragentDocumentGenerated::STATUS_ENABLED,
                    'date_at'         => date('Y-m-01 00:00:00', strtotime($lastLevyAt)),
                ]);
                $document->setNewNumber();
            }


            $data['number'] = $document->getFullNumber($levyType);
            $data['date']   = date('d.m.Y');

            $data['company']['name']             = $company->name;
            $data['company']['fullName']         = $company->fullName;
            $data['company']['inn']              = $company->inn;
            $data['company']['kpp']              = $company->kpp;
            $data['company']['organ']            = $company->organ;
            $data['company']['organ_fio']        = $company->organ_fio;
            $data['company']['organ_fio_short']  = CompanyHelper::shortFio($company->organ_fio);
            $data['company']['accountant_short'] = CompanyHelper::shortFio($company->accountant);

            $data['company']['account']['bank_name']    = $company->account->bank_name;
            $data['company']['account']['bik']          = $company->account->bik;
            $data['company']['account']['account']      = $company->account->account;
            $data['company']['account']['loro_account'] = $company->account->loro_account;

            $data['counteagent']['fullName'] = $counteragent->fullName;

            $sum      = 0;
            $products = [];

            if ($customPeriod) {
                if ($date = $customPeriod['end']) {
                    $date    = date('Y-m-01', strtotime($date));
                    $dateEnd = date('Y-m-01', strtotime($customPeriod['start']));

                    while ($date >= $dateEnd) {
                        $formatter = new IntlDateFormatter(
                            'ru_RU',
                            IntlDateFormatter::LONG,
                            IntlDateFormatter::NONE,
                            'Europe/Moscow',
                            null,
                            "LLLL"
                        );
                        $formatter->parse($date);
                        $monthString = $formatter->format(strtotime($date));

                        $products[] = [
                            'name'     => 'Членский взнос(' . $levyTypeTitleCode . ') за ' . $monthString . ' '
                                . date('Y', strtotime($date)) . ' г',
                            'quantity' => 1,
                            'unit'     => 'мес',
                            'price'    => (float)$customPeriod['cost'],
                        ];

                        $sum += (float)$customPeriod['cost'];

                        $date = date('Y-m-01', strtotime($date . ' -1 month'));
                    }
                } else {
                    $products[] = [
                        'name'     => 'Членский взнос(' . $levyTypeTitleCode . ')',
                        'quantity' => 1,
                        'unit'     => '',
                        'price'    => (float)$customPeriod['cost'],
                    ];

                    $sum += (float)$customPeriod['cost'];
                }
            } else {
                if (!count($levies)) {
                    return null;
                }

                $startBalance = $startBalanceBase;

                foreach ($levies as $levy) {
                    if ($startBalance >= 0) {
                        break;
                    }

                    $formatter = new IntlDateFormatter(
                        'ru_RU',
                        IntlDateFormatter::LONG,
                        IntlDateFormatter::NONE,
                        'Europe/Moscow',
                        null,
                        "LLLL"
                    );
                    $formatter->parse($levy->levy_at);
                    $monthString = $formatter->format(strtotime($levy->levy_at));

                    $levySum = $levy->levy_sum;

                    $surplus = $levy->levy_sum + $startBalance;
                    if ($surplus >= 0 && $startBalance < 0) {
                        $levySum = $startBalance * -1;
                    }

                    if ((float) $levySum > 0) {
                        $products[] = [
                            'name'     => 'Членский взнос(' . $levyTypeTitleCode . ') за ' . $monthString . ' '
                                . date('Y', strtotime($levy->levy_at)) . ' г',
                            'quantity' => 1,
                            'unit'     => 'мес',
                            'price'    => $levySum,
                        ];

                        $sum += $levySum;

                        $startBalance += $levySum;
                    }
                }

                $olderSum = $startBalanceBase * -1 - $sum;
                if ($olderSum > 0) {
                    $products[] = [
                        'name'     => 'Членский взнос(' . $levyTypeTitleCode . ') за предыдущий период ',
                        'quantity' => 1,
                        'unit'     => '',
                        'price'    => $olderSum,
                    ];

                    $sum += $olderSum;
                }
            }

            $data['sum']      = $sum;
            $data['products'] = $products;

            /** Signatures */
            $data['signature'] = ['stamp' => null, 'director' => null, 'accountant' => null];
            if ($company->signatureStamp) {
                $company->signatureStamp->getUrl();
                $data['signature']['stamp'] = $company->signatureStamp->getPath('org');
                $data['signature']['stamp'] = file_get_contents($data['signature']['stamp']);
                $data['signature']['stamp'] = 'data:image/jpg;base64,' . base64_encode($data['signature']['stamp']);
            }
            if ($company->signatureAccountant) {
                $company->signatureAccountant->getUrl();
                $data['signature']['accountant'] = $company->signatureAccountant->getPath('org');
                $data['signature']['accountant'] = file_get_contents($data['signature']['accountant']);
                $data['signature']['accountant'] = 'data:image/jpg;base64,' . base64_encode($data['signature']['accountant']);
            }
            if ($company->signatureDirector) {
                $company->signatureDirector->getUrl();
                $data['signature']['director'] = $company->signatureDirector->getPath('org');
                $data['signature']['director'] = file_get_contents($data['signature']['director']);
                $data['signature']['director'] = 'data:image/jpg;base64,' . base64_encode($data['signature']['director']);
            }

            $document->sum        = $sum;
            $document->name       = $document->getFullNumber($levyType);
            $document->data       = json_encode($data);
            $document->created_at = date('Y-m-d');

            $document->save();

            $this->_object = $document;
        }

        $path = Yii::getAlias(self::TEMPLATES_PATH) . DIRECTORY_SEPARATOR . 'account.php';

        $content = (new View())->renderFile($path, $data);

        $fileName = Yii::getAlias('@backend/runtime/' . time() . rand(0, 999999) . '.pdf');

        ini_set("pcre.backtrack_limit", "100000000");

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode'        => Pdf::MODE_UTF8,
            'format'      => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_FILE,
            'filename'    => $fileName,
            'content'     => $content,
            'cssFile'     => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline'   => '
             html,
        body {
            height: 100%;
        }

        #container {
            min-height: 100%;
            position: relative;
        }

        /*Стили для блока с шапкой*/
        #header {
            background: #222;
            padding: 10px;
        }

        /*Стили для центральной части*/
        #body {
            padding-bottom: 70px; /* Высота блока "footer" */
        }

        /*Стили для нижней части*/
        #footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 70px; /* Высота блока "footer" */
            background: #66ccff;
        }
            
            
            
             body {
            width: 210mm;
            margin-left: auto;
            margin-right: auto;
            border: 1px #efefef solid;
            font-size: 11pt;
        }

        table.invoice_bank_rekv {
            border-collapse: collapse;
            border: 1px solid;
        }

        table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td {
            border: 1px solid;
        }

        table.invoice_items {
            border: 1px solid;
            border-collapse: collapse;
        }

        table.invoice_items td, table.invoice_items th {
            border: 1px solid;
        }
            ',
            'options'     => ['title' => 'Krajee Report Title'],
            'methods'     => [
                'SetHeader' => ['Счёт на оплату'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        // return the pdf output as per the destination setting
        $pdf->render();

        return $fileName;
    }

    /**
     * Формируем аттач аккаунта
     *
     * @param Counteragent $counteragent
     * @return array
     */
    public static function getGenerator(Counteragent $counteragent, array $dataItem = []): array
    {
        $data = ['generator' => null, 'item' => null];

        $customPeriod = [];

        if (!empty($dataItem['params']['type']) && $dataItem['params']['type'] == 2) {
            if (!empty($dataItem['params']['cost'])) {
                $dateData = [];
                if (!empty($dataItem['params']['range']) && strpos($dataItem['params']['range'], ' - ')) {
                    $dateData = explode(' - ', $dataItem['params']['range']);
                }

                $customPeriod = [
                    'start' => $dateData[0] ?? null,
                    'end'   => $dateData[1] ?? null,
                    'cost'  => $dataItem['params']['cost'],
                ];
            }
        }

        $generator = new self();

        $dataItem['isFile'] = true;
        $dataItem['isDrop'] = true;
        $fullAcountMonths   = (bool)($dataItem['params']['accountFull'] ?? false);

        $dataItem['src'] = $generator->generate(
            $counteragent->company,
            $counteragent,
            null,
            $fullAcountMonths,
            $customPeriod,
            ($dataItem['params']['levyType'] ?? CounteragentLevyPaid::TYPE_MEMBER)
        );

        $data['generator'] = $generator;
        $data['item']      = $dataItem;

        return $data;
    }
}
