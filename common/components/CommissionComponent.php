<?php

namespace emilasp\commission\common\components;

use stdClass;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class CommissionComponent
 * @package emilasp\commission\common\components
 */
class CommissionComponent extends Component
{
    public const DATA_FIELDS = [
        'type'            => null,
        'printing_method' => null,
        'printing_count'  => null,
        'size'            => null,
        'paper'           => null,
        'colors'          => null,
        'perforation'     => null,
        'format'          => ['Формат'],
        'count'           => ['Количество'],
        'format_block'    => null,
        'format_stamp'    => null,
        'nominal'         => null,
        'year'            => null,
        'date_at'         => null,
        'artist_designer' => null,
        'themes'          => null,
    ];

    public const CATEGORY_STAMP_ID = 786;
    public const CATEGORY_BONA_ID  = 786;

    private const CACHE_KEY_CSRF_TOKEN           = 'key:csrf';
    private const CACHE_KEY_CSRF_COOKIES         = 'key:csrfCookies';
    private const CACHE_KEY_LOGIN_COOKIES        = 'key:loginCookies';
    private const CACHE_KEY_MARKA_SEARCH_COOKIES = 'key:markaSearchCookies';
    private const CACHE_KEY_CREATE_COOKIES       = 'key:createCookies';

    private const ACTION_LOGIN_CSRF     = '/login';
    private const ACTION_LOGIN          = '/login_check';
    private const ACTION_CREATE_CSRF    = '/backoffice/lot/new/';
    private const ACTION_CREATE         = '/backoffice/lot/create/';
    private const ACTION_UPDATE         = '/goods/edit/';
    private const ACTION_UPDATE_SAVE    = '/goods/update/';
    private const ACTION_UPLOAD_FILE    = '/goods/photo/upload/';
    private const PATH_TO_DEFAULT_IMAGE = '@vendor/emilasp/yii2-commission/common/assets/default.jpg';

    public $baseUrl = 'https://www.monetnik.ru';
    public $login;
    public $password;


    /**
     * Create product
     *
     * @param array $data
     * @return int|null
     */
    public function createProduct(array $data): ?int
    {
        $this->login();
        //$this->createPageCookie();

        $dataDefault = [
            'coinId'                       => '',
            'groupActionType'              => 'select',
            'lotForm[amountChangeComment]' => '',
            'lotForm[amount]'              => '1',

            'lotForm[articleReserve]'             => '',
            'lotForm[authenticityCertificate]'    => '',
            'lotForm[bankPackage]'                => '',
            'lotForm[circulation]'                => '10000',// 10000
            'lotForm[coinCondition]'              => '7',
            'lotForm[coinType]'                   => '',
            'lotForm[coinWeight]'                 => '',
            'lotForm[coin]'                       => '',
            'lotForm[customThickness]'            => '',
            'lotForm[delPhotoIds]'                => '',
            'lotForm[description]'                => 'Test description', // test description
            'lotForm[diameter]'                   => '',
            'lotForm[dutchAuctionChangeInterval]' => '',
            'lotForm[dutchAuctionMinimalPrice]'   => '',
            'lotForm[dutchAuctionPriceStep]'      => '',
            'lotForm[englishAuctionFinishAt]'     => date('d.m.Y H:i', strtotime('+7 days')),
            'lotForm[englishAuctionStartPrice]'   => '1',
            'lotForm[fineness]'                   => '',
            'lotForm[group]'                      => '',
            'lotForm[hiddenAmount]'               => '1',
            'lotForm[isMonitorSupply]'            => '1',
            'lotForm[isQuantitative]'             => '1',
            'lotForm[keywords]'                   => '',
            'lotForm[mainCategory]'               => '786',//
            'lotForm[material]'                   => '13',//
            'lotForm[maxCondition]'               => '',
            'lotForm[megavisorLink]'              => '',
            'lotForm[name]'                       => 'Test Name',// Name
            'lotForm[needShowNewLabel]'           => '1',
            'lotForm[notes]'                      => '',
            'lotForm[oldPrice]'                   => '',
            'lotForm[par]'                        => '',
            'lotForm[price]'                      => '100', //100
            'lotForm[profit]'                     => '',
            'lotForm[purchasePrice]'              => '20', //20
            'lotForm[saveAndView]'                => '',
            'lotForm[scheduleActivationAt]'       => '',
            'lotForm[scheduleDeactivationAt]'     => '',
            'lotForm[sheldonCondition]'           => '',
            'lotForm[slabCompany]'                => '',
            'lotForm[surplusDiscountAmount]'      => '',
            'lotForm[weight]'                     => '1',// 1
            'lotForm[wrapperType]'                => '',
            'lotForm[years]'                      => '2018', //2018
            'newGroupName'                        => '',
            'photo[0]'                            => '',
            'photo[1]'                            => '', // @TODO УБрать массив
            'selectedCategs[0]'                   => '786',
            'useUploadedPhotoName[0]'             => '',
            'useUploadedPhotoName[1]'             => '',
            'wholesaleAmount[0]'                  => '',
            'wholesaleAmount[1]'                  => '',
            'wholesaleAmount[2]'                  => '',
            'wholesalePrice[0]'                   => '',
            'wholesalePrice[1]'                   => '',
            'wholesalePrice[2]'                   => '',
        ];

        $data = ArrayHelper::merge($dataDefault, $data);

        $response = $this->sendRequest(
            self::ACTION_CREATE,
            $data,
            'post',
            self::CACHE_KEY_LOGIN_COOKIES,
            [
                'Origin'                    => 'https://www.monetnik.ru',
                'Content-Type'              => 'application/x-www-form-urlencoded',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Upgrade-Insecure-Requests' => 1,
                'Referer'                   => 'https://www.monetnik.ru/backoffice/lot/new/',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
            ]
        );

        $id = null;
        if (strpos($response->content, '"refresh"') !== false) {
            $crawler = new Crawler($response->content);
            $link    = $crawler->filter('meta[http-equiv="refresh"]');

            $content     = $link->attr('content');
            $contentData = explode('-', $content);

            $id = trim($contentData[count($contentData) - 1], '/');
        }

        return $id;
    }

    /**
     * Update im item
     *
     * @param string $imId
     * @param array  $fields
     * @return bool
     */
    public function updateItem(string $imId, array $fields = []): bool
    {
        $this->login();

        $action = self::ACTION_UPDATE . $imId . '/';

        $response = $this->sendRequest(
            $action,
            [],
            'get',

            self::CACHE_KEY_LOGIN_COOKIES,
            [
                'Origin'                    => 'https://www.monetnik.ru',
                'Content-Type'              => 'application/x-www-form-urlencoded',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Upgrade-Insecure-Requests' => 1,
                'Referer'                   => 'https://www.monetnik.ru/my/commission/s/',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
            ]
        );

        if (strpos($response->content, 'Редактирование товара') !== false) {
            $crawler = new Crawler($response->content);
            $form    = $crawler->filter('.container form');

            $inputs = $form->filter('input, textarea, select');

            $values = $inputs->each(function ($input) {
                $name  = $input->attr('name');
                $value = $input->attr('value');

                return ['name' => $name, 'value' => $value];
            });

            $fields = ArrayHelper::map($values, 'name', 'value');

            $fields = array_map(function ($value) {
                $value = $value == null ? '' : $value;
                return $value;
            }, $fields);

            ksort($fields);

            //= lotForm[coinCondition] = 7
            //our lotForm[crossSellLots][] = ''
            //= lotForm[description]
            //our lotForm[highPurchasePrice]=1
            //our lotForm[isActive]=1
            //our lotForm[isBeforeChrist]=1
            //our lotForm[isDutchAuction]=1
            //our lotForm[isEnglishAuction]=1
            //our lotForm[linkedLot]=1

            $action   = self::ACTION_UPDATE_SAVE . $imId . '/';
            $response = $this->sendRequest(
                $action,
                $fields,
                'post',
                self::CACHE_KEY_LOGIN_COOKIES,
                [
                    'Origin'                    => 'https://www.monetnik.ru',
                    'Content-Type'              => 'application/x-www-form-urlencoded',
                    'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                    'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Upgrade-Insecure-Requests' => 1,
                    'Referer'                   => 'https://www.monetnik.ru' . $action,
                    'Accept-Encoding'           => 'gzip, deflate, br',
                    'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
                ]
            );

            $tt = 10;

        }


        return true;
    }


    /**
     * Получаем данные по загруженному изображению
     *
     * @return StdClass
     */
    public function getUploadedDefaultImage(): StdClass
    {
        $this->login();

        $response = $this->sendRequest(
            self::ACTION_UPLOAD_FILE,
            '{"file":"/home/emilasp/Изображения/' . time() . '_default.jpg"}',
            'post',
            self::CACHE_KEY_LOGIN_COOKIES,
            [
                'Origin'                    => 'https://www.monetnik.ru',
                'Content-Type'              => 'application/x-www-form-urlencoded',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Upgrade-Insecure-Requests' => 1,
                'Referer'                   => 'https://www.monetnik.ru/backoffice/lot/new/',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
            ],
            [self::PATH_TO_DEFAULT_IMAGE]
        );

        return new StdClass();
    }

    /**
     * Parse links
     *
     * @return array
     */
    public function parseProductLinks(): array
    {
        $actions = [
            '/backoffice/my/commission/s/inactive/' => ['filter' => '.goods-view', 'attribute' => 'href'],
            '/backoffice/my/commission/s/sold/'     => ['filter' => '.goods-view', 'attribute' => 'href'],
            '/backoffice/my/commission/s/not-sold/' => ['filter' => '.goods-view', 'attribute' => 'href'],
        ];

        $productUrls = [];
        foreach ($actions as $action => $settings) {
            $response = $this->sendRequest($action, [], 'get', self::CACHE_KEY_LOGIN_COOKIES);

            $crawler = new Crawler($response->content);
            $links   = $crawler->filter($settings['filter']);

            $urls = $links->each(function ($link) use ($settings, &$productUrls) {
                $url     = $link->attr($settings['attribute']);
                $dataUrl = explode('-', $url);
                $last    = $dataUrl[count($dataUrl) - 1];
                return 'https://www.monetnik.ru/goods/edit/' . $last;
            });

            $productUrls = ArrayHelper::merge($productUrls, $urls);
        }

        return array_unique($productUrls);
    }


    /**
     * Parse product
     *
     * @return array
     */
    public function parseProduct(string $url): array
    {
        $attributeFilters = [
            'guid'        => [
                'filter'    => 'form',
                'attribute' => 'action',
                'value'     => function ($value) {
                    $dataUrl = explode('/', trim($value, '/'));
                    $last    = $dataUrl[count($dataUrl) - 1];
                    return $last;
                }
            ],
            'category_id' => ['filter' => '#lotForm_mainCategory option:selected', 'attribute' => 'value'],
            'name'        => ['filter' => '#lotForm_name', 'attribute' => 'value'],

            'price'               => ['filter' => '#lotForm_oldPrice', 'attribute' => 'value'],
            'price_sale'          => ['filter' => '#lotForm_price', 'attribute' => 'value'],
            'description'         => ['filter' => '#lotForm_description', 'type' => 'text'],
            'comment'             => ['filter' => '#lotForm_notes', 'attribute' => 'value'],
            'keywords'            => ['filter' => '#lotForm_keywords', 'attribute' => 'value'],
            'status'              => [
                'filter'    => '#lotForm_isActive',
                'attribute' => 'checked',
                'value'     => function ($value) {
                    return $value === 'checked' ? 1 : 0;
                }
            ],
            'data_year'           => ['filter' => '#lotForm_years', 'attribute' => 'value'],
            'data_printing_count' => ['filter' => '#lotForm_circulation', 'attribute' => 'value'],
            'article'             => ['filter' => '#lotForm_notes', 'attribute' => 'value'],
            //'count'               => ['filter' => '', 'attribute' => ''],
            //'cost'                => ['filter' => '', 'attribute' => ''],
            //'cost_r'              => ['filter' => '', 'attribute' => ''],
        ];

        $attributes = [];
        $response   = $this->sendRequest($url, [], 'get', self::CACHE_KEY_LOGIN_COOKIES);

        $crawler = new Crawler($response->content);

        if (strpos($crawler->text(), 'сервис временно недоступен')) {
            return [];
        }

        foreach ($attributeFilters as $attribute => $filter) {
            $node = $crawler->filter($filter['filter']);

            if (!empty($filter['attribute'])) {
                $value = $node->attr($filter['attribute']);
            } elseif ($filter['type'] === 'text') {
                $value = $node->text();
            }

            if (!empty($filter['value']) && is_callable($filter['value'])) {
                $value = $filter['value']($value);
            }
            $attributes[$attribute] = $value;
        }


        return $attributes;
    }


    /**
     * Auth in IM
     *
     * @return bool
     */
    public function login(): bool
    {
        $pageCsrf = $this->getCsrfToken(self::ACTION_LOGIN_CSRF);

        $response = $this->sendRequest(
            self::ACTION_LOGIN,
            [
                '_username'    => $this->login,
                '_password'    => $this->password,
                '_csrf_token'  => $pageCsrf,
                '_remember_me' => 'on',
            ], 'post', self::CACHE_KEY_CSRF_COOKIES . self::ACTION_LOGIN_CSRF);


        //if ($response->isOk) {
        $cookies = ArrayHelper::toArray($response->getCookies());

        //$cookies = $response->headers->get('set-cookie');


        $this->setInCache(self::CACHE_KEY_LOGIN_COOKIES, $cookies);
        //}

        return true;
    }

    /**
     * Получаем данные по тприобретенным товарам
     *
     * @param string $url
     * @return array
     */
    public function getOrderItems(string $url): array
    {
        $this->login();

        $response = $this->sendRequest(
            $url,
            [],
            'get',
            self::CACHE_KEY_LOGIN_COOKIES,
            [
                ':authority'                => 'www.monetnik.ru',
                ':method'                   => 'GET',
                ':path'                     => '/backoffice/my/commission/s/orders/other/',
                ':scheme'                   => 'https',
                'Origin'                    => 'https://www.monetnik.ru',
                'Content-Type'              => 'application/x-www-form-urlencoded',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Upgrade-Insecure-Requests' => 1,
                'Referer'                   => 'https://www.monetnik.ru/backoffice/my/commission/s/orders/ready/',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
                'x-compress'                => 'null',
            ]
        );

        $crawler = new Crawler($response->content);

        if (strpos($crawler->text(), 'сервис временно недоступен')) {
            return [];
        }


        $rows = $crawler->filter('.tab-content table tr');

        $items = $rows->each(function ($link, $index) {
            if (!$index) {
                return [];
            }
            $tdData = explode("\n", trim($link->filterXPath('//td[2]')->text()));

            //$orderData3  = $link->filterXPath('//td[3]')->html();
            $orderNumber = str_replace('№', '', $tdData[0]);

            $orderDateAt        = date('Y-m-d H:i:s', strtotime(str_replace('от ', '', $tdData[1])));
            $orderStatus        = $tdData[4];
            $orderIsPayed       = trim($tdData[3]) === 'не оплачен' ? 0 : 1;
            $orderIsMonetizated = 'не выплачено' === trim($link->filterXPath('//td[8]')->text()) ? 0 : 1;

            $url      = $link->filterXPath('//td[3]/a')->attr('href');
            $dataUrl  = explode('-', $url);
            $itemImId = trim($dataUrl[count($dataUrl) - 1], '/');

            $itemName       = $link->filterXPath('//td[3]/a')->text();
            $itemArticle    = $link->filterXPath('//td[3]/span/b[2]')->text();
            $itemCost       = (float)trim(str_replace(' ', '', $link->filterXPath('//td[4]')->text()));
            $itemCount      = (int)trim(str_replace(' ', '', $link->filterXPath('//td[5]')->text()));
            $itemCommission = (float)str_replace(' ', '', $link->filterXPath('//td[7]')->text());

            return [
                'number'       => $orderNumber,
                'date_at'      => $orderDateAt,
                'is_payed'     => $orderIsPayed,
                'is_monetized' => $orderIsMonetizated,
                'status'       => $orderStatus,


                'im_id'      => $itemImId,
                'article'    => $itemArticle,
                'name'       => $itemName,
                'count'      => $itemCount,
                'cost'       => $itemCost,
                'sum'        => $itemCount * $itemCost,
                'commission' => $itemCommission,
            ];
        });
        $items = array_filter($items, function ($item) {
            return $item;
        });
        return ArrayHelper::index($items, null, 'number');
    }

    /**
     * Получаем данные по тприобретенным товарам
     *
     * @param string $url
     * @return array
     */
    public function getReserveItems(int $reserveDays = 30, int $lessdays = 14): array
    {
        $this->login();

        $path     = "/backoffice/my/commission/s/plan_supplies/{$reserveDays}/{$lessdays}/";
        $response = $this->sendRequest(
            $path,
            [],
            'get',
            self::CACHE_KEY_LOGIN_COOKIES,
            [
                ':authority'                => 'www.monetnik.ru',
                ':method'                   => 'GET',
                ':path'                     => $path,
                ':scheme'                   => 'https',
                'Origin'                    => 'https://www.monetnik.ru',
                'Content-Type'              => 'application/x-www-form-urlencoded',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Upgrade-Insecure-Requests' => 1,
                'Referer'                   => 'https://www.monetnik.ru/backoffice/my/commission/s/orders/ready/',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
                'x-compress'                => 'null',
            ]
        );

        $crawler = new Crawler($response->content);

        if (strpos($crawler->text(), 'сервис временно недоступен')) {
            return [];
        }

        $rows = $crawler->filter('.tab-content table tr');

        $items = $rows->each(function ($link, $index) {
            if (!$index) {
                return [];
            }
            $html = $link->html();

            if (!$link->filterXPath('//td[3]/a')->count()) {
                return [];
            }
            $url       = $link->filterXPath('//td[3]/a')->attr('href');
            $dataUrl   = explode('-', $url);
            $itemImId  = trim($dataUrl[count($dataUrl) - 1], '/');
            $article   = trim($link->filterXPath('//td[1]')->text());
            $name      = $link->filterXPath('//td[3]/a')->text();
            $count     = (int)trim($link->filterXPath('//td[5]')->text());
            $sellDay   = (float)trim($link->filterXPath('//td[6]')->text());
            $toBay     = (int)trim($link->filterXPath('//td[7]')->text());
            $daysIsset = (int)trim($link->filterXPath('//td[8]')->text());

            return [
                'im_id'      => $itemImId,
                'article'    => $article,
                'name'       => $name,
                'count'      => $count,
                'sell_day'   => $sellDay,
                'to_bay'     => $toBay,
                'days_isset' => $daysIsset,
            ];
        });
        $items = array_filter($items, function ($item) {
            return $item;
        });
        return ArrayHelper::index($items, null, 'im_id');
    }

    /**
     * Получаем данные по тприобретенным товарам
     *
     * @return array
     */
    public function getNoActiveItems(): array
    {
        $this->login();

        $path     = "/backoffice/my/commission/s/inactive/";
        $response = $this->sendRequest(
            $path,
            [],
            'get',
            self::CACHE_KEY_LOGIN_COOKIES,
            [
                ':authority'                => 'www.monetnik.ru',
                ':method'                   => 'GET',
                ':path'                     => $path,
                ':scheme'                   => 'https',
                'Origin'                    => 'https://www.monetnik.ru',
                'Content-Type'              => 'application/x-www-form-urlencoded',
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Upgrade-Insecure-Requests' => 1,
                'Referer'                   => 'https://www.monetnik.ru/backoffice/my/commission/s/orders/ready/',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
                'x-compress'                => 'null',
            ]
        );

        $crawler = new Crawler($response->content);

        if (strpos($crawler->text(), 'сервис временно недоступен')) {
            return [];
        }

        $rows = $crawler->filter('.tab-content table tr');

        $items = $rows->each(function ($link, $index) {
            if (!$index) {
                return [];
            }

            if (!$link->filterXPath('//td[2]/a')->count()) {
                return [];
            }
            $url      = $link->filterXPath('//td[2]/a')->attr('href');
            $dataUrl  = explode('-', $url);
            $itemImId = trim($dataUrl[count($dataUrl) - 1], '/');
            return [$itemImId];
        });
        $items = array_filter($items, function ($item) {
            return $item;
        });
        return ArrayHelper::getColumn($items, 0);
    }


    /**
     * Получаем товары из поиска
     *
     * @return array
     */
    public function parseProductCategory(string $category): array
    {
        $this->login();

        $path     = "https://www.monetnik.ru/{$category}/";
        $response = $this->sendRequest(
            $path,
            [],
            'get',
            self::CACHE_KEY_LOGIN_COOKIES,
            [
                //'Origin'                    => 'https://www.monetnik.ru',
                //'Content-Type'              => 'application/x-www-form-urlencoded',
                'Upgrade-Insecure-Requests' => 1,
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
            ]
        );

        $crawler = new Crawler($response->content);

        if (strpos($crawler->text(), 'сервис временно недоступен')) {
            return [];
        }

        $itemsDiv = $crawler->filter('#searchResult > div');

        $isAlertNo = false;
        $items     = $itemsDiv->each(function ($link, $index) use (&$isAlertNo) {
            $attributes = $link->extract(['_text', 'class']);

            if (strpos($attributes[0][0], 'временно отсутствуют') !== false) {
                $isAlertNo = true;
            }

            if (!$link->filter('.namecont a')->count()) {
                return [];
            }

            $isOur    = $link->filter('.admtag')->count();
            $url      = $link->filter('.namecont a')->attr('href');
            $dataUrl  = explode('-', $url);
            $itemImId = trim($dataUrl[count($dataUrl) - 1], '/');

            $price = 0;
            if ($link->filter('.itshopprice .nowrap')->count()) {
                $price = $link->filter('.itshopprice .nowrap')->text();
            }
            $price = (float)str_replace(' ', '', $price);

            return [
                'im_id'       => $itemImId,
                'price'       => (float)$price,
                'is_no_isset' => $isAlertNo,
                'is_our'      => $isOur,
            ];
        });
        $items     = array_filter($items, function ($item) {
            return $item;
        });

        return $items;
    }


    /**
     * Получаем товары из поиска
     *
     * @return array
     */
    public function parseProductSearch(string $category, int $year, string $nominal): array
    {
        //$this->login();

        $path     = "https://www.monetnik.ru/{$category}/?name={$nominal}&yearFrom={$year}&yearTo={$year}";
        $response = $this->sendRequest(
            $path,
            [],
            'get',
            null,
            [
                //'Origin'                    => 'https://www.monetnik.ru',
                //'Content-Type'              => 'application/x-www-form-urlencoded',
                'Upgrade-Insecure-Requests' => 1,
                'User-Agent'                => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36',
                'Accept'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding'           => 'gzip, deflate, br',
                'Accept-Language'           => 'ru,en;q=0.9,en-US;q=0.8',
            ]
        );

        $crawler = new Crawler($response->content);

        if (strpos($crawler->text(), 'сервис временно недоступен')) {
            return [];
        }

        $itemsDiv = $crawler->filter('#searchResult > div');

        $isAlertNo = false;
        $items     = $itemsDiv->each(function ($link, $index) use (&$isAlertNo) {
            $attributes = $link->extract(['_text', 'class']);

            if (strpos($attributes[0][0], 'временно отсутствуют') !== false) {
                $isAlertNo = true;
            }

            if (!$link->filter('.namecont a')->count()) {
                return [];
            }

            $url      = $link->filter('.namecont a')->attr('href');
            $dataUrl  = explode('-', $url);
            $itemImId = trim($dataUrl[count($dataUrl) - 1], '/');
            $isOur    = $this->isOurProduct($itemImId);

            $price = 0;
            if ($link->filter('.itshopprice .nowrap')->count()) {
                $price = $link->filter('.itshopprice .nowrap')->text();
            }
            $price = (float)str_replace(' ', '', $price);

            return [
                'im_id'       => $itemImId,
                'price'       => (float)$price,
                'is_no_isset' => $isAlertNo,
                'is_our'      => $isOur,
            ];
        });
        $items     = array_filter($items, function ($item) {
            return $item;
        });

        return $items;
    }

    /**
     * @return string
     */
    private function getCsrfToken(string $action): string
    {
        $csrfToken = $this->getFromCache(self::CACHE_KEY_CSRF_TOKEN . $action);

        if (!$csrfToken) {
            $response = $this->sendRequest($action);

            if ($response->isOk) {
                $cookies = ArrayHelper::toArray($response->getCookies());

                $crawler   = new Crawler($response->content);
                $inputCsrf = $crawler->filter('form [name="_csrf_token"]');

                $csrfToken = $inputCsrf->attr('value');

                $this->setInCache(self::CACHE_KEY_CSRF_TOKEN . $action, $csrfToken);
                $this->setInCache(self::CACHE_KEY_CSRF_COOKIES . $action, $cookies);
            }
        }
        return $csrfToken;
    }


    /**
     * @param string $action
     * @param array  $data
     * @param string $method
     * @return array
     */
    private function sendRequest(
        string $action,
        $data = [],
        string $method = 'get',
        string $cookieName = null,
        array $headers = [
            'Origin'                                          => 'https://www.monetnik.ru',
            'Upgrade-Insecure-Requests'                       => 1,
            //'Content-Type'                                    => 'application/x-www-form-urlencoded',
            'User-Agent'                                      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
            'X-DevTools-Emulate-Network-Conditions-Client-Id' => 'B72A04C0215FD004F5A13DE1FE7A5979',
            'Accept'                                          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            //'Referer'                                         => 'https://www.monetnik.ru/login',
            //'Accept-Encoding'                                 => 'gzip, deflate, br',
            'Accept-Language'                                 => 'ru,en;q=0.9,en-US;q=0.8',
        ],
        array $files = []
    ): Response {
        sleep(0.1);

        $client  = new Client([
            'baseUrl'   => $this->baseUrl,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($action)
            //->setData($data)
            ->setHeaders($headers);

        if (is_array($data)) {
            $request->setData($data);
        } else {
            $request->setContent($data);
        }

        $request->setOptions([
            'encoding' => ''
        ]);

        if ($files) {
            $request->setFormat(Client::FORMAT_JSON);
            foreach ($files as $index => $file) {
                $request->addFile('file[' . ($index + 1) . ']', \Yii::getAlias($file));
            }
        }

        if ($cookieName) {
            $cookies = $this->getFromCache($cookieName);
            $request->setCookies($cookies);
        }

        $response = $request->send();
        return $response;
    }


    /**
     * Save cookies
     *
     * @param string       $key
     * @param array|string $cookies
     */
    private function setInCache(string $key, $value): void
    {
        Yii::$app->cache->set($key, $value, 3600);
    }

    /**
     * @param string $key
     * @return mixed
     */
    private function getFromCache(string $key)
    {
        return Yii::$app->cache->get($key);
    }

    /**
     * Проверяем наш ли товар
     *
     * @param $itemImId
     * @return bool
     */
    private function isOurProduct($itemImId): bool
    {
        $keyCache = 'TM:our im ids';
        if (!$ourIds = Yii::$app->cache->get($keyCache)) {
            $ourIds = Yii::$app->db->createCommand("SELECT im_id FROM commission_catalog")->queryColumn();
            Yii::$app->cache->set($keyCache, $ourIds, 100);
        }

        return array_search($itemImId, (array)$ourIds);
    }
}
