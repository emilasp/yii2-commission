<?php
namespace emilasp\commission\common;

use emilasp\core\CoreModule;
use yii\helpers\ArrayHelper;

/**
 * Class Commission
 * @package emilasp\commission\common
 */
class CommissionBaseModule extends CoreModule
{
    /**
     * @return array
     */
    function behaviors()
    {
        return ArrayHelper::merge([

        ], parent::behaviors());
    }
}
