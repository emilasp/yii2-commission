<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\common\models\MarketCateg;
use emilasp\commission\console\commands\ParseMarkaController;
use emilasp\commission\console\commands\ParseMarkiMiraController;
use Yii;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\commission\common\models\search\CommissionProductSearch;
use emilasp\core\components\base\Controller;
use yii\db\Connection;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * NestedController implements the CRUD actions for CommissionProduct model.
 */
class NestedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var Connection $db */
        $db = Yii::$app->mysql;

        $root = $db->createCommand('SELECT * FROM market_categ WHERE id=17')->queryOne();

        $rootNew = new MarketCateg($root);
        $rootNew->makeRoot();

        $this->createChilds($rootNew);



        $sql = <<<SQL
        UPDATE market_categ SET 
SQL;

        $root = $db->createCommand('SELECT * FROM market_categ WHERE id=17')->queryOne();


        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function createChilds(MarketCateg $parent)
    {

        $childs = Yii::$app->mysql->createCommand(
            "SELECT * FROM market_categ WHERE parentId={$parent->id} ORDER BY leftId ASC"
        )->queryAll();

        foreach ($childs as $child) {
            $childNew = new MarketCateg($child);
            $childNew->appendTo($parent);

            $this->createChilds($childNew);
        }
    }
}
