<?php

namespace emilasp\commission\backend\controllers;

use Yii;
use emilasp\commission\common\models\CommissionOrderAnalize;
use emilasp\commission\common\models\search\CommissionOrderAnalizeSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * OrderAnalizeController implements the CRUD actions for CommissionOrderAnalize model.
 */
class OrderAnalizeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionOrderAnalize models.
     * @return mixed
     */
    public function actionIndex()
    {
        $minDate = date('Y-m-d', strtotime('-30 days'));

        $sql = <<<SQL
CREATE OR REPLACE VIEW commission_order_analize_view (im_id, type, count, cost, sum) AS
SELECT item.im_id, MAX(catalog.type) as type, SUM(item.count) as count, MAX(item.cost) as cost, SUM(item.count) * MAX(item.cost) as sum
 FROM commission_order_item item
  LEFT JOIN commission_order im_order ON im_order.id=item.order_id
  LEFT JOIN commission_catalog catalog ON catalog.im_id=item.im_id
WHERE im_order.date_at::DATE >= '{$minDate}'::DATE
GROUP BY item.im_id;
SQL;

        Yii::$app->db->createCommand($sql)->execute();

        $searchModel = new CommissionOrderAnalizeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommissionOrderAnalize model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, CommissionOrderAnalize::className()),
        ]);
    }

    /**
     * Creates a new CommissionOrderAnalize model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionOrderAnalize();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->im_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommissionOrderAnalize model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CommissionOrderAnalize::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->im_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CommissionOrderAnalize model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, CommissionOrderAnalize::className())->delete();

        return $this->redirect(['index']);
    }
}
