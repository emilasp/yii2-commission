<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\common\models\CommissionCatalog;
use Yii;
use emilasp\commission\common\models\search\CommissionReserveSearch;
use emilasp\core\components\base\Controller;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use emilasp\rights\filters\AccessControl;

/**
 * InActiveController implements the CRUD actions for CommissionReserve model.
 */
class InActiveController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionReserve models.
     * @return mixed
     */
    public function actionIndex()
    {
       /* $imIds = Yii::$app->monentik->getNoActiveItems();

        CommissionCatalog::updateAll(['active' => 0]);
        CommissionCatalog::updateAll(['active' => 2], ['im_id' => $imIds]);*/

        $dataProvider = new ActiveDataProvider([
            'query' => CommissionCatalog::find()->where(['active' => 2]),
            'pagination' => ['pageSize' => 1000],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Remove user tempalte
     *
     * @return array
     */
    public function actionActivated(): array
    {
        if ($model = CommissionCatalog::findOne(Yii::$app->request->post('id'))) {
            if (Yii::$app->monentik->updateItem($model->im_id, ['lotForm[[isActive]' => 1])) {
                $model->active = 1;
                $model->save(false);

                return $this->setAjaxResponse(1, 'Позиция активирована');
            }
        }
        return $this->setAjaxResponse(0, 'Не удалось активировать позицию');
    }
}
