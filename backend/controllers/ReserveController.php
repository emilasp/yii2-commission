<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\common\components\export\ExportDataComponent;
use emilasp\commission\common\models\CommissionCatalog;
use Yii;
use emilasp\commission\common\models\CommissionReserve;
use emilasp\commission\common\models\search\CommissionReserveSearch;
use emilasp\core\components\base\Controller;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * ReserveController implements the CRUD actions for CommissionReserve model.
 */
class ReserveController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete', 'catalog'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'catalog'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionReserve models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel                        = new CommissionReserveSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Catalog redirect
     * @return mixed
     */
    public function actionCatalog()
    {
        $searchModel                        = new CommissionReserveSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        $models = ArrayHelper::getColumn($dataProvider->getModels(), 'catalogItem');

        $ids = implode(',', ArrayHelper::getColumn($models, 'id'));

        return $this->redirect(['/commission/catalog/index', 'ids' => $ids]);
    }


    /**
     * Displays a single CommissionReserve model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, CommissionReserve::className()),
        ]);
    }

    /**
     * Displays a single CommissionReserve model.
     * @return mixed
     */
    public function actionYears(int $year = null, int $status = null)
    {

        if ($year) {
            $query     = CommissionCatalog::find()
                ->where(['year' => $year])->andFilterWhere(['status' => $status])
                ->orderBy('year DESC, im_id DESC');
            $component = new ExportDataComponent();
            $component->sendAsCsv(
                $query,
                ['Артикул' => 'article', 'ИМ ID' => 'im_id', 'Название' => 'title', 'Статус' => 'status'],
                'year_'
            );
        } else {
            $sql = <<<SQL
          SELECT year, COUNT(*) as all, SUM(
          CASE 
              WHEN im_id IS NULL THEN 0
              WHEN im_id = 0 THEN 0
              WHEN im_id > 0 THEN 1
        END
          ) as isset FROM  commission_catalog GROUP BY year ORDER BY year DESC;
SQL;

            $data = Yii::$app->db->createCommand($sql)->queryAll();

            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'sort'      => [
                    'attributes' => ['year', 'all', 'isset'],
                ],
            ]);

            $dataProvider->pagination->pageSize = 5000;

            return $this->render('years', ['dataProvider' => $dataProvider]);
        }
    }

    /**
     * Creates a new CommissionReserve model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionReserve();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommissionReserve model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CommissionReserve::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CommissionReserve model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, CommissionReserve::className())->delete();

        return $this->redirect(['index']);
    }


    /**
     * Remove user tempalte
     *
     * @return array
     */
    public function actionUpdateAjax(): array
    {
        $params = Yii::$app->request->post();

        if ($model = CommissionReserve::findOne($params['id'])) {
            $model->catalogItem->per_list = $params['per_list'];
            if ($model->catalogItem->save()) {
                return $this->setAjaxResponse(1, 'Позиция изменена');
            }
        }
        return $this->setAjaxResponse(0, 'Не удалось изменить позицию');
    }


    /**
     * Отправляем сформированный счёт
     */
    public function actionDownloadReserveExport(string $site)
    {
        $searchModel  = new CommissionReserveSearch();
        $dataProvider = $searchModel->search(['site' => $site]);

        $component = new ExportDataComponent();
        $component->sendAsCsv($dataProvider->query, [
            'Артикул'      => 'catalogItem.article',
            'Год'          => 'catalogItem.year',
            'Стомость'     => 'catalogItem.cost',
            'В листе'      => 'catalogItem.per_list',
            'В ИМ'         => 'count',
            'За день'      => 'sell_day',
            'Закупить'     => 'to_bay',
            'В листах'     => function ($model) {
                return ceil($model->to_bay / $model->catalogItem->per_list);
            },
            'Сумма'        => function ($model) {
                return $model->catalogItem->cost * $model->to_bay;
            },
            'Наименование' => 'name',
        ], 'reserve_', 8);
    }
}
