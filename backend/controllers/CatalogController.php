<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\common\components\CommissionComponent;
use emilasp\commission\common\components\export\DeliveryExportPdf;
use emilasp\commission\common\components\export\ExportDataComponent;
use emilasp\commission\common\models\CommissionCatalogMode;
use emilasp\commission\console\commands\ImCheckOurIndexController;
use emilasp\commission\console\commands\ImCheckPricesController;
use Yii;
use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\search\CommissionCatalogSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * CatalogController implements the CRUD actions for CommissionCatalog model.
 */
class CatalogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionCatalog models.
     * @return mixed
     */
    public function actionIndex($modeId = null, int $modeReset = null, string $ids = null)
    {
        /** @var CommissionComponent $component */
        /*$component = Yii::$app->monentik;
        $items = $component->parseProductCategory('pochtovye-marki');*/

       /*$controller = new ImCheckOurIndexController('TEST', Yii::$app);
       $controller->actionIndex();*/

       /** @var CommissionComponent $component */
        $component = Yii::$app->monentik;
        $component->updateItem(121121);

        if ($modeId) {
            CommissionCatalogMode::setMode($modeId, $modeReset);

            $this->redirect(['index']);
        }

        $searchModel  = new CommissionCatalogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['id' => ($ids ? explode(',', $ids) : null)]);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('index', [
            'modes'        => CommissionCatalogMode::find()->orderBy('id')->all(),
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommissionCatalog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, CommissionCatalog::className()),
        ]);
    }

    /**
     * Creates a new CommissionCatalog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionCatalog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommissionCatalog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CommissionCatalog::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Copy model catalog
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $model = $this->findModel($id, CommissionCatalog::className());

        $newModel = new CommissionCatalog();
        $newModel->setAttributes($model->getAttributes());

        $newModel->article .= '_copy';
        $newModel->im_id   = null;


        if ($newModel->load(Yii::$app->request->post()) && $newModel->save()) {
            return $this->redirect(['index', 'id' => $newModel->id]);
        } else {
            return $this->render('update', [
                'model' => $newModel,
            ]);
        }
    }

    /**
     * Remove user tempalte
     *
     * @return array
     */
    public function actionUpdateAjax(): array
    {
        $params = Yii::$app->request->post();

        if ($model = CommissionCatalog::findOne($params['id'])) {
            $model->setAttributes($params);
            if ($model->save()) {
                return $this->setAjaxResponse(1, 'Позиция изменена');
            }
        }
        return $this->setAjaxResponse(0, 'Не удалось изменить позицию');
    }

    /**
     * Remove user tempalte
     *
     * @return array
     */
    public function actionMonetnikCreateAjax(): array
    {
        if ($model = CommissionCatalog::findOne(Yii::$app->request->post('id'))) {
            $name          = $model->title;
            $description   = $model->getTableParams();
            $year          = $model->year;
            $pricePurchase = $model->cost;
            $price         = $model->getCalcultateSum(2.8);
            $circulation   = $model->circulation;
            $nominal       = $model->nominal ?: '';
            $bankPackage   = $model->type === CommissionCatalog::TYPE_MONEY_BON ? 3 : '';

            $id = Yii::$app->monentik->createProduct([
                'lotForm[name]'          => $name,
                'lotForm[description]'   => $description,
                'lotForm[purchasePrice]' => $pricePurchase,
                'lotForm[price]'         => $price,
                'lotForm[years]'         => $year,
                //'lotForm[par]'           => $nominal,
                //'lotForm[bankPackage]'   => $bankPackage,
                'lotForm[circulation]'   => ($circulation ?: ''),
            ]);

            if ($id) {
                $model->im_id = $id;

                if ($model->save()) {
                    return $this->setAjaxResponse(1, 'Товар успешно создан', $id);
                }
            }
        }
        return $this->setAjaxResponse(0, 'Не удалось создать позицию', $id);
    }

    /**
     * Deletes an existing CommissionCatalog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, CommissionCatalog::className())->delete();

        return $this->redirect(['index']);
    }


    /**
     * Формируем закупочный лист
     *
     * @param int $status
     */
    public function actionDownloadDeliveryExport(int $status = CommissionCatalog::STATUS_NEW)
    {
        $component = new DeliveryExportPdf();
        $file      = $component->generate(CommissionCatalogMode::getCurrentMode(), $status);
        Yii::$app->response->sendFile($file);
    }

    /**
     * Отправляем сформированный счёт
     */
    public function actionDownloadIssetProducts(int $isset = 1)
    {
        if ($isset) {
            $query = CommissionCatalog::find()->where(['>', 'im_id', 0])->orderBy('year DESC, updated_at DESC');
        } else {
            $query = CommissionCatalog::find()->orderBy('year DESC, updated_at DESC');
        }
        $component = new ExportDataComponent();
        $component->sendAsCsv(
            $query,
            ['Артикул' => 'article', 'ИМ ID' => 'im_id', 'Год' => 'year', 'Название' => 'title', 'Статус' => 'status'],
            'isset_'
        );
    }
}
