<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\common\components\CommissionComponent;
use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionOrderItem;
use Yii;
use emilasp\commission\common\models\CommissionOrder;
use emilasp\commission\common\models\search\CommissionOrderSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * OrderController implements the CRUD actions for CommissionOrder model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel                        = new CommissionOrderSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        $chartStart = Yii::$app->request->post('start', date('Y-m-d', strtotime('-100 days')));
        $chartEnd   = Yii::$app->request->post('end', date('Y-m-d'));

        $sumsByDay     = CommissionOrder::getSumByPeriod($chartStart, $chartEnd, 'd.m');
        $avgByDay      = CommissionOrder::getAvgSum($chartStart, $chartEnd, 'd.m');
        $sumsByDayBons = CommissionOrder::getSumByPeriod(
            $chartStart,
            $chartEnd, 'd.m',
            CommissionCatalog::$typeGroups[CommissionCatalog::TYPE_MONEYS]
        );

        return $this->render('index', [
            'sumsByDay'      => array_reverse($sumsByDay),
            'sumsByDayBons'  => array_reverse($sumsByDayBons),
            'avgByDay'       => array_reverse($avgByDay),
            'chartStart'     => $chartStart,
            'chartEnd'       => $chartEnd,
            'avgSumsByMonth' => CommissionOrder::getAvgSumWithGroup($chartStart, $chartEnd, 'MM'),
            'sumsByMonth'    => CommissionOrder::getSumByPeriodWithGroup($chartStart, $chartEnd, 'MM'),
            'avgSumsByWeek'  => CommissionOrder::getAvgSumWithGroup($chartStart, $chartEnd),
            'sumsByWeek'     => CommissionOrder::getSumByPeriodWithGroup($chartStart, $chartEnd, 'WW'),
            'searchModel'    => $searchModel,
            'dataProvider'   => $dataProvider,
            'statuses'       => CommissionOrder::find()->select('status')->distinct()->map('status', 'status')->all(),
        ]);
    }

    /**
     * Displays a single CommissionOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView(string $date, string $year = 'Y')
    {
        $date = date('Y-m-d', strtotime($date . '.' . date($year)));

        $orders   = CommissionOrder::find()->where("date_at::DATE='{$date}'::DATE")->orderBy('sum DESC')->all();
        $products = CommissionOrderItem::find()->where([
            'order_id' => ArrayHelper::getColumn($orders, 'id')
        ])->orderBy('sum DESC')->all();

        $items = [];
        foreach ($products as $item) {
            if (isset($items[$item->catalogItem->id])) {
                $items[$item->catalogItem->id]->count += $item->count;
                $items[$item->catalogItem->id]->sum   += $item->sum;
            } else {
                $items[$item->catalogItem->id] = $item;
            }
        }

        return $this->render('view', [
            'date'   => $date,
            'orders' => $orders,
            'items'  => $items,
        ]);
    }

    /**
     * Creates a new CommissionOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommissionOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CommissionOrder::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CommissionOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, CommissionOrder::className())->delete();

        return $this->redirect(['index']);
    }
}
