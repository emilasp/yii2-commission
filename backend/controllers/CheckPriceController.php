<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\console\commands\ImCheckPricesController;
use Yii;
use emilasp\commission\common\models\ImCheckPrice;
use emilasp\commission\common\models\search\ImCheckPriceSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * CheckPriceController implements the CRUD actions for ImCheckPrice model.
 */
class CheckPriceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete', 'clone', 'check'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'clone', 'check'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ImCheckPrice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel                        = new ImCheckPriceSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 500;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Lists all ImCheckPrice models.
     * @return mixed
     */
    public function actionCheck()
    {
        ob_start();
        $controller = new ImCheckPricesController('front', Yii::$app);
        $controller->actionIndex();
        ob_end_clean();

        return $this->redirect(['index']);
    }

    /**
     * Displays a single ImCheckPrice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, ImCheckPrice::className()),
        ]);
    }

    /**
     * Creates a new ImCheckPrice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImCheckPrice(Yii::$app->request->get());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ImCheckPrice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, ImCheckPrice::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Clone
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionClone($id)
    {
        $model = $this->findModel($id, ImCheckPrice::className());

        $newModel          = new ImCheckPrice($model->getAttributes([
            'catalog_id',
            'im_id',
            'year',
            'category',
            'status'
        ]));
        $newModel->nominal = '-';
        $newModel->save();

        return $this->redirect(['update', 'id' => $newModel->id]);
    }

    /**
     * Deletes an existing ImCheckPrice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, ImCheckPrice::className())->delete();

        return $this->redirect(['index']);
    }
}
