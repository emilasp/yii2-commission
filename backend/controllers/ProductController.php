<?php

namespace emilasp\commission\backend\controllers;

use emilasp\commission\common\components\MeshokComponent;
use emilasp\commission\console\commands\MeshokBidderController;
use emilasp\commission\console\commands\ParseCollmartController;
use emilasp\commission\console\commands\ParseMarkaController;
use emilasp\commission\console\commands\ParseMarkiMiraController;
use emilasp\commission\console\commands\SyncImController;
use Yii;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\commission\common\models\search\CommissionProductSearch;
use emilasp\core\components\base\Controller;
use yii\filters\VerbFilter;
use emilasp\rights\filters\AccessControl;

/**
 * ProductController implements the CRUD actions for CommissionProduct model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CommissionProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $controller = new ParseCollmartController('ID', Yii::$app);
        $controller->actionIndex(0);

        $searchModel  = new CommissionProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CommissionProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, CommissionProduct::className()),
        ]);
    }

    /**
     * Creates a new CommissionProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CommissionProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CommissionProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CommissionProduct::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CommissionProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, CommissionProduct::className())->delete();

        return $this->redirect(['index']);
    }
}
