<?php

use backend\modules\counteragents\models\Counteragent;
use emilasp\media\models\File;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="panel panel-success bg-white">
    <div class="panel-body" data-id="<?= $model->id ?>">

        <div class="row">
            <div class="col-md-8">

                <?php $data = json_decode($model->data, true); ?>


                <?php if ($data) : ?>
                    <ul class="nav nav-tabs">
                        <?php foreach ($data as $index => $dataItem) : ?>
                            <li>
                                <a data-toggle="tab" href="#tab<?= $index . $model->id ?>"
                                   class="nav-link <?= !$index ? 'active' : '' ?>">
                                    <?= $dataItem['cost'] ?? '' ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?php foreach ($data as $index => $dataItem) : ?>
                            <?php $url = 'http://rusmarka.ru' . ($dataItem['image'] ?? '') ?>

                            <div id="tab<?= $index . $model->id ?>" class="tab-pane <?= !$index ? 'active' : '' ?>">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="text-center"><img src="<?= $url ?>" height="200px" /></div>
                                    </div>
                                    <div class="col-md-6">
                                        <?= Html::textarea('nameName', $model->getTitle($index),
                                            ['class' => 'form-control', 'rows' => 2]) ?>

                                        <?= Html::textarea('tableParams', $model->getTableParams($index),
                                            ['class' => 'form-control', 'rows' => 6]) ?>
                                    </div>
                                </div>


                                <?php $params = \yii\helpers\ArrayHelper::remove($dataItem, 'params') ?>

                                <?= \yii\widgets\DetailView::widget([
                                    'model'      => (object)$dataItem,
                                    'attributes' => array_keys($dataItem),
                                ]) ?>

                                <hr/>

                                <?= \yii\widgets\DetailView::widget([
                                    'model'      => $params,
                                    'attributes' => array_keys($params),
                                ]) ?>

                            </div>

                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>

            </div>
            <div class="col-md-2">
                <b>ИМ:</b>

                <a href="<?= $model->getMonetnikUrl() ?>"><i class="fa fa-eye"></i></a>
                <a href="<?= $model->getMonetnikUrl(true) ?>"><i class="fa fa-edit"></i></a><br/>


                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-minus"></i></span>
                    </div>
                    <?= Html::textInput('cost-2x', $model->getCalcultateSum(2), [
                        'class'       => 'form-control',
                        'placeholder' => '2X',
                        'disabled'    => true
                    ]) ?>
                    <?= Html::textInput('cost-2x8', $model->getCalcultateSum(2.8), [
                        'class'       => 'form-control',
                        'placeholder' => '2.8X',
                        'disabled'    => true
                    ]) ?>
                </div>

            </div>
            <div class="col-md-2 text-right">

                <?php if ($image = $model->file) : ?>
                    <?= Html::a(
                        Html::img($image->getUrl(File::SIZE_MIN), ['height' => 100]),
                        $image->getUrl(File::SIZE_ORG),
                        [
                            'data-jbox-image' => 'gl',
                            'data-pjax'       => 0
                        ]
                    ) ?>
                <?php else: ?>
                    <?= Html::img(File::getNoImageUrl(), ['height' => 100]) ?>
                <?php endif; ?>

            </div>
        </div>

    </div>
</div>
