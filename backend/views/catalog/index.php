<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use emilasp\commission\common\models\CommissionCatalogMode;
use emilasp\commission\common\models\CommissionCatalog;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionCatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Commission Catalogs';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="commission-catalog-index box box-primary">

        <div class="box-header with-border text-right">

            <a href="<?= Url::toRoute(['download-isset-products']) ?>" target="_blank"
               class="btn btn-success btn-sm float-left">
                <i class="fa fa-cart-plus"></i>
            </a>

            <a href="<?= Url::toRoute(['download-delivery-export', 'status' => CommissionCatalog::STATUS_NEW]) ?>"
               target="_blank"
               class="btn btn-info btn-sm float-left" title="Новые/на добавление в ИМ">
                <i class="fa fa-download"></i>
            </a>
            <a href="<?= Url::toRoute(['download-delivery-export', 'status' => CommissionCatalog::STATUS_ISSET]) ?>"
               target="_blank"
               class="btn btn-info btn-sm float-left" title="Повторы/имеющееся в ИМ">
                <i class="fa fa-download color-dark-blue"></i>
            </a>

            <div class="btn-group float-left modes-container" role="group">
                <?php foreach ($modes as $mode) : ?>
                    <a href="<?= Url::toRoute(['index', 'modeId' => $mode->id]) ?>"
                       class="btn btn-primary btn-sm <?= $mode->status ? 'active' : '' ?>">
                        <?= $mode->name ?>
                        <small class="mode-count-block">(<i><?= $mode->count ?></i>)</small>
                    </a>
                    <?php if ($mode->status) : ?>
                        <?php if (CommissionCatalogMode::getCurrentMode()->type !== CommissionCatalogMode::TYPE_DELIVERY) : ?>
                            <button class="btn btn-success btn-sm btn-check-open">
                                <i class="fa fa-angle-double-right"></i>
                            </button>
                        <?php endif; ?>

                        <a href="<?= Url::toRoute(['index', 'modeId' => $mode->id, 'modeReset' => 1]) ?>"
                           class="btn btn-danger btn-sm"
                           data-confirm="Обнулить?">
                            <i class="fa fa-trash"></i>
                        </a>
                    <?php endif; ?>
                <?php endforeach; ?>

            </div>


            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
                'class' => 'btn btn-success btn-flat'
            ]) ?>
        </div>

        <div class="box-body table-responsive no-padding">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin([
                'id'      => 'pjax-catalog',
                'timeout' => 10000
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                //'layout'       => "{items}\n{summary}\n{pager}",
                'layout'       => "{pager}\n{summary}\n{items}\n{summary}\n{pager}",
                'columns'      => [
                    [
                        'class'         => 'kartik\grid\ExpandRowColumn',
                        'expandOneOnly' => true,
                        'value'         => function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail'        => function ($model, $key, $index, $column) {
                            return Yii::$app->controller->renderpartial('_detail', ['model' => $model]);
                        }
                    ],
                    [
                        'attribute' => 'is_modify',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::checkbox('is_modify', $model->is_modify, [
                                'class' => 'form-control input-is_modify-row bg-white',
                            ]);
                        },
                        'filter'    => [0 => 'нет', 1 => 'ДА'],
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'status',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::tag('i', CommissionCatalog::$statuses[$model->status], [
                                    'class' => $model->im_id ? 'text-bold text-danger' : 'text-muted'
                            ]);
                        },
                        'filter'    => CommissionCatalog::$statuses,
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                        'visible'   => CommissionCatalogMode::getCurrentMode()->type === CommissionCatalogMode::TYPE_DELIVERY,
                    ],

                    [
                        'attribute' => 'is_modify_count',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::textInput('is_modify_count', $model->is_modify_count, [
                                'class'       => 'form-control input-is_modify_count-row bg-white',
                                'data-value'  => $model->is_modify_count,
                                'placeholder' => 'Поставка'
                            ]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                        'visible'   => CommissionCatalogMode::getCurrentMode()->type === CommissionCatalogMode::TYPE_DELIVERY,
                    ],

                    [
                        'attribute' => 'file',
                        'class'     => \emilasp\media\components\gridview\ImageColumn::className(),
                        'options'   => ['width' => '50px']
                    ],
                    'site',
                    [
                        'attribute' => 'type',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::dropDownList('type', $model->type, CommissionCatalog::$types, [
                                'class'       => 'form-contrsol input-type-row bg-white',
                                'data-value'  => $model->type,
                            ]);
                        },
                        'filter'    => CommissionCatalog::$types,
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                        'visible'   => CommissionCatalogMode::getCurrentMode()->type !== CommissionCatalogMode::TYPE_DELIVERY,
                    ],
                    [
                        'attribute' => 'year',
                        'class'     => '\kartik\grid\DataColumn',
                        'width'     => '100px',
                        'format'    => 'raw'
                    ],
                    [
                        'attribute' => 'article',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            $html = Html::beginTag('div', ['class' => 'input-group']);
                            $html .= Html::textInput('article', $model->article, [
                                'class'       => 'form-control input-article-row bg-white',
                                'data-value'  => $model->article,
                                'placeholder' => 'Артикул'
                            ]);
                            $html .= Html::beginTag('div', [
                                'class' => 'input-group-prepend',
                                'style' => 'width: 45px',
                            ]);
                            $html .= Html::button(Html::tag('i', '', ['class' => 'fa fa-search']), [
                                'class'       => 'btn btn-default btn-set-search',
                                'data-search' => $model->article_full
                            ]);
                            $html .= Html::endTag('div');
                            $html .= Html::endTag('div');
                            return $html;
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '250px',
                        'format'    => 'raw'
                    ],

                    [
                        'attribute' => 'cost',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::textInput('cost', $model->cost, [
                                'class'       => 'form-control input-cost-row bg-white',
                                'placeholder' => 'Стоимость'
                            ]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw'
                    ],

                    [
                        'attribute' => 'nominal',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::textInput('cost', $model->nominal, [
                                'class'       => 'form-control input-nominal-row bg-white',
                                'placeholder' => 'Номинал'
                            ]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw'
                    ],

                    [
                        'attribute' => 'im_id',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::textInput('cost', $model->im_id, [
                                'class'       => 'form-control input-im-id-row bg-white',
                                'data-value'  => $model->im_id,
                                'placeholder' => 'ИМ ID'
                            ]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '165px',
                        'format'    => 'raw'
                    ],

                    [
                        'attribute' => 'name',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            $html = Html::beginTag('div', ['class' => 'input-group']);
                            $html .= Html::textInput('name', $model->name, [
                                'class'       => 'form-control input-name-row bg-white',
                                'placeholder' => 'Наименование'
                            ]);
                            $html .= Html::beginTag('div', ['class' => 'input-group-append']);
                            $html .= Html::button(Html::tag('i', '', ['class' => 'fa fa-copy text-info']), [
                                'class'     => 'btn btn-default btn-copy-name',
                                'data-name' => $model->title
                            ]);
                            $html .= Html::endTag('div');
                            $html .= Html::endTag('div');

                            return $html;
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '600px',
                        'format'    => 'raw'
                    ],

                    [
                        'attribute' => 'IM',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            $muted = $model->im_id ? '' : 'text-muted';
                            $html  = '';
                            $html  .= Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-eye ' . $muted]),
                                $model->getMonetnikUrl(),
                                ['target' => '_blank', 'class' => 'monetnik-link-view']
                            );
                            $html  .= '&nbsp;&nbsp;';
                            $html  .= Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-edit ' . $muted]),
                                $model->getMonetnikUrl(true),
                                ['target' => '_blank', 'class' => 'monetnik-link-update']
                            );

                            $html .= '&nbsp;' . Html::button(Html::tag('i', '', ['class' => 'fa fa-plus']), [
                                    'class'    => 'btn btn-xs btn-success btn-create-monetnik',
                                    'disabled' => $model->im_id,
                                    'data-id'  => $model->id
                                ]);

                            return $html;
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw'
                    ],

                    //'description:ntext',
                    // 'cost',
                    // 'cost_r',
                    // 'url:url',
                    // 'data',
                    // 'year',
                    // 'created_at',
                    // 'updated_at',
                    // 'created_by',
                    // 'updated_by',

                    [
                        'class'    => \kartik\grid\ActionColumn::class,
                        'template' => '{copy} {view} {update} {date}',
                        // the default buttons + your custom button
                        'buttons'  => [
                            'copy' => function ($url, $model, $key) {     // render your custom button
                                return Html::a(
                                    Html::tag('i', '', ['class' => 'fa fa-copy']),
                                    ['/commission/catalog/copy', 'id' => $model->id],
                                    ['data-pjax' => 0, 'target' => '_blank']
                                );
                            },
                            'date' => function ($url, $model, $key) {     // render your custom button
                                return  '<br />' . Html::tag('small', date('Y-m-d', strtotime($model->created_at)), [
                                    'class' => 'text-muted'
                                ]);
                            },

                        ],
                        'width'    => '100px',
                    ],
                ],
                'rowOptions'   => function ($model) {
                    if (count(explode(',', $model->article_full)) > 2) {
                        return ['class' => 'table-warning'];
                    }

                    if ($model->status === CommissionCatalog::STATUS_ISSET) {
                        return ['class' => 'table-success'];
                    }
                    return null;
                }
            ]); ?>

            <?php Pjax::end(); ?>

        </div>

    </div>


<?php

$url       = Url::toRoute(['/commission/catalog/update-ajax']);
$urlCreate = Url::toRoute(['/commission/catalog/monetnik-create-ajax']);

$js = <<<JS

    $(document).on('click', '.btn-set-search', function() {
        var button = $(this);
        var search = button.data('search');
        var searchInput = $('[name="CommissionCatalogSearch[article]"]');
        
        searchInput.val(search);
        searchInput.change();
        console.log(search, searchInput);
    });

    $(document).on('click', '.btn-check-open', function() {
        var rows = $('#pjax-catalog table tbody tr');
        
        var newItems = [];
        rows.each(function (index, item) {
            if (newItems.length < 10) {
                var itemRow = $(item);
                var isChecked = itemRow.find('[name="is_modify"]');
                var imId = itemRow.find('.input-im-id-row').val();
                
                if (imId && isChecked.length && !isChecked.is(':checked')) {
                    newItems.push(isChecked);
                    
                    var link = itemRow.find('.monetnik-link-update').attr('href');
                    
                    console.log(item, itemRow, link);
                    
                    window.open(link, '_blank');
                     
                    isChecked.attr('checked', true); 
                    isChecked.change();
                }
            }
        });
    });


    $(document).on('change', '.input-is_modify-row', function() {
        var container = $(this).closest('tr');
        var modeCountBlock = $('.modes-container .active .mode-count-block i');
        var modeCountBlockValue = parseInt(modeCountBlock.text());
        
        var id = container.data('key');
        
        var is_modify = container.find('.input-is_modify-row').is(':checked') ? 1 : 0;
        var is_modify_count = container.find('.input-is_modify_count-row').val();
        
        if (!is_modify) {
            is_modify_count = 0;
            container.find('.input-is_modify_count-row').val(is_modify_count)
        }
        
        modeCountBlock.html(is_modify ? modeCountBlockValue + 1 : modeCountBlockValue - 1);
        
        sendSaveData(container, {id:id, is_modify: is_modify, is_modify_count:is_modify_count});
    });

    $(document).on('change', '.input-article-row, .input-im-id-row, .input-name-row, .input-cost-row, .input-nominal-row, .input-type-row', function() {
        var container = $(this).closest('tr');
        
        var id = container.data('key');
        
        var article = container.find('.input-article-row').val();
        var name = container.find('.input-name-row').val();
        var imId = container.find('.input-im-id-row').val();
        var cost = container.find('.input-cost-row').val();
        var nominal = container.find('.input-nominal-row').val();
        var type = container.find('.input-type-row').val();
        
        var articleOld = container.find('.input-article-row').data('value');
        var imIdOld = container.find('.input-im-id-row').data('value');
                
        if (articleOld && articleOld != article) {
            if (!confirm('Изменить артикул?')) {
                container.find('.input-article-row').val(articleOld);
                return;
            }
        }
        if (imIdOld && imIdOld != imId) {
            if (!confirm('Изменить идентификатор магазина?')) {
                container.find('.input-im-id-row').val(imIdOld)
                return;
            }
        }
        
        sendSaveData(container, {id:id, article: article, name: name, im_id:imId, cost:cost, nominal:nominal, type:type});
    });
    
    $(document).on('change', '.input-is_modify_count-row', function() {
        var container = $(this).closest('tr');
        var id = container.data('key');
        var isModify = container.find('.input-is_modify-row');
        var is_modify = container.find('.input-is_modify-row').is(':checked') ? 1 : 0;
        var is_modify_count = container.find('.input-is_modify_count-row').val();
        
        if (!is_modify) {
            var modeCountBlock = $('.modes-container .active .mode-count-block i');
            var modeCountBlockValue = parseInt(modeCountBlock.text());
            
            modeCountBlock.html(modeCountBlockValue + 1);
        }
        
        isModify.attr('checked', true);
        
        sendSaveData(container, {id:id, is_modify_count:is_modify_count, is_modify: 1});
    });


    
    function sendSaveData(container, data, callable) {
        $.ajax({
            type: 'POST',
            url: '{$url}',
            dataType: "json",
            data: $.param(data),
            success: function(msg) {
                notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
            }
        });  
    }
    
    $(document).on('click', '.btn-create-monetnik', function () {
        var button = $(this);
        var icon = button.find('i');
        var id = button.data('id');
        var container = button.closest('tr');
        var linkView = container.find('.monetnik-link-view');
        var linkUpdate = container.find('.monetnik-link-update');
        var inputMId = container.find('.input-im-id-row');
        var cost = container.find('.input-cost-row').val();
        var nominal = container.find('.input-nominal-row').val();
        var is_modify_count = container.find('.input-is_modify_count-row').val();
        var is_modify = container.find('.input-is_modify-row').is(':checked');
        
        if (!cost || !cost.length) {
            notice('Не высталена базовая цена товара', 'red');
            return;
        }
        if (!nominal || !nominal.length) {
            notice('Не выстален номинал товара', 'red');
            return;
        }
        if (is_modify_count == 0) {
            notice('Не высталено количество для поставки товара', 'red');
            return;
        }
        if (!is_modify) {
            notice('Не высталена галка', 'red');
            return;
        }
        
        button.attr('disabled', true);
        icon.removeClass('fa-plus').addClass('text-danger').addClass('fa-sync').addClass('fa-spin');
        
        
        $.ajax({
            type: 'POST',
            url: '{$urlCreate}',
            dataType: "json",
            data: $.param({id:id}),
            success: function(msg) {
                notice(msg.message, (msg.status=='1' ? 'green' : 'red'));
                
                var linkUpdateUrl = 'https://www.monetnik.ru/goods/edit/' + msg.data + '/';
                
                linkView.find('i').removeClass('text-muted');
                linkUpdate.find('i').removeClass('text-muted');
                linkView.attr('href', 'https://www.monetnik.ru/pochtovye-marki/rossiya/listy/100-r-' + msg.data + '/');
                linkUpdate.attr('href', linkUpdateUrl);
                
                inputMId.val(msg.data);
                
                button.attr('disabled', true);
                icon.addClass('fa-plus').removeClass('fa-sync').removeClass('fa-spin').removeClass('text-danger');
                
                if (typeof callable === 'function') {
                    callable();
                }
                
                container.find('.input-is_modify-row').attr('checked', true);
                container.find('.input-is_modify-row').change();
                
                container.addClass('table-success');
                
                window.open(linkUpdateUrl, '_blank');
            }
        }); 
    });
    
    $(document).on('click', '.btn-copy-name', function () {
        var names = $(this).data('name');
        
        console.log(name);
        
        var temp = $("<input>");
        $(this).append(temp);
        temp.val(names);
        temp.select();
        document.execCommand("copy");
        temp.remove();
    });
    
       
JS;

$this->registerJs($js);