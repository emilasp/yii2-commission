<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use emilasp\commission\common\models\CommissionCatalog;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\search\CommissionCatalogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-catalog-search">

    <?php $form = ActiveForm::begin([
        'id' => 'form-search-id',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-10">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="width: 70px">
                        <?= Html::activeCheckbox($model, 'is_live', ['label' => false]) ?>ИМ
                    </div>
                </div>
                <div class="input-group-prepend">
                    <div class="input-group-text" style="width: 100px">
                        <?= Html::activeCheckbox($model, 'is_series', ['label' => false]) ?> серия
                    </div>
                </div>
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <?= Html::activeDropDownList($model, 'type', ArrayHelper::merge(['' => ''], CommissionCatalog::$types)) ?>
                    </div>
                </div>

                <?= Html::activeTextInput($model, 'search', [
                    'class' => 'form-control',
                    'placeholder' => 'Поиск по всем атрибутам: год, наименование, описание и т.д.'
                ]) ?>

            </div>
        </div>
        <div class="col-md-2">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="width: 80px">Период</div>
                </div>
                <?= Html::activeTextInput($model, 'years[0]', [
                    'class' => 'form-control',
                    'placeholder' => 'с года'
                ]) ?>
                <div class="input-group-prepend">
                    <div class="input-group-text" style="width: 40px">до</div>
                </div>
                <?= Html::activeTextInput($model, 'years[1]', [
                    'class' => 'form-control',
                    'placeholder' => 'по год'
                ]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
    $(document).on('change', '#commissioncatalogsearch-type, #commissioncatalogsearch-search, #commissioncatalogsearch-is_live, #commissioncatalogsearch-is_series', function () {
        $('#form-search-id').submit();
    }); 
    
    $('#commissioncatalogsearch-search, #commissioncatalogsearch-years-0, #commissioncatalogsearch-years-1').keypress(function (e) {
        if (e.which == 13) {
            $('#form-search-id').submit();
            return false;
        }
    });
JS;


$this->registerJs($js);