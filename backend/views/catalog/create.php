<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionCatalog */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => 'Commission Catalogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-catalog-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
