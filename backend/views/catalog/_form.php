<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use emilasp\media\models\File;
use emilasp\commission\common\models\CommissionCatalog;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionCatalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-catalog-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">


        <div class="row">
            <div class="col-md-1">
                <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-1">
                <?= $form->field($model, 'type')->dropDownList(CommissionCatalog::$types) ?>
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-8">
                        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'im_id')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-2">
                        <hr/>
                        <a href="<?= $model->getMonetnikUrl() ?>"><i class="fa fa-eye"></i></a><br/>
                        <a href="<?= $model->getMonetnikUrl(true) ?>"><i class="fa fa-edit"></i></a>
                    </div>
                    <div class="col-md-4">
                        <hr/>
                        <a href="<?= Url::toRoute([
                            '/commission/check-price/create',
                            'catalog_id' => $model->id,
                            'im_id'      => $model->im_id,
                            'year'       => $model->year,
                            'nominal'    => $model->nominal,
                        ]) ?>"
                           class="btn btn-info">
                            <i class="fa fa-eye"></i> Создать чекер цены
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <?php if ($model->file) : ?>
            <?= Html::a(
                Html::img($model->file->getUrl(File::SIZE_ICO), [
                    'class'    => 'img-thumbnail',
                    'data-src' => $model->file->getUrl(File::SIZE_MAX),
                    'alt'      => $model->file->title,
                ]),
                $model->file->getUrl(File::SIZE_MAX),
                [
                    'data-jbox-image' => 'gl',
                    'data-pjax'       => 0,
                ]
            ) ?>
        <?php else : ?>
            <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail']); ?>
        <?php endif ?>

        <?= $form->field($model, 'imageUpload')->fileInput()->label(false) ?>


        <div class="row">
            <div class="col-md-6">

                <?php $data = json_decode($model->data, true); ?>

                <?php if ($data) : ?>
                    <ul class="nav nav-tabs">
                        <?php foreach ($data as $index => $dataItem) : ?>
                            <li>
                                <a data-toggle="tab" href="#tab<?= $index ?>"
                                   class="nav-link <?= !$index ? 'active' : '' ?>">
                                    <?= $dataItem['cost'] ?? '' ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?php foreach ($data as $index => $dataItem) : ?>
                            <?php $url = 'http://rusmarka.ru' . ($dataItem['image'] ?? '') ?>

                            <div id="tab<?= $index ?>" class="tab-pane <?= !$index ? 'active' : '' ?>">

                                <div class="text-center"><img src="<?= $url ?>" height="200px"/></div>

                                <?php $params = \yii\helpers\ArrayHelper::remove($dataItem, 'params') ?>
                                <?= \yii\widgets\DetailView::widget([
                                    'model'      => (object)$dataItem,
                                    'attributes' => array_keys($dataItem),
                                ]) ?>

                                <hr/>

                                <?= \yii\widgets\DetailView::widget([
                                    'model'      => $params,
                                    'attributes' => array_keys($params),
                                ]) ?>

                                <hr/>

                                <?= Html::textarea('nameName', $model->getTitle($index),
                                    ['class' => 'form-control', 'rows' => 2]) ?>

                                <?= Html::textarea('tableParams', $model->getTableParams($index),
                                    ['class' => 'form-control', 'rows' => 6]) ?>
                            </div>

                        <?php endforeach; ?>

                    </div>
                <?php endif; ?>

            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'cost')->textInput() ?>
                    </div>
                    <div class="col-md-3">
                        <label>2Х</label><br/>
                        <?= $model->getCalcultateSum(2) ?>
                    </div>
                    <div class="col-md-3">
                        <label>2.8Х</label><br/>
                        <?= $model->getCalcultateSum(2.8) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'cost_r')->textInput() ?>
                    </div>
                </div>

                <?= $form->field($model, 'year')->textInput() ?>

                <?= $form->field($model, 'nominal')->textInput() ?>

                <?= $form->field($model, 'circulation')->textInput() ?>

                <?= $form->field($model, 'data')->textarea(['rows' => 10]) ?>

            </div>
        </div>


    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
            Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
            . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
            ['class' => 'btn btn-success btn-flat']
        ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
