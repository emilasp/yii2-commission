<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commission Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-product-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
            'class' => 'btn btn-success btn-flat'
        ]) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'category_id',
                'article',
                'name',
                'count',
                // 'cost',
                // 'cost_r',
                // 'price',
                // 'price_sale',
                // 'description:ntext',
                // 'comment:ntext',
                // 'keywords:ntext',
                // 'data_url:url',
                // 'data_url_base:url',
                // 'data_name',
                // 'data_type',
                // 'data_printing_method',
                // 'data_printing_count',
                // 'data_paper',
                // 'data_colors',
                // 'data_perforation',
                // 'data_format_block',
                // 'data_format_stamp',
                // 'data_nominal',
                // 'data_year',
                // 'data_artist_designer',
                // 'data_themes:ntext',
                // 'status',
                // 'created_at',
                // 'updated_at',
                // 'created_by',
                // 'updated_by',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

            <?php Pjax::end(); ?>

    </div>

</div>
