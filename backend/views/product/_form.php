<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use emilasp\commission\common\models\CommissionProduct;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-product-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <div class="row">
            <div class="col-md-6">

                <div class="card">
                    <div class="card-header bg-primary text-white">
                        Модель
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'status')->dropDownList(CommissionProduct::$statuses) ?>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'guid')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'price_sale')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'price')->textInput() ?>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
                            </div>
                        </div>

                        <?= $form->field($model, 'keywords')->textarea(['rows' => 6]) ?>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'category_id')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'count')->textInput() ?>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <div class="col-md-6">

                <div class="card">
                    <div class="card-header">
                        ДАТА
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'cost')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'cost_r')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_url')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_url_base')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <?= $form->field($model, 'data_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'data_type')->textInput(['maxlength' => true]) ?>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_printing_method')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_printing_count')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_paper')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_colors')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_format_block')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'data_format_stamp')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>

                        <?= $form->field($model, 'data_perforation')->textInput(['maxlength' => true]) ?>


                        <?= $form->field($model, 'data_nominal')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'data_year')->textInput() ?>

                        <?= $form->field($model, 'data_artist_designer')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'data_themes')->textarea(['rows' => 6]) ?>


                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
            Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
            . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
            ['class' => 'btn btn-success btn-flat']
        ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
