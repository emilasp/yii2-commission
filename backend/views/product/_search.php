<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\search\CommissionProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'article') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'count') ?>

    <?php // echo $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'cost_r') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'price_sale') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'keywords') ?>

    <?php // echo $form->field($model, 'data_url') ?>

    <?php // echo $form->field($model, 'data_url_base') ?>

    <?php // echo $form->field($model, 'data_name') ?>

    <?php // echo $form->field($model, 'data_type') ?>

    <?php // echo $form->field($model, 'data_printing_method') ?>

    <?php // echo $form->field($model, 'data_printing_count') ?>

    <?php // echo $form->field($model, 'data_paper') ?>

    <?php // echo $form->field($model, 'data_colors') ?>

    <?php // echo $form->field($model, 'data_perforation') ?>

    <?php // echo $form->field($model, 'data_format_block') ?>

    <?php // echo $form->field($model, 'data_format_stamp') ?>

    <?php // echo $form->field($model, 'data_nominal') ?>

    <?php // echo $form->field($model, 'data_year') ?>

    <?php // echo $form->field($model, 'data_artist_designer') ?>

    <?php // echo $form->field($model, 'data_themes') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
