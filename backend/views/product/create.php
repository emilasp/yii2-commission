<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionProduct */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => 'Commission Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-product-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
