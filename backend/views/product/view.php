<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionProduct */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Commission Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-product-view box box-primary">
    <div class="box-header text-right">
        <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'category_id',
                'article',
                'name',
                'count',
                'cost',
                'cost_r',
                'price',
                'price_sale',
                'description:ntext',
                'comment:ntext',
                'keywords:ntext',
                'data_url:url',
                'data_url_base:url',
                'data_name',
                'data_type',
                'data_printing_method',
                'data_printing_count',
                'data_paper',
                'data_colors',
                'data_perforation',
                'data_format_block',
                'data_format_stamp',
                'data_nominal',
                'data_year',
                'data_artist_designer',
                'data_themes:ntext',
                'status',
                'created_at:datetime',
                'updated_at:datetime',
                'created_by',
                'updated_by',
            ],
        ]) ?>
    </div>
</div>
