<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionProduct */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => 'Commission Product',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commission Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="commission-product-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
