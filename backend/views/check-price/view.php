<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\ImCheckPrice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Im Check Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="im-check-price-view box box-primary">
    <div class="box-header text-right">
        <?= Html::a(Yii::t('site', 'Clone'), ['clone', 'id' => $model->id], ['class' => 'btn btn-info btn-flat']) ?>
        <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'catalog_id',
                'price_our',
                'price_min',
                'price_max',
                'im_id',
                'price_min_im_id',
                'year',
                'category',
                'nominal',
                'status',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ]) ?>
    </div>
</div>
