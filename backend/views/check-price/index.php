<?php

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\ImCheckPrice;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\ImCheckPriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Im Check Prices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="im-check-price-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-eye']) . ' ' . Yii::t('site', 'Check'), ['check'], [
            'class' => 'btn btn-info btn-flat'
        ]) ?>

        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
            'class' => 'btn btn-success btn-flat'
        ]) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'layout'       => "{items}\n{summary}\n{pager}",
            'columns'      => [
                //['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'catalog.file',
                    'class'     => \emilasp\media\components\gridview\ImageColumn::className(),
                    'options'   => ['width' => '22px']
                ],

                [
                    'attribute' => 'is_set',
                    'label'     => 'Набор',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::tag('i', null,
                            ['class' => ('fa fa-' . ($model->catalog->isSet() ? 'check-circle text-success' : 'circle text-muted'))]);
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '70px',
                    'format'    => 'raw',
                ],

                [
                    'attribute' => 'catalog_id',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::a(
                            $model->catalog->name,
                            \yii\helpers\Url::toRoute(['/commission/catalog/update', 'id' => $model->catalog_id]),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw',
                    'filter'    => \emilasp\commission\common\models\CommissionCatalog::find()
                        ->where(['id' => ImCheckPrice::find()->select('catalog_id')->column()])->map()->all()
                ],
                'nominal',
                'year',
                [
                    'attribute' => 'price_our',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::a(
                            Yii::$app->formatter->asCurrency($model->price_our),
                            ImCheckPrice::getMonetnikUrl($model->im_id),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw'
                ],

                [
                    'attribute' => 'price_min',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        if (!$model->price_min_im_id) {
                            return Html::tag('span', Yii::$app->formatter->asCurrency($model->price_min), [
                                'class' => 'text-muted'
                            ]);
                        }
                        return Html::a(
                            Yii::$app->formatter->asCurrency($model->price_min),
                            ImCheckPrice::getMonetnikUrl($model->price_min_im_id),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw'
                ],
                'price_max',
                // 'im_id',
                // 'price_min_im_id',
                // 'year',
                // 'category',
                // 'nominal',
                [
                    'attribute' => 'status',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return ImCheckPrice::$statuses[$model->status];
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw',
                    'filter'    => ImCheckPrice::$statuses
                ],
                // 'created_at',
                // 'updated_at',

                [
                    'class'    => \kartik\grid\ActionColumn::class,
                    'template' => '{opensearch} {openmin} {openour} {update}',
                    // the default buttons + your custom button
                    'buttons'  => [
                        'openmin'    => function ($url, $model, $key) {
                            if (!$model->price_min_im_id) {
                                return Html::tag('i', '', ['class' => 'fa fa-caret-down text-muted']);
                            }

                            return Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-caret-down']),
                                ImCheckPrice::getMonetnikUrl($model->price_min_im_id),
                                ['data-pjax' => 0, 'target' => '_blank']
                            );
                        },
                        'openour'    => function ($url, $model, $key) {     // render your custom button
                            return Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-eye']),
                                ImCheckPrice::getMonetnikUrl($model->im_id),
                                ['data-pjax' => 0, 'target' => '_blank']
                            );
                        },
                        'opensearch' => function ($url, $model, $key) {     // render your custom button
                            return Html::a(
                                Html::tag('i', '', ['class' => 'fa fa-list']),
                                $model->monetnikSearchUrl,
                                ['data-pjax' => 0, 'target' => '_blank']
                            );
                        }
                    ],
                    'width'    => '100px',
                ],
            ],
            'rowOptions'   => function ($model) {
                if ($model->status === ImCheckPrice::STATUS_DISABLED) {
                    return ['class' => 'text-muted'];
                } elseif ($model->status === ImCheckPrice::STATUS_HIDED && !$model->catalog->isSet()) {
                    return ['class' => 'text-danger table-danger'];
                }
                if (!(int)$model->price_min) {
                    if ($model->status === ImCheckPrice::STATUS_WAIT) {
                        return ['class' => 'table-warning'];
                    }
                    return ['class' => 'table-success'];
                } elseif ($model->price_our > $model->price_min) {
                    return ['class' => 'table-danger'];
                } elseif (!in_array($model->catalog->type,
                        CommissionCatalog::$typeSets) && $model->price_our < $model->price_min) {
                    return ['class' => 'table-info'];
                }/* elseif (in_array($model->catalog->type, CommissionCatalog::$typeSets) && $model->price_our < $model->price_min) {
                    return ['class' => 'table-warning'];
                }*/
                return null;
            }
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>
