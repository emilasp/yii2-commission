<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\ImCheckPrice */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => 'Im Check Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="im-check-price-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
