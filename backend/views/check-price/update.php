<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\ImCheckPrice */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => 'Im Check Price',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Im Check Prices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="im-check-price-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
