<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use emilasp\commission\common\models\ImCheckPrice;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\ImCheckPrice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="im-check-price-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'catalog_id')->textInput() ?></div>
            <div class="col-md-4"><?= $form->field($model, 'im_id')->textInput(['maxlength' => true]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'price_our')->textInput() ?></div>
        </div>


        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'year')->textInput() ?></div>
            <div class="col-md-4"><?= $form->field($model, 'nominal')->textInput(['maxlength' => true]) ?></div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList(ImCheckPrice::$statuses) ?></div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <label><b>MIN:</b></label> <?= $model->price_min ?><br/>
                <label><b>MAX:</b></label> <?= $model->price_max ?><br/>
                <label>ID с минимальной ценой: </label><?= $model->price_min_im_id ?>
            </div>
            <div class="col-md-4">

                <?php if (!$model->isNewRecord) : ?>
                    <?php if ($model->price_min_im_id) : ?>
                        <div class="alert-<?= $model->price_our > $model->price_min ? 'danger' : 'success' ?>">
                            <?= Html::a(
                                'Ссылка на товар с минимальной стоимостью',
                                ImCheckPrice::getMonetnikUrl($model->price_min_im_id),
                                ['target' => '_blank']
                            );
                            ?>
                        </div>
                    <?php else: ?>
                        <div class="text-muted">Ссылка на товар с минимальной стоимостью отсутствует</div>
                    <?php endif; ?>

                    <?= Html::a(
                        'Ссылка на наш товар',
                        ImCheckPrice::getMonetnikUrl($model->im_id),
                        ['target' => '_blank']
                    );
                    ?>

                <?php endif; ?>
            </div>
        </div>


    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
            Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
            . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
            ['class' => 'btn btn-success btn-flat']
        ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
