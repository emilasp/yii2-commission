<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use emilasp\commission\common\models\CommissionCatalog;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionReserveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Analize sales';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="commission-reserve-analize box box-primary">

        <div class="box-header with-border text-right">
            Analize
        </div>


        <div class="box-body table-responsive no-padding">
            Analize

        </div>

    </div>


<?php

$url = Url::toRoute(['/commission/reserve/update-ajax']);

$js = <<<JS
    
    $(document).on('change', '.input-per_list-row', function() {
        var container = $(this).closest('tr');
        var id = container.data('key');
        var perList = container.find('.input-per_list-row').val();
        
        sendSaveData(container, {id:id, per_list:perList});
    });


    
    function sendSaveData(container, data, callable) {
        $.ajax({
            type: 'POST',
            url: '{$url}',
            dataType: "json",
            data: $.param(data),
            success: function(msg) {
                notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
            }
        });  
    }
    
       
JS;

$this->registerJs($js);