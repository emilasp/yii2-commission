<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionReserve */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => 'Commission Reserve',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commission Reserves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="commission-reserve-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
