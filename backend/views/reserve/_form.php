<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionReserve */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-reserve-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'im_id')->textInput() ?>

        <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'count')->textInput() ?>

        <?= $form->field($model, 'sell_day')->textInput() ?>

        <?= $form->field($model, 'to_bay')->textInput() ?>

        <?= $form->field($model, 'days_isset')->textInput() ?>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
                Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
                . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
