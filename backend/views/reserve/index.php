<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use emilasp\commission\common\models\CommissionCatalog;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionReserveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Commission Reserves';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="commission-reserve-index box box-primary">

        <div class="box-header with-border text-right">

            <a href="<?= Url::toRoute(['download-reserve-export', 'site' => 'marka']) ?>"
               target="_blank"
               class="btn btn-primary btn-sm float-left" title="marka">
                <i class="fa fa-download color-dark-blue"></i> ООО Марка
            </a>
            <a href="<?= Url::toRoute(['download-reserve-export', 'site' => 'markimira']) ?>"
               target="_blank"
               class="btn btn-primary btn-sm float-left" title="markimira">
                <i class="fa fa-download color-dark-blue"></i> СССР
            </a>


            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Открыть в каталоге'),
                ['catalog'], [
                    'class' => 'btn btn-success btn-flat'
                ]) ?>
        </div>


        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'layout'       => "{items}\n{summary}\n{pager}",
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'catalogItem.file',
                        'class'     => \emilasp\media\components\gridview\ImageColumn::className(),
                        'options'   => ['width' => '50px']
                    ],
                    [
                        'attribute' => 'site',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return ArrayHelper::getValue($model, 'catalogItem.site');
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                        //'filter' => CommissionCatalog::find()->distinct()->map('site', 'site')->all()
                    ],
                    [
                        'attribute' => 'article',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return 'IM: ' . $model->im_id . '<br />A: ' . ArrayHelper::getValue($model,
                                    'catalogItem.article');
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'cost',
                        'label'     => 'Стоимость',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            $catalogModel = $model->catalogItem;
                            $cost         = $catalogModel->cost ?? 0;
                            $sum          = $model->to_bay * $cost;
                            return "<div>{$cost}</div><div class='text-muted'>{$sum}</div>";
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'per_list',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::textInput('per_list', $model->catalogItem->per_list, [
                                'class'       => 'form-control input-per_list-row bg-white',
                                'placeholder' => 'На листе'
                            ]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw'
                    ],
                    'name',
                    'count',
                    'sell_day',
                    'to_bay',
                    'days_isset',
                    [
                        'attribute' => 'IM',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            $html = '-';

                            if ($model = $model->catalogItem) {
                                $muted = $model->im_id ? '' : 'text-muted';
                                $html  = '';
                                $html  .= Html::a(
                                    Html::tag('i', '', ['class' => 'fa fa-eye ' . $muted]),
                                    $model->getMonetnikUrl(),
                                    ['target' => '_blank', 'class' => 'monetnik-link-view']
                                );
                                $html  .= '&nbsp;&nbsp;';
                                $html  .= Html::a(
                                    Html::tag('i', '', ['class' => 'fa fa-edit ' . $muted]),
                                    $model->getMonetnikUrl(true),
                                    ['target' => '_blank', 'class' => 'monetnik-link-update']
                                );

                                $html .= '&nbsp;' . Html::button(Html::tag('i', '', ['class' => 'fa fa-plus']), [
                                        'class'    => 'btn btn-xs btn-success btn-create-monetnik',
                                        'disabled' => $model->im_id,
                                        'data-id'  => $model->id
                                    ]);
                            }

                            return $html;
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw'
                    ],
                    [
                        'class'    => \kartik\grid\ActionColumn::class,
                        'template' => '{catalog}',
                        // the default buttons + your custom button
                        'buttons'  => [
                            'catalog' => function ($url, $model, $key) {     // render your custom button
                                return Html::a(
                                    Html::tag('i', '', ['class' => 'fa fa-edit']),
                                    ['/commission/catalog/update', 'id' => $model->catalogItem->id ?? null],
                                    ['data-pjax' => 0, 'target' => '_blank']
                                );
                            }
                        ],
                        'width'    => '100px',
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>

    </div>


<?php

$url = Url::toRoute(['/commission/reserve/update-ajax']);

$js = <<<JS
    
    $(document).on('change', '.input-per_list-row', function() {
        var container = $(this).closest('tr');
        var id = container.data('key');
        var perList = container.find('.input-per_list-row').val();
        
        sendSaveData(container, {id:id, per_list:perList});
    });


    
    function sendSaveData(container, data, callable) {
        $.ajax({
            type: 'POST',
            url: '{$url}',
            dataType: "json",
            data: $.param(data),
            success: function(msg) {
                notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
            }
        });  
    }
    
       
JS;

$this->registerJs($js);