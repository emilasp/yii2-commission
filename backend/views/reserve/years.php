<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Years';
$this->params['breadcrumbs'][] = ['label' => 'Commission Reserves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-reserve-view box box-primary">
    <div class="box-header text-right">

    </div>
    <div class="box-body no-padding">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout'       => "{items}\n{summary}\n{pager}",
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'year',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::a($model['year'], \yii\helpers\Url::toRoute(['/commission/reserve/years', 'year' => $model['year']]), [
                                'target' => '_blank',
                        ]);
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '100px',
                    'format'    => 'raw',
                    //'filter' => CommissionCatalog::find()->distinct()->map('site', 'site')->all()
                ],
                'year',
                'all',
                'isset',

            ],
        ]); ?>

    </div>
</div>
