<?php

use emilasp\commission\common\models\CommissionCatalogMode;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use emilasp\commission\common\models\CommissionCatalog;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionReserveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Commission Reserves';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-reserve-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
            'class' => 'btn btn-success btn-flat'
        ]) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout'       => "{items}\n{summary}\n{pager}",
            'columns'      => [
                [
                    'attribute' => 'file',
                    'class'     => \emilasp\media\components\gridview\ImageColumn::className(),
                    'options'   => ['width' => '50px']
                ],
                'site',
                [
                    'attribute' => 'year',
                    'class'     => '\kartik\grid\DataColumn',
                    'width'     => '100px',
                    'format'    => 'raw'
                ],
                'article',
                'cost:currency',
                'nominal',
                'im_id',
                'name',


                [
                    'attribute' => 'IM',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        $muted = $model->im_id ? '' : 'text-muted';
                        $html  = '';
                        $html  .= Html::a(
                            Html::tag('i', '', ['class' => 'fa fa-eye ' . $muted]),
                            $model->getMonetnikUrl(),
                            ['target' => '_blank', 'class' => 'monetnik-link-view']
                        );
                        $html  .= '&nbsp;&nbsp;';
                        $html  .= Html::a(
                            Html::tag('i', '', ['class' => 'fa fa-edit ' . $muted]),
                            $model->getMonetnikUrl(true),
                            ['target' => '_blank', 'class' => 'monetnik-link-update']
                        );

                        $html .= '&nbsp;' . Html::button(Html::tag('i', '', ['class' => 'fa fa-plus']), [
                                'class'    => 'btn btn-xs btn-success btn-active-monetnik',
                                'data-id'  => $model->id
                            ]);

                        return $html;
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '100px',
                    'format'    => 'raw'
                ],

            ],
            'rowOptions'   => function ($model) {
                if (count(explode(',', $model->article_full)) > 2) {
                    return ['class' => 'table-warning'];
                }

                if ($model->status === CommissionCatalog::STATUS_ISSET) {
                    return ['class' => 'table-success'];
                }
                return null;
            }
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>


<?php

$url = Url::toRoute(['/commission/in-active/activated']);

$js = <<<JS

    
    $(document).on('click', '.btn-active-monetnik', function () {
        var button = $(this);
        var icon = button.find('i');
        var id = button.data('id');
        
        button.attr('disabled', true);
        icon.removeClass('fa-plus').addClass('text-danger').addClass('fa-sync').addClass('fa-spin');
        
        $.ajax({
            type: 'POST',
            url: '{$url}',
            dataType: "json",
            data: $.param({id:id}),
            success: function(msg) {
                notice(msg.message, (msg.status=='1' ? 'green' : 'red'));
                
                button.attr('disabled', true);
                icon.addClass('fa-plus').addClass('text-success').removeClass('fa-sync').removeClass('fa-spin').removeClass('text-danger');
            }
        }); 
    });

JS;

$this->registerJs($js);