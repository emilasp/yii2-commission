<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrderAnalize */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => 'Commission Order Analize',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commission Order Analizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->im_id, 'url' => ['view', 'id' => $model->im_id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="commission-order-analize-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
