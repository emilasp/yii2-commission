<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrderAnalize */

$this->title = $model->im_id;
$this->params['breadcrumbs'][] = ['label' => 'Commission Order Analizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-order-analize-view box box-primary">
    <div class="box-header text-right">
        <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->im_id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->im_id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'im_id',
                'count',
                'cost',
                'sum',
            ],
        ]) ?>
    </div>
</div>
