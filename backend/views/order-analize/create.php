<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrderAnalize */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => 'Commission Order Analizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-order-analize-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
