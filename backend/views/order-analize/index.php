<?php

use emilasp\commission\common\models\CommissionCatalog;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionOrderAnalizeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commission Order Analizes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-order-analize-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
            'class' => 'btn btn-success btn-flat'
        ]) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                [
                    'attribute' => 'catalog.file',
                    'class'     => \emilasp\media\components\gridview\ImageColumn::className(),
                    'options'   => ['width' => '22px']
                ],
                [
                    'attribute' => 'type',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return  CommissionCatalog::$types[$model->type] ?? null;
                    },
                    'filter'    => CommissionCatalog::$types,
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '100px',
                    'format'    => 'raw',
                ],
                [
                    'attribute' => 'im_id',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::a(
                            $model->catalog->name,
                            \yii\helpers\Url::toRoute(['/commission/catalog/update', 'id' => $model->catalog->id]),
                            ['data-pjax' => 0, 'target' => '_blank']
                        );
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw',
                ],
                'count',
                'cost',
                'sum',


                [
                    'attribute' => 'IM',
                    'class'     => '\kartik\grid\DataColumn',
                    'value'     => function ($model, $key, $index, $column) {
                        $html  = '';
                        $html  .= Html::a(
                            Html::tag('i', '', ['class' => 'fa fa-eye ']),
                            $model->catalog->getMonetnikUrl(),
                            ['target' => '_blank', 'class' => 'monetnik-link-view']
                        );
                        $html  .= '&nbsp;&nbsp;';
                        $html  .= Html::a(
                            Html::tag('i', '', ['class' => 'fa fa-edit ']),
                            $model->catalog->getMonetnikUrl(true),
                            ['target' => '_blank', 'class' => 'monetnik-link-update']
                        );

                        return $html;
                    },
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '100px',
                    'format'    => 'raw'
                ],
            ],
        ]); ?>

            <?php Pjax::end(); ?>

    </div>

</div>
