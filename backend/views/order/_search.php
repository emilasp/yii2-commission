<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\search\CommissionOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'number') ?>

    <?= $form->field($model, 'date_at') ?>

    <?= $form->field($model, 'count') ?>

    <?= $form->field($model, 'sum') ?>

    <?php // echo $form->field($model, 'is_payed') ?>

    <?php // echo $form->field($model, 'is_monetized') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
