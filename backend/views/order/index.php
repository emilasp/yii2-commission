<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use miloschuman\highcharts\Highcharts;
use emilasp\commission\common\models\CommissionOrder;
use kartik\date\DatePicker;
use emilasp\commission\common\models\CommissionCatalog;


/* @var $this yii\web\View */
/* @var $searchModel emilasp\commission\common\models\search\CommissionOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Commission Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="commission-order-index box box-primary">

        <div class="box-header with-border text-right" style="color: #2d48aa">
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
                'class' => 'btn btn-success btn-flat'
            ]) ?>
        </div>


        <div class="row">
            <div class="col-md-9">
                <?php Pjax::begin(['id' => 'pjax-chart']); ?>


                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#base" class="nav-link active">
                            Все
                            <small class="text-success">
                                <?= Yii::$app->formatter->asInteger($sumsByDay[date('d.m')] ?? 0) ?>
                            </small>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#bons" class="nav-link">
                            Банкноты
                            <small class="text-success">
                                <?= Yii::$app->formatter->asInteger($sumsByDayBons[date('d.m')] ?? 0) ?>
                            </small>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="base" class="tab-pane active clearfix">
                        <?= Highcharts::widget([
                            'id'      => 'chartYear',
                            'options' => [
                                'title'  => ['text' => 'Суммы по дням'],
                                'colors' => ['green', 'red'],

                                'scrollbar'   => ['enabled' => true],//, 'showFull' => false
                                'xAxis'       => [
                                    'categories' => array_keys($sumsByDay),
                                    /* 'min'        => count(array_keys($sumsByDay)) - 75,
                                     'max'        => count(array_keys($sumsByDay)),*/
                                ],
                                'yAxis'       => [
                                    'title'     => ['text' => 'Сумма начислений'],
                                    'plotLines' => [
                                        [
                                            'value'     => 3000,
                                            'color'     => '#ffc6d0',
                                            'width'     => 1,
                                            'dashStyle' => 'longdashdot',
                                            'label'     => ['text' => 3000]
                                        ],
                                        [
                                            'value'  => 6000,
                                            'color'  => '#2d48aa',
                                            'width'  => 1,
                                            'zIndex' => 4,
                                            //'dashStyle' => 'longdashdot',
                                            'label'  => ['text' => 6000]
                                        ],
                                        [
                                            'value'     => 10000,
                                            'color'     => '#45cc63',
                                            'width'     => 1,
                                            'zIndex'    => 1,
                                            'dashStyle' => 'longdashdot',
                                            'label'     => ['text' => 10000]
                                        ],
                                        [
                                            'value'     => 25000,
                                            'color'     => '#27aa42',
                                            'width'     => 1,
                                            'zIndex'    => 1,
                                            'dashStyle' => 'longdashdot',
                                            'label'     => ['text' => 25000]
                                        ],
                                    ]
                                ],
                                'plotOptions' => [
                                    'line' => [
                                        'dataLabels'          => [
                                            'enabled' => true
                                        ],
                                        'enableMouseTracking' => true
                                    ]
                                ],
                                'series'      => [
                                    [
                                        'name' => 'Сумма заказов за день',
                                        'data' => array_values($sumsByDay),
                                        'point' => [
                                            'events' => [
                                                'click' => new \yii\web\JsExpression('
                                                      function() {
                                                            console.log(this);
                                                            window.open(\'/commission/order/view.html?date=\'+this.category, \'_blank\');
                                                    }
                                                ')
                                            ]
                                        ]
                                    ],
                                    [
                                        'name' => 'Средняя сумма заказа за день',
                                        'data' => array_values($avgByDay),
                                        'point' => [
                                            'events' => [
                                                'click' => new \yii\web\JsExpression('
                                                      function() {
                                                            console.log(this);
                                                            window.open(\'/commission/order/view.html?date=\'+this.category, \'_blank\');
                                                    }
                                                ')
                                            ]
                                        ]
                                    ]
                                ],
                                'chart'       => [
                                    'type'     => 'line',
                                    'zoomType' => 'xy'
                                ],
                            ],

                            'scripts' => [
                                'highcharts-more',
                                // enables supplementary chart types (gauge, arearange, columnrange, etc.)
                                'modules/exporting',
                                // adds Exporting button/menu to chart
                            ],
                        ]); ?>
                    </div>
                    <div id="bons" class="tab-pane fade clearfix">
                        <?= Highcharts::widget([
                            'id'      => 'chartYear2',
                            'options' => [
                                'title'  => ['text' => 'Суммы по дням'],
                                'colors' => ['green', 'red'],

                                'scrollbar'   => ['enabled' => true],//, 'showFull' => false
                                'xAxis'       => [
                                    'categories' => array_keys($sumsByDayBons),
                                ],
                                'yAxis'       => [
                                    'title' => ['text' => 'Сумма начислений'],
                                ],
                                'plotOptions' => [
                                    'line' => [
                                        'dataLabels'          => [
                                            'enabled' => true
                                        ],
                                        'enableMouseTracking' => true
                                    ]
                                ],
                                'series'      => [
                                    [
                                        'name'  => 'Сумма заказов за день',
                                        'data'  => array_values($sumsByDayBons),
                                        'point' => [
                                            'events' => [
                                                'click' => new \yii\web\JsExpression('
                                                      function() {
                                                            console.log(this);
                                                            window.open(\'/commission/order/view.html?date=\'+this.category, \'_blank\');
                                                    }
                                                ')
                                            ]
                                        ]
                                    ],
                                ],
                                'chart'       => [
                                    'type'     => 'line',
                                    'zoomType' => 'xy'
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>


                <div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <?= Html::dropDownList(
                                'type-products',
                                null,
                                \yii\helpers\ArrayHelper::merge(['' => ''], CommissionCatalog::$types),
                                ['class' => 'form-control select-type-item']
                            ) ?>
                        </div>
                        <div class="col-md-3">
                            <?= DatePicker::widget([
                                'name'          => 'charts-day-start',
                                'value'         => $chartStart,
                                'options'       => [
                                    'placeholder' => 'Выберите дату..',
                                    'class'       => 'chart-day-start form-control'
                                ],
                                'language'      => 'ru-RU',
                                'pluginOptions' => [
                                    'format'         => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= DatePicker::widget([
                                'name'          => 'charts-day-end',
                                'value'         => $chartEnd,
                                'options'       => [
                                    'placeholder' => 'Выберите дату..',
                                    'class'       => 'chart-day-end form-control'
                                ],
                                'language'      => 'ru-RU',
                                'pluginOptions' => [
                                    'format'         => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>

                <?php Pjax::end(); ?>
            </div>
            <div class="col-md-3">
                <div class="row" style="height: 500px; overflow-y: auto">
                    <div class="col-md-4">
                        <div class="">
                            <b>По месяцам</b>
                            <hr/>
                            <b>
                                <small>Средний заказ</small>
                            </b>
                            <?php foreach ($avgSumsByMonth as $date => $sum) : ?>
                                <div class="text-muted">
                                    <b class="text-primary"><?= $date ?></b>
                                    : <?= Yii::$app->formatter->asInteger((int)$sum) ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <hr/>
                        <div class="">
                            <b>
                                <small>Сумма заказов</small>
                            </b>
                            <?php foreach ($sumsByMonth as $date => $sum) : ?>
                                <div class="text-muted">
                                    <b class="text-primary"><?= $date ?></b>
                                    : <?= Yii::$app->formatter->asInteger((int)$sum) ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <hr/>
                        <small>
                            <b>Дней:</b> <?= count($sumsByDay) ?><br/>
                            <b>Всего:</b>
                            <?= Yii::$app->formatter->asInteger((int)array_sum($sumsByWeek)); ?> <br/>
                            <b>Приб.:</b>
                            <?= Yii::$app->formatter->asInteger(((int)array_sum($sumsByWeek) / 2)); ?>
                        </small>
                    </div>

                    <div class="col-md-4">
                        <div class="">
                            <b>По неделям</b>
                            <hr/>
                            <b>
                                <small>Средний заказ</small>
                            </b>
                            <?php foreach ($avgSumsByWeek as $date => $sum) : ?>
                                <div class="text-muted">
                                    <b class="text-primary"><?= CommissionOrder::getDateByMonthNumber($date) ?></b>
                                    : <?= Yii::$app->formatter->asInteger((int)$sum) ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="">
                            <b>По неделям</b>
                            <hr/>
                            <b>
                                <small>Сумма заказов</small>
                            </b>
                            <?php foreach ($sumsByWeek as $date => $sum) : ?>
                                <div class="text-muted">
                                    <b class="text-primary"><?= CommissionOrder::getDateByMonthNumber($date) ?></b>
                                    : <?= Yii::$app->formatter->asInteger((int)$sum) ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>


                </div>
            </div>

        </div>


        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin(); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'layout'       => "{pager}\n{summary}\n{items}\n{summary}\n{pager}",
                'columns'      => [
                    [
                        'attribute'  => 'date_at',
                        'class'      => '\kartik\grid\DataColumn',
                        'value'      => function ($model, $key, $index, $column) {
                            return date('Y-m-d', strtotime($model->date_at));
                        },
                        'hAlign'     => GridView::ALIGN_LEFT,
                        'vAlign'     => GridView::ALIGN_MIDDLE,
                        'width'      => '100px',
                        'format'     => 'raw',
                        'group'      => true,
                        'subGroupOf' => 1
                    ],
                    [
                        'attribute' => 'date_at',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return date('H:i', strtotime($model->date_at));
                        },
                        'hAlign'    => GridView::ALIGN_CENTER,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '100px',
                        'format'    => 'raw',
                    ],
                    [
                        'class'         => 'kartik\grid\ExpandRowColumn',
                        'expandOneOnly' => true,
                        'value'         => function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail'        => function ($model, $key, $index, $column) {
                            return Yii::$app->controller->renderpartial('_detail', ['model' => $model]);
                        }
                    ],
                    //['class' => 'kartik\grid\SerialColumn'],
                    'number',
                    [
                        'attribute' => 'count',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return $model->count;
                        },
                        'hAlign'    => GridView::ALIGN_CENTER,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'sum_payment',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Yii::$app->formatter->asCurrency($model->sum_payment);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw',
                    ],

                    [
                        'attribute' => 'status',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return $model->status;
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '200px',
                        'format'    => 'raw',
                        'filter'    => $statuses,
                    ],
                    [
                        'attribute' => 'is_payed',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::tag('i', null,
                                ['class' => ('fa fa-' . ($model->is_payed ? 'check-circle text-success' : 'circle text-muted'))]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw',
                        'filter'    => [1 => 'Да', 0 => 'Нет'],
                    ],
                    [
                        'attribute' => 'is_monetized',
                        'class'     => '\kartik\grid\DataColumn',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::tag('i', null,
                                ['class' => ('fa fa-' . ($model->is_monetized ? 'check-circle text-success' : 'circle text-muted'))]);
                        },
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '150px',
                        'format'    => 'raw',
                        'filter'    => [1 => 'Да', 0 => 'Нет'],
                    ],

                    //['class' => 'kartik\grid\ActionColumn'],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

        </div>

    </div>

<?php

$js = <<<JS
    setInterval(function () {
        location.reload();
    }, 300000);

    $(document).on('change', '.select-type-item, .chart-day-start, .chart-day-end', function () {
        var start = $('.chart-day-start').val();
        var end   = $('.chart-day-end').val();
        var type  = $('.select-type-item').val();
    
        $.pjax({
            container:"#pjax-chart",
            method:'POST',
            timeout: 0,
            "push": true,
            data: {type: type, start: start, end: end}
        });
    });
JS;

$this->registerJs($js);