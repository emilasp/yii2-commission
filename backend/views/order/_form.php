<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commission-order-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date_at')->textInput() ?>

        <?= $form->field($model, 'count')->textInput() ?>

        <?= $form->field($model, 'sum')->textInput() ?>

        <?= $form->field($model, 'is_payed')->textInput() ?>

        <?= $form->field($model, 'is_monetized')->textInput() ?>

        <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
                Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
                . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
