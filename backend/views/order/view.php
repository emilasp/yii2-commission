<?php

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\media\models\File;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrder */

$this->title                   = 'Куплено за день: ' . $date;
$this->params['breadcrumbs'][] = ['label' => 'Commission Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-order-view box box-primary">
    <div class="box-header text-right">

    </div>
    <div class="box-body table-responsive no-padding">

        <div class="row">
            <div class="col-md-4">

                <div class="panel panel-default">
                    <div class="panel-heading">Заказы</div>
                    <div class="panel-body">
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <th>Заказ</th>
                                <th>Время</th>
                                <th>Количество</th>
                                <th>Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($orders as $order) : ?>
                                <tr>
                                    <td><?= $order->id ?></td>
                                    <td><?= $order->count ?></td>
                                    <td><?= date('H:i:s', strtotime($order->date_at)) ?></td>
                                    <td><?= Yii::$app->formatter->asCurrency($order->sum_payment) ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-md-8">

                <div class="panel panel-default">
                    <div class="panel-heading">Товары</div>
                    <div class="panel-body">

                        <ul class="nav nav-tabs">
                            <li><a data-toggle="tab" href="#base" class="active nav-link">Все</a></li>
                            <li><a data-toggle="tab" href="#bons" class="nav-link">Банкноты и монеты</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="base" class="tab-pane in active">
                                <table class="table table-hover table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Наименование</th>
                                        <th>Количество</th>
                                        <th>Стоимость</th>
                                        <th>Сумма</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($items as $item) : ?>
                                        <tr>
                                            <td>
                                                <img width="50px"  class="img-thumbnail" src="<?= $item->catalogItem->file->getUrl(File::SIZE_ICO) ?>"/>
                                                <?= $item->name ?>
                                            </td>
                                            <td><?= $item->count ?></td>
                                            <td><?= Yii::$app->formatter->asCurrency($item->cost - $item->commission) ?></td>
                                            <td><?= Yii::$app->formatter->asCurrency(($item->cost - $item->commission) * $item->count) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>

                            <div id="bons" class="tab-pane fade">
                                <table class="table table-hover table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Наименование</th>
                                        <th>Количество</th>
                                        <th>Стоимость</th>
                                        <th>Сумма</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($items as $item) : ?>
                                        <?php if (in_array($item->catalogItem->type,
                                            CommissionCatalog::$typeGroups[CommissionCatalog::TYPE_MONEYS])) : ?>
                                            <tr>
                                                <td>
                                                    <img width="50px" class="img-thumbnail" src="<?= $item->catalogItem->file->getUrl(File::SIZE_ICO) ?>" />
                                                    <?= $item->name ?>
                                                </td>
                                                <td><?= $item->count ?></td>
                                                <td><?= Yii::$app->formatter->asCurrency($item->cost - $item->commission) ?></td>
                                                <td><?= Yii::$app->formatter->asCurrency(($item->cost - $item->commission) * $item->count) ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
