<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrder */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => 'Commission Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commission-order-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
