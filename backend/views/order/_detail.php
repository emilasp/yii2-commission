<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use emilasp\commission\common\models\CommissionOrderItem;

/** var $model CommissionOrderItem */

?>
<div class="panel panel-success bg-white">
    <div class="panel-body" data-id="<?= $model->id ?>">

        <div class="row">
            <div class="col-md-12">

                <?php

                $dataProvider = new ActiveDataProvider([
                    'query'      => $model->getCommissionOrderItems(),
                    'pagination' => [
                        'pageSize' => 10000,
                    ],
                    'sort'       => [
                        'defaultOrder' => [
                            'cost' => SORT_DESC,
                        ]
                    ],
                ]);
                ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout'       => "{items}\n{summary}\n{pager}",
                    'columns'      => [

                        [
                            'attribute' => 'catalogItem.file',
                            'class'     => \emilasp\media\components\gridview\ImageColumn::className(),
                            'options'   => ['width' => '50px']
                        ],

                        'im_id',
                        'name',
                        'count',
                        [
                            'attribute'  => 'sum',
                            'class'      => '\kartik\grid\DataColumn',
                            'value'      => function ($model, $key, $index, $column) {
                                return Yii::$app->formatter->asCurrency($model->sum);
                            },
                            'hAlign'     => GridView::ALIGN_LEFT,
                            'vAlign'     => GridView::ALIGN_MIDDLE,
                            'width'      => '150px',
                            'format'     => 'raw',
                        ],                        // 'cost_r',
                        [
                            'attribute' => 'IM',
                            'class'     => '\kartik\grid\DataColumn',
                            'value'     => function ($model, $key, $index, $column) {
                                $html  = '';

                                if ($catalogModel = $model->catalogItem) {
                                    $html  .= Html::a(
                                        Html::tag('i', '', ['class' => 'fa fa-eye ']),
                                        $model->catalogItem->getMonetnikUrl(),
                                        ['target' => '_blank', 'class' => 'monetnik-link-view']
                                    );
                                    $html  .= '&nbsp;&nbsp;';
                                    $html  .= Html::a(
                                        Html::tag('i', '', ['class' => 'fa fa-edit ']),
                                        $model->catalogItem->getMonetnikUrl(true),
                                        ['target' => '_blank', 'class' => 'monetnik-link-update']
                                    );
                                }

                                return $html;
                            },
                            'hAlign'    => GridView::ALIGN_LEFT,
                            'vAlign'    => GridView::ALIGN_MIDDLE,
                            'width'     => '100px',
                            'format'    => 'raw'
                        ],
                    ],
                ]); ?>


            </div>
        </div>

    </div>
</div>
