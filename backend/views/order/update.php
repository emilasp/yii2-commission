<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\commission\common\models\CommissionOrder */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => 'Commission Order',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Commission Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="commission-order-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
