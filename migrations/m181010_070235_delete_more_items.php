<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m181010_070235_delete_more_items*/
class m181010_070235_delete_more_items extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->deleteModelItems();

        $this->afterMigrate();
    }

    /**
     * Set nominal
     */
    private function deleteModelItems()
    {
        $query = \emilasp\commission\common\models\CommissionCatalog::find();
        $query->where(['>', 'id', 14654])->andWhere(['site' => 'marka']);

        $batch = 500;
        foreach ($query->batch($batch) as $indexBatch => $batchItems) {
            foreach ($batchItems as $indexPack => $model) {
                if (!$model->im_id && mb_stripos($model->article, '-')) {
                    $modelIsset = \emilasp\commission\common\models\CommissionCatalog::find()
                        ->where("article ilike '%{$model->article}%'")
                        ->andWhere("id <> {$model->id}")
                        ->one();

                    if ($modelIsset) {
                        echo "{$model->article} -> {$modelIsset->article}" . PHP_EOL;

                        $modelIsset->article = $model->article;
                        if($modelIsset->save()) {
                            $model->delete();
                        }
                    } else {
                        echo "Not found! {$model->article} !!!!!" . PHP_EOL;
                    }
                }
            }
        }
    }


    /**
     * DOWN
     */
    public function down()
    {
        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
