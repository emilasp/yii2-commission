<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m181001_074345_add_table_mode*/
class m181001_074345_add_table_mode extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('commission_catalog_mode', [
            'id'         => $this->primaryKey(11),
            'name'       => $this->string(255)->notNull(),
            'type'       => $this->smallInteger(1)->notNull()->defaultValue(0),
            'count'      => $this->integer(11)->notNull()->defaultValue(0),
            'status'     => $this->smallInteger(1)->notNull()->defaultValue(0),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addDefaultMode();

        $this->createTable('commission_catalog_mode_item', [
            'id'         => $this->primaryKey(11),
            'mode_id'    => $this->integer(11)->notNull(),
            'catalog_id' => $this->integer(11)->notNull(),
            'count'      => $this->integer(11)->notNull(),
            'status'     => $this->smallInteger(1)->notNull()->defaultValue(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_commission_catalog_mode_item_mode_id',
            'commission_catalog_mode_item',
            'mode_id',
            'commission_catalog_mode',
            'id'
        );
        $this->addForeignKey(
            'fk_commission_catalog_mode_item_catalog_id',
            'commission_catalog_mode_item',
            'catalog_id',
            'commission_catalog',
            'id'
        );
        $this->addForeignKey(
            'fk_commission_catalog_mode_item_created_by',
            'commission_catalog_mode_item',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_commission_catalog_mode_item_updated_by',
            'commission_catalog_mode_item',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addColumn('commission_catalog', 'is_modify_count', $this->integer(11)->defaultValue(0));



        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('commission_catalog_mode_item');
        $this->dropTable('commission_catalog_mode');

        $this->dropColumn('commission_catalog', 'is_modify_count');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }

    private function addDefaultMode()
    {
        $sql = <<<SQL
        INSERT INTO commission_catalog_mode (name, type, count, status, created_at, updated_at)
         VALUES ('Закупка', 1, 0, 0, NOW(), NOW()),
                ('Обработка', 0, 0, 1, NOW(), NOW());
SQL;
        $this->db->createCommand($sql)->execute();
    }
}
