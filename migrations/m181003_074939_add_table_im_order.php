<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m181003_074939_add_table_im_order*/
class m181003_074939_add_table_im_order extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('commission_order', [
            'id'           => $this->primaryKey(11),
            'number'       => $this->string(255),
            'date_at'      => $this->dateTime(),
            'count'        => $this->integer(10)->notNull(),
            'sum'          => $this->decimal(12, 2),
            'sum_payment'  => $this->decimal(12, 2),
            'commission'   => $this->decimal(12, 2),
            'is_payed'     => $this->smallInteger(1)->notNull()->defaultValue(0),
            'is_monetized' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'status'       => $this->string(255)->notNull(),
        ], $this->tableOptions);

        $this->createTable('commission_order_item', [
            'id'         => $this->primaryKey(11),
            'order_id'   => $this->integer(11)->notNull(),
            'im_id'      => $this->integer(11),
            'article'    => $this->string(255)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'date_at'    => $this->dateTime(),
            'count'      => $this->integer(10)->notNull(),
            'cost'       => $this->decimal(12, 2),
            'sum'        => $this->decimal(12, 2),
            'commission' => $this->decimal(12, 2),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_commission_order_item_order_id',
            'commission_order_item',
            'order_id',
            'commission_order',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('commission_order_item');
        $this->dropTable('commission_order');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
