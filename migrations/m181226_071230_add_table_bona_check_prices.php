<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m181226_071230_add_table_bona_check_prices*/
class m181226_071230_add_table_bona_check_prices extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('commission_check_price', [
            'id'         => $this->primaryKey(11),
            'catalog_id' => $this->integer(11)->notNull(),

            'price_our' => $this->decimal(14, 2)->defaultValue(0),
            'price_min' => $this->decimal(14, 2)->defaultValue(0),
            'price_max' => $this->decimal(14, 2)->defaultValue(0),

            'im_id'           => $this->string(255),
            'price_min_im_id' => $this->string(255),

            'year'     => $this->smallInteger(4)->notNull(),
            'category' => $this->string(255)->notNull(),
            'nominal'  => $this->string(255)->notNull(),

            'status' => $this->smallInteger(1)->notNull(),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_commission_check_price_catalog_id',
            'commission_check_price',
            'catalog_id',
            'commission_catalog',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('commission_check_price');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
