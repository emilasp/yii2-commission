<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
* Class m181008_075200_update_nominal*/
class m181008_075200_update_nominal extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
    * UP
    */
    public function up()
    {
        /**
         * Устанавливаем значения для поиска по gin полю
         */
       $this->setDataSearchForModels();

        $this->afterMigrate();
    }

    /**
     * Set nominal
     */
    private function setDataSearchForModels()
    {
        $query = \emilasp\commission\common\models\CommissionCatalog::find();

        $batch = 500;
        foreach ($query->batch($batch) as $indexBatch => $batchItems) {
            foreach ($batchItems as $indexPack => $model) {
                $model->nominal = $model->parseNominal($model->nominal);

                $model->article = $this->setArticleSeries($model->article);


                $model->save();


                $index = $indexBatch * $batch + $indexPack;
                echo "Set model {$index}/{$query->count()}: {$model->nominal}".PHP_EOL;
            }
        }
    }

    /**
     * Обновляем серии
     *
     * @param string $article
     * @return string
     */
    private function setArticleSeries(string $article): string
    {
        if (mb_stripos($article, '-')) {
            $dataArticle = explode('-', $article);

            if (count($dataArticle) === 2) {
                $numbers = range((int)$dataArticle[0], (int)$dataArticle[1]);
                $numbers[] = $article;
                $article = implode(',', $numbers);
            }
        }

        return $article;
    }

    /**
    * DOWN
    */
    public function down()
    {
        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
