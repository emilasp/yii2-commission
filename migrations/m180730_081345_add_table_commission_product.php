<?php

use emilasp\commission\common\models\CommissionProduct;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m180730_081345_add_table_commission_product*/
class m180730_081345_add_table_commission_product extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('commission_product', [
            'id'          => $this->primaryKey(11),
            'guid'        => $this->integer(11),
            'category_id' => $this->integer(11)->notNull(),
            'type'        => $this->smallInteger(1)->defaultValue(CommissionProduct::TYPE_SINGLE),
            'article'     => $this->string(20),
            'name'        => $this->string(255)->notNull(),
            'count'       => $this->integer(4),
            'cost'        => $this->decimal(14, 2)->defaultValue(0),
            'cost_r'      => $this->decimal(14, 2)->defaultValue(0),
            'price'       => $this->decimal(14, 2)->defaultValue(0),
            'price_sale'  => $this->decimal(14, 2)->defaultValue(0),
            'description' => $this->text(),
            'comment'     => $this->text(),
            'keywords'    => $this->text(),

            'material' => $this->smallInteger(1)->defaultValue(CommissionProduct::MATERIAL_PAPER),

            'url'      => $this->string(255),
            'url_base' => $this->string(255),

            'data'     => 'jsonb NULL DEFAULT \'[]\'',

            'status' => $this->smallInteger(1)->notNull(),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_commission_product_created_by',
            'commission_product',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_commission_product_updated_by',
            'commission_product',
            'updated_by',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('commission_product');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
