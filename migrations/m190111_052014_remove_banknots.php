<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;
use emilasp\commission\common\models\CommissionCatalog;


/**
 * Class m190111_052014_remove_banknots */
class m190111_052014_remove_banknots extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $query = CommissionCatalog::find()->where(['type' => CommissionCatalog::$typeGroups[CommissionCatalog::TYPE_MONEYS]])->andWhere(['IS', 'im_id', null]);

        $countAll = $query->count();

        $batch = 500;
        foreach ($query->batch($batch) as $indexBatch => $batchItems) {
            foreach ($batchItems as $indexPack => $model) {
                $count = $indexBatch * $batch + $indexPack;
                echo "{$count}/{$countAll} Delete model {$model->id}" . PHP_EOL;
                $model->delete();
            }
        }

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
