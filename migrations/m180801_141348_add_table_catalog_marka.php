<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m180801_141348_add_table_catalog_marka*/
class m180801_141348_add_table_catalog_marka extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('commission_catalog', [
            'id'       => $this->primaryKey(11),
            'image_id' => $this->integer(11),
            'site'     => $this->string(255),
            'article'  => $this->string(255),

            'name'        => $this->string(255)->notNull(),
            'description' => $this->text(),

            'cost'   => $this->decimal(14, 2)->defaultValue(0),
            'cost_r' => $this->decimal(14, 2)->defaultValue(0),

            'url' => $this->string(255),

            'data'        => 'jsonb NULL DEFAULT \'[]\'',
            'data_params' => 'jsonb NULL DEFAULT \'[]\'',

            'year' => $this->smallInteger(4),

            'im_id' => $this->integer(11),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_commission_catalog_created_by',
            'commission_catalog',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_commission_catalog_updated_by',
            'commission_catalog',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_commission_catalog_image_id',
            'commission_catalog',
            'image_id',
            'media_file',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('commission_catalog');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
