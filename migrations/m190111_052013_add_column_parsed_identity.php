<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m190111_052013_add_column_parsed_identity*/
class m190111_052013_add_column_parsed_identity extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('commission_catalog_parsed', [
            'id'         => $this->primaryKey(11),
            'catalog_id' => $this->integer(11),
            'site'       => $this->string(50),
            'url'        => $this->string(255),
            'identity'   => $this->string(255),
            'created_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_commission_catalog_parsed_catalog_id',
            'commission_catalog_parsed',
            'catalog_id',
            'commission_catalog',
            'id'
        );

        $this->addColumn('commission_catalog', 'country', $this->string(100));

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('commission_catalog_parsed');

        $this->dropColumn('commission_catalog', 'country');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
