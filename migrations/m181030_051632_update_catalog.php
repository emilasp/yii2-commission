<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
* Class m181030_051632_update_catalog*/
class m181030_051632_update_catalog extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
    * UP
    */
    public function up()
    {
        $this->addColumn('commission_catalog', 'active', $this->smallInteger(1)->defaultValue(0));
        $this->addColumn('commission_catalog', 'cost_sol', $this->decimal(14, 2)->defaultValue(0));

        $this->afterMigrate();
    }

    /**
    * DOWN
    */
    public function down()
    {
        $this->dropColumn('commission_catalog', 'active');
        $this->dropColumn('commission_catalog', 'cost_sol');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
