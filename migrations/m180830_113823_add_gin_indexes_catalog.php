<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
* Class m180830_113823_add_gin_indexes_catalog*/
class m180830_113823_add_gin_indexes_catalog extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
    * UP
    */
    public function up()
    {
        $sql = <<<SQL
CREATE EXTENSION pg_trgm;
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE INDEX commission_catalog_name_trgm_idx ON commission_catalog
USING gin (name gin_trgm_ops);
SQL;
        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
CREATE INDEX commission_catalog_desc_trgm_idx ON commission_catalog
USING gin (description gin_trgm_ops);
SQL;
        $this->db->createCommand($sql)->execute();

        $this->afterMigrate();
    }

    /**
    * DOWN
    */
    public function down()
    {
        $sql = <<<SQL
drop index commission_catalog_name_trgm_idx;
SQL;
        $this->db->createCommand($sql)->execute();
        $sql = <<<SQL
drop index commission_catalog_desc_trgm_idx;
SQL;
        $this->db->createCommand($sql)->execute();
        $sql = <<<SQL
DROP EXTENSION pg_trgm;
SQL;
        $this->db->createCommand($sql)->execute();

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
