<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
* Class m181010_064358_add_alt_article_catalog*/
class m181010_064358_add_alt_article_catalog extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
    * UP
    */
    public function up()
    {
        $this->addColumn('commission_catalog', 'article_full', $this->string(150)->null());

        $sql = <<<SQL
CREATE INDEX commission_catalog_article_full_trgm_idx ON commission_catalog
USING gin (article_full gin_trgm_ops);
SQL;
        $this->db->createCommand($sql)->execute();

        $this->fillArticles();

        $this->afterMigrate();
    }

    /**
     * Set nominal
     */
    private function fillArticles()
    {
        $query = \emilasp\commission\common\models\CommissionCatalog::find();

        $batch = 500;
        foreach ($query->batch($batch) as $indexBatch => $batchItems) {
            foreach ($batchItems as $indexPack => $model) {
                $model->save();

                $index = $indexBatch * $batch + $indexPack;
                echo "Set model {$index}/{$query->count()}: {$model->article_full}".PHP_EOL;
            }
        }
    }

    /**
    * DOWN
    */
    public function down()
    {
        $this->dropColumn('commission_catalog', 'article_full');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
