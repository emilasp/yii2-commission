<?php
namespace emilasp\commission\console;

use emilasp\commission\common\CommissionBaseModule;

/**
 * Class CommissionModule
 * @package emilasp\commission\console;
 */
class CommissionModule extends CommissionBaseModule
{
    public $controllerNamespace = 'emilasp\commission\console\commands';
}
