<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\core\commands\AbstractConsoleController;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;


/**
 *
 * @package emilasp\commission\commands
 */
class ParseMarkaController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(): void
    {
        $this->display("-----Parse marka ooo-----", self::FONT_COLOR_YELLOW);

        $links = $this->parseMarkaLinks();
        $count = count($links);
        $this->display("Parse stamps: {$count}", self::FONT_COLOR_YELLOW);

        $count = count($links);
        foreach ($links as $index => $link) {
            $data       = $this->parseProductFromMarka($link);
            $attributes = $data[count($data) - 1];

            if (!$model = CommissionCatalog::findOne([
                'article' => $attributes['article'],
                'site'    => 'marka',
            ])) {
                $model = new CommissionCatalog([
                    'site' => 'marka',
                ]);
            }

            $attributes['name'] = $model->name ?: $attributes['name'];
            $attributes['nominal'] = $model->nominal ?: ($attributes['nominal'] ?? '');
            $attributes['article'] = $model->article ?: ($attributes['article'] ?? '');

            $model->setAttributes($attributes);
            $model->data_params = json_encode(ArrayHelper::remove($data, 'params'));
            $model->data        = json_encode($data);
            $model->year        = $attributes['date_at'] ? date('Y', strtotime($attributes['date_at'])) : null;

            $imageUrl = $data[count($data) - 1]['image'] ?? null;
            if ($imageUrl && !$model->image_id) {
                $model->imageUpload = 'http://rusmarka.ru' . $imageUrl;
                $model->saveSingleImage();
            }

            $model->save();

            if ($model->hasErrors()) {
                $errors = json_encode($model->getErrors()) . json_encode($model->getAttributes());
                $this->display("Model has Errors: {$errors}", self::FONT_COLOR_RED);
            }

            $this->display("Parse model {$index}/{$count}: {$model->id}", self::FONT_COLOR_GREEN);
        }
    }

    /**
     * Parse links
     *
     * @return array
     */
    private function parseMarkaLinks(): array
    {
        $allUrls = [];

        $count = 0;
        $stop  = false;
        while (!$stop) {
            $this->display("Parse urls: {$count}", self::FONT_COLOR_BLUE);

            $link     = "http://rusmarka.ru/catalog/marka/year/0/p/{$count}.aspx";
            $response = $this->sendRequest($link);

            $crawler = new Crawler($response->content);
            $links   = $crawler->filter('.catalog a');

            if (!$links->count()) {
                $stop = true;
            } else {
                $urls = $links->each(function ($link) {
                    return $link->attr('href');
                });

                $allUrls = ArrayHelper::merge($allUrls, $urls);
            }
            $count++;
            //break;
        }

        return array_unique($allUrls);
    }


    /**
     * Parse product
     *
     * @return array
     */
    private function parseProductFromMarka(string $link): array
    {
        $link = 'http://rusmarka.ru' . $link;
        //$link = 'http://rusmarka.ru/catalog/marka/position/12986.aspx';

        echo $link . ' >> ' . PHP_EOL;
        $this->display($link);

        $attributeFilters = [
            'date_at'         => [
                'filter' => '.wrap > .info .date',
                'type'   => 'text',
                'value'  => function ($value) {
                    return $value ? date('Y-m-d H:i:s', strtotime($value)) : '';
                }
            ],
            'artist_designer' => [
                'filter' => '.wrap > .info',
                'type'   => 'html',
                'value'  => function ($value) {
                    if (strpos($value, '<p>') !== false) {
                        $data = explode('<p>', $value);
                        return strip_tags(nl2br($data[1]));
                    }
                    return '';
                }
            ],
            'name'            => [
                'filter' => 'h1',
                'type'   => 'text',
                'value'  => function ($value) {
                    if (strpos($value, '.') !== false) {
                        $dataValue = explode('.', $value);
                        unset($dataValue[0]);
                        $value = implode(' ', $dataValue);
                    }
                    return $value;
                }
            ],
            'description'     => [
                'filter' => '#ctl00_MainColumn_ctl00_gooddetail_repParts_ctl00_part_description',
                'type'   => 'html',
            ],

            'image' => [
                'filter'    => '.paint_marka a',
                'attribute' => 'href',
            ],

            'cost'    => [
                'filter' => '.cart1',
                'type'   => 'html',
                'value'  => function ($value) {
                    if (strpos($value, 'нет в наличии') !== false) {
                        return null;
                    }
                    $cost = 0;
                    if (preg_match('/Цена чист.:(.*)руб/', $value, $matches)) {
                        $cost = $matches[1] ?? 0;
                    }
                    if ($cost) {
                        return floatval($cost);
                    }
                    return null;
                }
            ],
            'cost_r'  => [
                'filter' => '.cart1',
                'type'   => 'html',
                'value'  => function ($value) {
                    if (strpos($value, 'нет в наличии') !== false) {
                        return null;
                    }
                    $cost = 0;
                    if (preg_match('/Цена гашен.:(.*)руб/', $value, $matches)) {
                        $cost = $matches[1] ?? 0;
                    }
                    if ($cost) {
                        return floatval($cost);
                    }
                    return null;
                }
            ],
            'article' => [
                'filter' => 'h1',
                'type'   => 'text',
                'value'  => function ($value) {
                    $article = '';
                    if (strpos($value, '.') !== false) {
                        $dataValue = explode('.', $value);
                        if (strpos($dataValue[0], '№') !== false) {
                            $article = trim($dataValue[0], '№ ');
                        }
                    }
                    return $article;
                }
            ],
            'type'    => [
                'filter' => '.wrap2 .info .red',
                'type'   => 'text',
            ],
            'themes'  => [
                'filterXpath' => '//table[@class="info2"]/following-sibling::p/a',
                'type'        => 'text',
            ],
        ];

        $response = $this->sendRequest($link);

        // Get Block Variants
        $crawler = new Crawler($response->content);

        $title  = $crawler->filter('.text h1');
        $wrap   = $crawler->filter('.text .wrap');
        $wrap2s = $crawler->filter('.text .wrap2');
        $infos  = $crawler->filter('.text .fullinfo');
        $tables = $crawler->filter('.text table.info2');


        $titleHtml = $title->getNode(0)->ownerDocument->saveHTML($title->getNode(0));
        $wrapHtml  = $wrap->getNode(0)->ownerDocument->saveHTML($wrap->getNode(0));

        $blocks = [];
        foreach ($wrap2s as $index => $wrap) {
            $html       = '<div>';
            $wrap2sHtml = $wrap2s->getNode($index)->ownerDocument->saveHTML($wrap2s->getNode($index));
            $infosHtml  = $infos->getNode($index)->ownerDocument->saveHTML($infos->getNode($index));
            $tablesHtml = $tables->getNode($index)->ownerDocument->saveHTML($tables->getNode($index));
            $html       .= $titleHtml . $wrapHtml . $wrap2sHtml . $infosHtml . $tablesHtml;
            $html       .= '<div>';

            $blocks[] = $html;
        }


        //Get All Blocks DATA

        $allAttributes = [];
        foreach ($blocks as $blockHtml) {
            $attributes = ['url' => $link];

            $crawler = new Crawler($blockHtml);

            foreach ($attributeFilters as $attribute => $filter) {
                if (!empty($filter['filter'])) {
                    $node = $crawler->filter($filter['filter']);
                } elseif (!empty($filter['filterXpath'])) {
                    $node = $crawler->filterXPath($filter['filterXpath']);
                }

                $value = '';

                if ($node->count()) {
                    if (!empty($filter['attribute'])) {
                        $value = $node->attr($filter['attribute']);
                    } elseif ($filter['type'] === 'text') {
                        $value = $node->text();
                    } elseif ($filter['type'] === 'html') {
                        $value = $node->html();
                    } elseif ($filter['type'] === 'crawler') {
                        $value = $node;
                    }

                    if (!empty($filter['value']) && is_callable($filter['value'])) {
                        $value = $filter['value']($value);
                    }
                }

                $attributes[$attribute] = trim($value);
            }

            $attributes['nominal'] = $attributes['cost'];

            $params     = $this->getStampParams($crawler);
            $attributes = ArrayHelper::merge($attributes, $params);

            $paramsFull           = $this->getStampParams($crawler, true);
            $attributes['params'] = $paramsFull;

            $attributes['cost']   = isset($attributes['cost']) ? floatval($attributes['cost']) : null;
            $attributes['cost_r'] = isset($attributes['cost_r']) ? floatval($attributes['cost_r']) : null;

            $allAttributes[] = $attributes;
        }

        return $allAttributes;
    }

    /**
     * Парсим колонку
     *
     * @param Crawler $crawler
     * @return array
     */
    private static function getStampParams(Crawler $crawler, bool $full = false): array
    {
        $attributeColumns = CommissionCatalog::DATA_ATTRIBUTES;

        $ths        = $crawler->filterXPath('//table[@class=\'info2\']/tr[1]/th');
        $tds        = $crawler->filterXPath('//table[@class="info2"]/tr[2]/td');
        $map        = [];
        $attributes = [];
        foreach ($ths as $index => $td) {
            $text = trim($td->textContent);

            if ($text) {
                if (!$full) {
                    $isFind = false;
                    foreach ($attributeColumns as $attribute => $columns) {
                        if (in_array($text, $columns)) {
                            $map[$attribute] = $index;
                            $isFind          = true;

                            $attributes[$attribute] = $tds->getNode($index)->textContent ?? '';
                        }
                    }

                    if (!$isFind && !in_array($text, ['№', 'Цена чистых'])) {
                        echo 'NOT FOUND: ' . $text;
                        $tt = 10;
                        //parent::display('NOT FOUND: ' . $text);
                    }
                } else {
                    $attributes[$text] = $tds->getNode($index)->textContent ?? '';
                }
            }
        }

        return $attributes;
    }

    /**
     * @param string $action
     * @param array  $data
     * @param string $method
     * @return array
     */
    private function sendRequest(
        string $action,
        array $data = [],
        string $method = 'get',
        string $cookieName = null
    ): Response {
        sleep(0.1);

        $client  = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($action)
            ->setData($data)
            ->setHeaders([
                'Origin'                                          => 'https://www.rusmarka.ru',
                'Upgrade-Insecure-Requests'                       => 1,
                'User-Agent'                                      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
                'X-DevTools-Emulate-Network-Conditions-Client-Id' => 'B72A04C0215FD004F5A13DE1FE7A5979',
                'Accept'                                          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Language'                                 => 'ru,en;q=0.9,en-US;q=0.8',
            ]);


        $response = $request->send();
        return $response;
    }
}
