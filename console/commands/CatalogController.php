<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\commission\common\models\ImCheckPrice;
use emilasp\core\commands\AbstractConsoleController;
use Yii;


/**
 *
 * @package emilasp\commission\commands
 */
class CatalogController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(int $catalogId = null): void
    {
        $this->display("-----Update catalog search data-----", self::FONT_COLOR_YELLOW);

        $query = \emilasp\commission\common\models\CommissionCatalog::find();

        $batch = 500;
        foreach ($query->batch($batch) as $indexBatch => $batchItems) {
            foreach ($batchItems as $indexPack => $model) {
                $model->save();

                $index = $indexBatch * $batch + $indexPack;
                $this->display("Set model {$index}/{$query->count()}: {$model->article_full}", self::FONT_COLOR_GREEN);
            }
        }
    }
}
