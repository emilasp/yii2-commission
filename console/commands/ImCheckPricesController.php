<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\commission\common\models\ImCheckPrice;
use emilasp\core\commands\AbstractConsoleController;
use Yii;


/**
 *
 * @package emilasp\commission\commands
 */
class ImCheckPricesController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(int $catalogId = null): void
    {
        $this->display("-----Content publication-----", self::FONT_COLOR_YELLOW);

        //Yii::$app->monentik->login();

        $checkModels = ImCheckPrice::find()
            ->where(['status' => ImCheckPrice::STATUS_ENABLED])->andFilterWhere(['catalog_id' => $catalogId])->all();


        $count = count($checkModels);

        $this->display("Found {$count} checkers");

        foreach ($checkModels as $checkModel) {
            $products = Yii::$app->monentik->parseProductSearch($checkModel->category, $checkModel->year, $checkModel->nominal);

            $checkModel->price_min = 0;
            $checkModel->price_max = 0;

            $hasOur = false;
            foreach ($products as $product) {
                if ($product['im_id'] === $checkModel->im_id) {
                    $hasOur = true;
                    if (!in_array($checkModel->catalog->type, CommissionCatalog::$typeSets) && mb_stripos($checkModel->catalog->name, 'набор') === false) {
                        $checkModel->price_our = $product['price'];
                    }
                } elseif($product['is_our']) {
                    continue;
                } elseif (!$product['is_no_isset']) {
                    if (!$checkModel->price_min && !$checkModel->price_max) {
                        $checkModel->price_min = $product['price'];
                        $checkModel->price_max = $product['price'];
                    }
                    if ($product['price'] <= $checkModel->price_min) {
                        $checkModel->price_min = $product['price'];
                        $checkModel->price_min_im_id = $product['im_id'];
                    } elseif ($product['price'] > $checkModel->price_max) {
                        $checkModel->price_max = $product['price'];
                    }
                }
            }

            if (!$hasOur) {
                $checkModel->status = ImCheckPrice::STATUS_HIDED;
                $this->sendErrors($checkModel, "Наш товар выбыл из списка");
            }

            $checkModel->save();

            $this->display("Проверяем(OUR {$checkModel->price_our}/MIN {$checkModel->price_min}): {$checkModel->catalog->name}",
                self::FONT_COLOR_GREEN);

            if ((int)$checkModel->price_min && $checkModel->price_our > $checkModel->price_min) {
                $this->display("{$checkModel->catalog->name} price > ID:{$checkModel->price_min_im_id}",
                    self::FONT_COLOR_RED);

                $this->sendErrors($checkModel, 'Новая минимальная цена на позицию');
            }

            sleep(0.5);
        }
    }


    /**
     * Send errors to email
     */
    private function sendErrors(ImCheckPrice $model, string $error): void
    {
        $this->display('Send errors to email', self::FONT_COLOR_YELLOW);

        $message = Yii::$app->mailer->compose('commission/check-min-price', ['model' => $model])
            ->setFrom(Yii::$app->params['email']['admin'])
            ->setTo('fremil@yandex.ru')
            ->setSubject('Commission: ' . $error);

        $message->send();
    }
}
