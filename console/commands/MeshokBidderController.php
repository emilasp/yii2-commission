<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionProduct;
use emilasp\core\commands\AbstractConsoleController;
use Yii;


/**
 *
 * @package emilasp\commission\commands
 */
class MeshokBidderController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionFromIm(int $onlyNew = 1): void
    {
        $this->display("-----Content publication-----", self::FONT_COLOR_YELLOW);

        Yii::$app->monentik->login();

        $productUrls = Yii::$app->monentik->parseProductLinks();

        $count = count($productUrls);

        $this->display("Found {$count} products");

        foreach ($productUrls as $url) {
            $this->display("{$url}", self::FONT_COLOR_GREEN);
            $attributes = Yii::$app->monentik->parseProduct($url);

            if ($attributes) {
                $model = CommissionProduct::findOne(['guid' => $attributes['guid']]);

                if (!$model) {
                    $model = new CommissionProduct([]);

                    $this->display("Create new item: {$attributes['article']}", self::FONT_COLOR_GREEN);
                } else {
                    if ($onlyNew) {
                        $this->display("Continue item: {$attributes['article']}", self::FONT_COLOR_YELLOW);
                        continue;
                    }

                    $this->display("Update item: {$attributes['article']}", self::FONT_COLOR_BLUE);
                }

                $model->setAttributes($attributes);
                $model->data = json_encode($attributes);
                $model->save();

                if (!$model->save()) {
                    $this->display("Error save item", self::FONT_COLOR_RED);
                    var_dump($model->getErrors());
                }
            } else {
                $this->display("No parse page: {$url}", self::FONT_COLOR_RED);
            }

            sleep(0.5);
        }
    }
}
