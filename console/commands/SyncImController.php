<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionOrder;
use emilasp\commission\common\models\CommissionReserve;
use emilasp\core\commands\AbstractConsoleController;
use Yii;
use yii\helpers\ArrayHelper;


/**
 *
 * @package emilasp\commission\commands
 */
class SyncImController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionOrders(): void
    {
        $this->display("-----Parse orders-----", self::FONT_COLOR_YELLOW);

        $itemsReady = Yii::$app->monentik->getOrderItems('/backoffice/my/commission/s/orders/ready/');
        $itemsOther = Yii::$app->monentik->getOrderItems('/backoffice/my/commission/s/orders/other/');
        $itemsPaid  = Yii::$app->monentik->getOrderItems('/backoffice/my/commission/s/orders/paid/');

        $items = ArrayHelper::merge($itemsReady, $itemsOther);
        $items = ArrayHelper::merge($items, $itemsPaid);

        $count = count($items);
        $iter  = 1;
        foreach ($items as $number => $orderItems) {
            $this->display("Update order {$iter}/{$count} {$number}", self::FONT_COLOR_GREEN);

            CommissionOrder::updateOrderFromItems($orderItems);
            $iter++;
        }
    }

    /**
     * Content publication
     */
    public function actionReserves(): void
    {
        $this->display("-----Parse orders-----", self::FONT_COLOR_YELLOW);

        $items = Yii::$app->monentik->getReserveItems();

        CommissionReserve::deleteAll();

        $count = count($items);
        $iter  = 1;
        foreach ($items as $imId => $item) {
            $this->display("Update reserve {$iter}/{$count} {$imId}", self::FONT_COLOR_GREEN);

            CommissionReserve::updateReserve($item[0]);
            $iter++;
        }
    }

    /**
     * Content publication
     */
    public function actionSetNoActives(): void
    {
        $this->display("-----Parse no active items-----", self::FONT_COLOR_YELLOW);

        $imIds = Yii::$app->monentik->getNoActiveItems();

        CommissionCatalog::updateAll(['active' => 0]);
        CommissionCatalog::updateAll(['active' => 1], ['im_id' => $imIds]);
    }
}
