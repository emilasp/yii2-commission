<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionCatalogParsed;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\core\commands\AbstractConsoleController;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;


/**
 *
 * @package emilasp\commission\commands
 */
class ParseCollmartController extends AbstractConsoleController
{
    public const SITE_COLLMART = 'collmart';

    private const BASE_URL = 'https://collmart.ru/';

    private const CATEGORY_LINKS = [
        //CommissionCatalog::TYPE_MONEY_BON  => ['category/bony_39/'],
        CommissionCatalog::TYPE_MONEY_COIN => ['category/monety-inostrannye/', 'category/monety-rossii_x2/'],
    ];


    /**
     * Content publication
     */
    public function actionIndex(int $debug = 0, int $hardParse = 0): void
    {
        $this->display("-----Parse Collmart ooo-----", self::FONT_COLOR_YELLOW);

        $links = $this->parse($debug, $hardParse);
    }

    /**
     * Проверяем на прошлые парсы
     *
     * @param string $link
     * @return bool
     */
    private function checkIssetParse(string $link): bool
    {
        if (!CommissionCatalogParsed::findOne(['site' => self::SITE_COLLMART, 'identity' => $link])) {
            $parsed = new CommissionCatalogParsed(['site' => self::SITE_COLLMART, 'identity' => $link, 'url' => $link]);
            $parsed->save();
            return false;
        }

        return true;
    }

    /**
     * Parse links
     *
     * @return array
     */
    private function parse(int $debug = 0, int $hardParse = 0): array
    {
        $allUrls = [];
        foreach (self::CATEGORY_LINKS as $type => $categories) {
            foreach ($categories as $category) {
                $this->display("Parse urls for category: {$category}", self::FONT_COLOR_YELLOW);

                $urls = $this->parseLinksFromCategory(self::BASE_URL . $category, $debug);

                $this->parseProducts($type, $urls, $hardParse);

                $allUrls = ArrayHelper::merge($allUrls, $urls);
                if ($debug && count($allUrls) > 100) {
                    break;
                }
            }
        }
        return array_unique($allUrls);
    }

    /**
     * Парсим продукты
     *
     * @param int   $type
     * @param array $links
     * @param int $hardParse
     * @return int
     */
    private function parseProducts(int $type, array $links, int $hardParse): int
    {
        $newProducts = 0;
        $count       = count($links);
        $this->display("Parse items: {$count}", self::FONT_COLOR_YELLOW);

        $count = count($links);
        foreach ($links as $index => $link) {
            if ($hardParse || !$this->checkIssetParse($link)) {
                $data = $this->parseProduct($link);

                if ($data) {
                    $article = CommissionCatalogParsed::getIdentityFromData(
                        '',
                        $data['country'],
                        $type,
                        (int)$data['year'],
                        $data['nominal']
                    );

                    if (!$model = CommissionCatalog::findOne(['article' => $article])) {
                        $model = new CommissionCatalog([
                            'site'        => self::SITE_COLLMART,
                            'type'        => $type,
                            'article'     => $article,
                            'name'        => $data['name'],
                            'description' => $data['description'],
                            'year'        => (int)$data['year'],
                            'nominal'     => $data['nominal'],
                            'country'     => $data['country'],
                            'data_params' => json_encode($data['params']),
                            'data'        => json_encode([$data]),
                        ]);

                        $model->save();

                        if ($model->hasErrors()) {
                            $errors = json_encode($model->getErrors()) . ' |-| ' . json_encode($model->getAttributes(),
                                    JSON_UNESCAPED_UNICODE);
                            $this->display("Model has Errors: {$errors}", self::FONT_COLOR_RED);
                        }

                        $imageUrl = $data['image'] ?? null;
                        if ($imageUrl) {
                            $model->imageUpload = self::BASE_URL . $imageUrl;
                            $model->saveSingleImage();
                        }

                        $this->display("Parse model {$index}/{$count}: {$model->id}", self::FONT_COLOR_GREEN);

                        $newProducts++;
                    }
                } else {
                    $this->display("Parse model  ERROR: {$link}", self::FONT_COLOR_RED);
                }
            }
        }

        return $newProducts;
    }

    /**
     * Parse links from category
     *
     * @param string $url
     */
    private function parseLinksFromCategory(string $url, int $debug = 0): array
    {
        $allUrls = [];

        $count = 1;
        $stop  = false;
        while (!$stop) {
            $countAll = count($allUrls);
            $this->display("Parse urls({$countAll}): {$count}", self::FONT_COLOR_BLUE);

            $link     = $url . '?page=' . $count;
            $response = $this->sendRequest($link);

            $crawler = new Crawler($response->content);
            $links   = $crawler->filter('.product-list .product h5 a');

            if (!$links->count()) {
                $stop = true;
            } else {
                $urls = $links->each(function ($link) {
                    return $link->attr('href');
                });

                $allUrls = ArrayHelper::merge($allUrls, $urls);
            }
            $count++;

            if ($debug && $count > 2) {
                $stop = true;
            }
        }

        return array_unique($allUrls);
    }


    /**
     * Parse product
     *
     * @return array
     */
    private function parseProduct(string $link): array
    {
        $link = 'https://collmart.ru' . $link;

        echo $link . ' >> ' . PHP_EOL;
        $this->display($link);

        $response = $this->sendRequest($link);

        $crawler = new Crawler($response->content);

        $idNode          = $crawler->filter('.product_page input[name="product_id"]');
        $titleNode       = $crawler->filter('.product_page h1.product-name');
        $descriptionNode = $crawler->filter('.product_page .description');
        $imageNode       = $crawler->filter('.product_page a.img_middle_in');
        $costNode        = $crawler->filter('.product_page .prices .price');

        $id          = $idNode->count() ? $idNode->attr('value') : '';
        $title       = $titleNode->count() ? $titleNode->text() : '';
        $description = $descriptionNode->count() ? $descriptionNode->text() : '';
        $image       = $imageNode->count() ? $imageNode->attr('href') : '';
        $cost        = $costNode->count() ? $costNode->attr('data-price') : '';

        $params = $this->getParamsFromTableElement($crawler);

        $paramsBase = $this->getBaseParamsFromTitle($title);

        if (!$paramsBase) {
            return [];
        }

        return [
            'id'          => $id,
            'type'        => '',
            'title'       => $title,
            'name'        => $paramsBase['name'],
            'description' => $description,
            'image'       => $image,
            'cost'        => $cost,
            'nominal'     => $paramsBase['nominal'],
            'year'        => $paramsBase['year'],
            'country'     => $paramsBase['country'],
            'params'      => $params,
        ];
    }

    /**
     * Получаем базовые папраметры из названия
     *
     * @param string $title
     * @return array
     */
    private function getBaseParamsFromTitle(string $title): array
    {
        $dataFirst = explode('.', $title);

        $prevEl = '';
        foreach ($dataFirst as $index => $str) {
            unset($dataFirst[$index]);

            $goodStr = '';
            if (mb_strpos($str, ' год') || mb_strpos($str, ' гг')) {
                $goodStr = $str;
                break;
            } else {
                $prevEl = $str;
            }
        }

        $dataFirst = array_map('trim', $dataFirst);
        $dataFirst = array_diff($dataFirst, ['']);

        $data = ['nominal' => '', 'year' => '', 'country' => '', 'name' => '',];
        if ($goodStr) {

            $pattern = '/([\d]{1,20}[ ]+[A-Za-zА-Яа-яЁё ]+)[ ]+([\d-]{1,20})[ ]+(год|гг)/u';
            preg_match_all($pattern, $goodStr, $out);

            if (!isset($out[0][0])) {
                return [];
            }

            $data['country'] = trim(str_replace($out[0][0], '', $goodStr), ' ,') ?: trim($prevEl, ' ,');
            $data['nominal'] = $out[1][0];
            $data['year']    = $out[2][0];

            $data['name'] = trim($dataFirst ? (implode('. ',
                $dataFirst)) : $data['country'] . ' ' . $data['nominal'] . ' ' . $data['year'], '., ');
        } else {
            $tt = 10;
        }

        return $data;
    }

    /**
     * Получаем параметры из таблицы
     *
     * @param Crawler $crawler
     * @return array
     */
    private function getParamsFromTableElement(Crawler $crawler): array
    {
        $params      = [];
        $tr_elements = $crawler->filter('.product_page table#product-features')->filterXPath('//table/tr');
        foreach ($tr_elements as $content) {
            $tds     = [];
            $crawler = new Crawler($content);
            foreach ($crawler->filter('td') as $node) {
                $tds[] = trim(trim($node->nodeValue), ':');
            }
            $params[] = $tds;
        }

        return ArrayHelper::map($params, 0, 1);
    }


    /**
     * @param string $action
     * @param array  $data
     * @param string $method
     * @return array
     */
    private function sendRequest(
        string $action,
        array $data = [],
        string $method = 'get',
        string $cookieName = null
    ): Response {
        sleep(0.3);

        $client  = new Client([
            //'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($action)
            ->setData($data)
            ->setHeaders([
                'Accept'           => '*/*',
                'Accept-Encoding'  => 'Accept-Encoding',
                'Accept-Language'  => 'ru,en;q=0.9,en-US;q=0.8',
                'Connection'       => 'keep-alive',
                'Host'             => 'collmart.ru',
                'Referer'          => 'https://collmart.ru/category/monety-rossii_x2/',
                'X-Compress'       => 'null',
                'X-Requested-With' => 'XMLHttpRequest',
                'User-Agent'       => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
                //'Upgrade-Insecure-Requests'                       => 1,
                //'X-DevTools-Emulate-Network-Conditions-Client-Id' => 'B72A04C0215FD004F5A13DE1FE7A5979',
                //'Accept'                                          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                //'Origin'          => 'https://collmart.ru',
            ]);

        $response = $request->send();
        return $response;
    }
}
