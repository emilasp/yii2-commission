<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\core\commands\AbstractConsoleController;
use Symfony\Component\DomCrawler\Crawler;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\Response;


/**
 * ParseMarkiMiraController
 *
 * @package emilasp\commission\commands
 */
class ParseMarkiMiraController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(): void
    {
        $this->display("-----Parse markimir ooo-----", self::FONT_COLOR_YELLOW);

        $links = $this->parseLinks();
        $count = count($links);
        $this->display("Parse stamps: {$count}", self::FONT_COLOR_YELLOW);

        foreach ($links as $index => $link) {
            $attributes = $this->parseProduct($link);

            if (isset($attributes['article'])) {
                if (!$model = CommissionCatalog::findOne([
                    'article' => $attributes['article'],
                    'site'    => 'markimira',
                ])) {
                    $model = new CommissionCatalog([
                        'site' => 'markimira',
                    ]);
                }
                $attributes['name'] = $model->name ?: $attributes['name'];
                $attributes['nominal'] = $model->nominal ?: ($attributes['nominal'] ?? '');
                $attributes['article'] = $model->article ?: ($attributes['article'] ?? '');

                $model->setAttributes($attributes);
                $model->data_params = json_encode([$attributes]);
                $model->data        = json_encode([$attributes]);
                $model->year        = (int)$model->year;

                $imageUrl = $attributes['image'] ?? null;
                if ($imageUrl && !$model->image_id) {
                    $model->imageUpload = 'https://markimira.ru' . $imageUrl;
                    $model->saveSingleImage();
                }

                $model->save();

                if ($model->hasErrors()) {
                    $errors = json_encode($model->getErrors()) . json_encode($model->getAttributes());
                    $this->display("Model has Errors: {$errors}", self::FONT_COLOR_RED);
                }

                $this->display("Parse model {$index}/{$count}: {$model->id}", self::FONT_COLOR_GREEN);
            } else {
                $this->display("Model NO articles!:", self::FONT_COLOR_RED);
            }
        }
    }

    /**
     * Parse links
     *
     * @return array
     */
    private function parseLinks(): array
    {
        $allUrls = [];

        $firstLinks = [];
        $count      = 1;
        $stop       = false;
        while (!$stop) {
            $this->display("Parse urls: {$count}", self::FONT_COLOR_BLUE);

            $link     = "https://markimira.ru/catalog_stamps_ussr/series_ussr/?arrFilter_91_MIN=1900&arrFilter_91_MAX=2000&set_filter=Y&PAGEN_1={$count}";
            $response = $this->sendRequest($link);

            $crawler = new Crawler($response->content);
            $links   = $crawler->filter('.item a');

            if (!$links->count()) {
                $stop = true;
            } else {
                $urls = $links->each(function ($link) {
                    $href = $link->attr('href');
                    if (strpos($href, 'ID=') !== false) {
                        return $href;
                    }
                    return null;
                });

                $allUrls = ArrayHelper::merge($allUrls, $urls);
            }
            $count++;
            //break;


            $urls = array_slice($urls, 0, 20);
            if ($count === 2) {
                $firstLinks = $urls;
            } elseif ($firstLinks === $urls) {
                $stop = true;
            }
            if ($count > 500) {
                $stop = true;
            }
        }

        $allUrls = array_unique($allUrls);
        $allUrls = array_diff($allUrls, [null, '']);

        return $allUrls;
    }


    /**
     * Parse product
     *
     * @return array
     */
    private function parseProduct(string $link): array
    {
        $link = 'https://markimira.ru' . $link;
        //$link = 'https://markimira.ru/catalog_stamps_ussr/stamps_ussr/detail.php?ID=10443';

        echo $link . ' >> ' . PHP_EOL;
        $this->display($link);

        $attributeFilters = [
            'name'        => [
                'filter' => '.product-name h1',
                'type'   => 'text',
                'value'  => function ($value) {
                    return str_replace('.', ',', $value);
                }
            ],
            'description' => [
                'filter' => '.product-tabs .std',
                'type'   => 'html',
            ],
            'image'       => [
                'filter'    => '.product-img-box a',
                'attribute' => 'href',
            ],

            'options' => [
                'filter' => '.product-options',
                'type'   => 'html',
            ],


            /*'date_at'         => [
                'filter' => '.wrap > .info .date',
                'type'   => 'text',
                'value'  => function ($value) {
                    return $value ? date('Y-m-d H:i:s', strtotime($value)) : '';
                }
            ],
            'artist_designer' => [
                'filter' => '.wrap > .info',
                'type'   => 'html',
                'value'  => function ($value) {
                    if (strpos($value, '<p>') !== false) {
                        $data = explode('<p>', $value);
                        return strip_tags(nl2br($data[1]));
                    }
                    return '';
                }
            ],*/

            /*'cost'    => [
                'filter' => '.cart1',
                'type'   => 'html',
                'value'  => function ($value) {
                    if (strpos($value, 'нет в наличии') !== false) {
                        return null;
                    }
                    $cost = 0;
                    if (preg_match('/Цена чист.:(.*)руб/', $value, $matches)) {
                        $cost = $matches[1] ?? 0;
                    }
                    if ($cost) {
                        return floatval($cost);
                    }
                    return null;
                }
            ],
            'cost_r'  => [
                'filter' => '.cart1',
                'type'   => 'html',
                'value'  => function ($value) {
                    if (strpos($value, 'нет в наличии') !== false) {
                        return null;
                    }
                    $cost = 0;
                    if (preg_match('/Цена гашен.:(.*)руб/', $value, $matches)) {
                        $cost = $matches[1] ?? 0;
                    }
                    if ($cost) {
                        return floatval($cost);
                    }
                    return null;
                }
            ],
            'article' => [
                'filter' => 'h1',
                'type'   => 'text',
                'value'  => function ($value) {
                    $article = '';
                    if (strpos($value, '.') !== false) {
                        $dataValue = explode('.', $value);
                        if (strpos($dataValue[0], '№') !== false) {
                            $article = trim($dataValue[0], '№ ');
                        }
                    }
                    return $article;
                }
            ],
            'type'    => [
                'filter' => '.wrap2 .info .red',
                'type'   => 'text',
            ],
            'themes'  => [
                'filterXpath' => '//table[@class="info2"]/following-sibling::p/a',
                'type'        => 'text',
            ],*/
        ];

        $response = $this->sendRequest($link);

        // Get Block Variants
        $crawler = new Crawler($response->content);

        $attributes = ['url' => $link];

        foreach ($attributeFilters as $attribute => $filter) {
            if (!empty($filter['filter'])) {
                $node = $crawler->filter($filter['filter']);
            } elseif (!empty($filter['filterXpath'])) {
                $node = $crawler->filterXPath($filter['filterXpath']);
            }

            $value = '';

            if ($node->count()) {
                if (!empty($filter['attribute'])) {
                    $value = $node->attr($filter['attribute']);
                } elseif ($filter['type'] === 'text') {
                    $value = $node->text();
                } elseif ($filter['type'] === 'html') {
                    $value = $node->html();
                } elseif ($filter['type'] === 'crawler') {
                    $value = $node;
                }

                if (!empty($filter['value']) && is_callable($filter['value'])) {
                    $value = $filter['value']($value);
                }
            }

            $attributes[$attribute] = trim($value);
        }


        $params     = $this->getStampParams($attributes['options']);
        $attributes = ArrayHelper::merge($attributes, $params);

        $attributes['params'] = $params;

        $attributes['cost']   = isset($attributes['cost']) ? floatval($attributes['cost']) : null;
        $attributes['cost_r'] = isset($attributes['cost_r']) ? floatval($attributes['cost_r']) : null;


        return $attributes;
    }

    /**
     * Парсим колонку
     *
     * @param Crawler $crawler
     * @return array
     */
    private static function getStampParams(string $data): array
    {
        $crawler        = new Crawler("<div>{$data}</div>");
        $optionsCrawler = $crawler->filter('p.availability');

        $options = $optionsCrawler->each(function ($link) {
            $name  = '';
            $value = '';
            $row   = $link->text();

            if (strpos($row, ':')) {
                $data = explode(':', $row);

                $name  = trim($data[0]);
                $value = trim($data[1], " \t\n\r\0\x0B ");
            }

            return [$name => $value];
        });

        $return = [];
        foreach ($options as $option) {
            $key   = array_keys($option)[0];
            $value = array_values($option)[0];

            $issetAttribute = false;
            foreach (CommissionCatalog::DATA_ATTRIBUTES as $param => $variates) {
                if (in_array($key, $variates)) {
                    $return[$param] = $value;
                    $issetAttribute = true;
                    break;
                }
            }

            if (!$issetAttribute) {
                $tt = 10;
            }
        }

        return $return;
    }

    /**
     * @param string $action
     * @param array  $data
     * @param string $method
     * @return array
     */
    private function sendRequest(
        string $action,
        array $data = [],
        string $method = 'get',
        string $cookieName = null
    ): Response {
        sleep(0.1);

        $client  = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $request = $client->createRequest()
            ->setMethod($method)
            ->setUrl($action)
            ->setData($data)
            ->setHeaders([
                'Origin'                                          => 'https://www.markimira.ru',
                'Upgrade-Insecure-Requests'                       => 1,
                'User-Agent'                                      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
                'X-DevTools-Emulate-Network-Conditions-Client-Id' => 'B72A04C0215FD004F5A13DE1FE7A5979',
                'Accept'                                          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Language'                                 => 'ru,en;q=0.9,en-US;q=0.8',
            ]);


        $response = $request->send();
        return $response;
    }
}
