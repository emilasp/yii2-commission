<?php

namespace emilasp\commission\console\commands;

use emilasp\commission\common\models\CommissionCatalog;
use emilasp\commission\common\models\CommissionProduct;
use emilasp\commission\common\models\ImCheckPrice;
use emilasp\core\commands\AbstractConsoleController;
use Yii;


/**
 *
 * @package emilasp\commission\commands
 */
class ImCheckOurIndexController extends AbstractConsoleController
{
    /**
     * Content publication
     */
    public function actionIndex(): void
    {
        $this->display("-----Start index check Monetnik category-----", self::FONT_COLOR_YELLOW);

        //Yii::$app->monentik->login();

        $this->display("Parse Monetnik category", self::FONT_COLOR_GREEN);

        $category = 'pochtovye-marki';

        $products = Yii::$app->monentik->parseProductCategory($category);

        $countAll = count($products);


        $this->display("Check items", self::FONT_COLOR_GREEN);

        $our                   = 0;
        $indexFirst            = null;
        $indexFirstLast        = null;
        $indexFirstLastWithout = false;
        foreach ($products as $index => $item) {
            $index++;
            if ($item['is_our']) {
                $our++;
                if (!$indexFirst) {
                    $indexFirst = $index;
                }

                if ($indexFirst && !$indexFirstLastWithout) {
                    $indexFirstLast = $index;
                }
            } else {
                if ($indexFirst) {
                    $indexFirstLastWithout = true;
                }
            }
        }

        $this->display("Found ALL: {$countAll}");
        $this->display("Found OUR: {$our}");
        $this->display("Found OUR First: {$indexFirst}");
        $this->display("Found OUR First last: {$indexFirstLast}");

        if ($indexFirst === null || $indexFirst > 4) {
            $this->sendErrors($category, $countAll, $our, $indexFirst);
        }
    }


    /**
     * Send errors to email
     */
    private function sendErrors(
        string $category,
        int $countAll = 0,
        int $our = 0,
        int $indexFirst = null,
        int $indexFirstLast = null
    ): void {
        $this->display('Send errors to email', self::FONT_COLOR_YELLOW);

        $message = Yii::$app->mailer->compose('commission/check-category-our', [
            'category'  => $category,
            'all'       => $countAll,
            'our'       => $our,
            'first'     => $indexFirst,
            'firstLast' => $indexFirstLast,
        ])
            ->setFrom(Yii::$app->params['email']['admin'])
            ->setTo('fremil@yandex.ru')
            ->setSubject('Commission: Изменилась видимость наших товаров!');

        $message->send();
    }
}
